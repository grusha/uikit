import type { Meta, StoryObj } from '@storybook/vue3';
import { ref } from 'vue';
import { userEvent, screen } from '@storybook/testing-library';
import { MaskitoOptions } from '@maskito/core';
import UiInput from '../../components/form/input/UiInput.vue';

const meta: Meta<typeof UiInput> = {
  title: 'Form/UiInput',
  tags: ['autodocs'],
  component: UiInput,
};

export default meta;

type Story = StoryObj<typeof UiInput>;

export const Primary: Story = {
  render: (args: any) => ({
    components: { UiInput },
    setup() {
      const model = ref('');
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-input v-bind="args" :model="model" @update:model="updateModel"></ui-input></div>',
  }),
  args: {
    inputStyles: {
      defaultInput: 'ui-input',
      errorInput: 'ui-input--error',
      defaultLabel: 'ui-input__label',
      errorLabel: 'ui-input__label--error',
      activeLabel: 'ui-input__label--active',
      bodyInput: 'ui-input-body',
      wrapper: 'ui-input__wrapper',
      input: 'ui-input__input',
      icon: 'ui-input__icon',
      error: 'ui-input__error',
    },
  },
  play: async () => {
    const input = screen.getByRole('input');

    
    userEvent.click(input);
    await userEvent.type(input, 'something', { delay: 200 });
  },
};

export const Password: Story = {
  render: (args: any) => ({
    components: { UiInput },
    setup() {
      const model = ref('');
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-input v-bind="args"></ui-input></div>',
  }),
  args: {
    type: 'password',
    model: 'password',
    inputStyles: {
      defaultInput: 'ui-input',
      errorInput: 'ui-input--error',
      defaultLabel: 'ui-input__label',
      errorLabel: 'ui-input__label--error',
      activeLabel: 'ui-input__label--active',
      bodyInput: 'ui-input-body',
      wrapper: 'ui-input__wrapper',
      input: 'ui-input__input',
      icon: 'ui-input__icon',
      error: 'ui-input__error',
    },
  },

};

export const Masked: Story = {
  render: (args: any) => ({
    components: { UiInput },
    setup() {
      const model = ref('');
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-input v-bind="args"></ui-input></div>',
  }),
  argTypes: {
    model: {
      table: {
        disable: true,
      },
    },
    disabled: {
      table: {
        disable: true,
      },
    },
    inputStyles: {
      table: {
        disable: true,
      },
    },
    type: {
      table: {
        disable: true,
      },
    },
    errorMessage: {
      table: {
        disable: true,
      },
    },
    label: {
      table: {
        disable: true,
      },
    },
    name: {
      table: {
        disable: true,
      },
    },
  },
  args: {
    model: '12125552368',
    maskitoOptions: {
      mask: [
        '+',
        '1',
        ' ',
        '(',
        /\d/,
        /\d/,
        /\d/,
        ')',
        ' ',
        /\d/,
        /\d/,
        /\d/,
        '-',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ],
    } as MaskitoOptions,
    inputStyles: {
      defaultInput: 'ui-input',
      errorInput: 'ui-input--error',
      defaultLabel: 'ui-input__label',
      errorLabel: 'ui-input__label--error',
      activeLabel: 'ui-input__label--active',
      bodyInput: 'ui-input-body',
      wrapper: 'ui-input__wrapper',
      input: 'ui-input__input',
      icon: 'ui-input__icon',
      error: 'ui-input__error',
    },
  },
};

export const Disabled: Story = {
  render: (args: any) => ({
    components: { UiInput },
    setup() {
      return { args };
    },
    template: '<div style="width: 600px;"><ui-input v-bind="args"></ui-input></div>',
  }),
  argTypes: {
    model: {
      table: {
        disable: true,
      },
    },
    errorMessage: {
      table: {
        disable: true,
      },
    },
    inputStyles: {
      table: {
        disable: true,
      },
    },
    type: {
      table: {
        disable: true,
      },
    },
    label: {
      table: {
        disable: true,
      },
    },
    name: {
      table: {
        disable: true,
      },
    },
  },
  args: {
    disabled: true,
    model: 'disabled',
    inputStyles: {
      defaultInput: 'ui-input',
      errorInput: 'ui-input--error',
      defaultLabel: 'ui-input__label',
      errorLabel: 'ui-input__label--error',
      activeLabel: 'ui-input__label--active',
      bodyInput: 'ui-input-body',
      wrapper: 'ui-input__wrapper',
      input: 'ui-input__input',
      icon: 'ui-input__icon',
      error: 'ui-input__error',
    },
  },
};

export const Error: Story = {
  render: (args: any) => ({
    components: { UiInput },
    setup() {
      return { args };
    },
    template: '<div style="width: 600px;"><ui-input v-bind="args"></ui-input></div>',
  }),
  argTypes: {
    model: {
      table: {
        disable: true,
      },
    },
    disabled: {
      table: {
        disable: true,
      },
    },
    inputStyles: {
      table: {
        disable: true,
      },
    },
    type: {
      table: {
        disable: true,
      },
    },
    label: {
      table: {
        disable: true,
      },
    },
    name: {
      table: {
        disable: true,
      },
    },
  },
  args: {
    model: '123',
    errorMessage: 'Некорректное значение',
    inputStyles: {
      defaultInput: 'ui-input',
      errorInput: 'ui-input--error',
      defaultLabel: 'ui-input__label',
      errorLabel: 'ui-input__label--error',
      activeLabel: 'ui-input__label--active',
      bodyInput: 'ui-input-body',
      wrapper: 'ui-input__wrapper',
      input: 'ui-input__input',
      icon: 'ui-input__icon',
      error: 'ui-input__error',
    },
  },
};
