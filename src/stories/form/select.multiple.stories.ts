import type { Meta, StoryObj } from '@storybook/vue3';
import { ref } from 'vue';
import { userEvent, screen } from '@storybook/testing-library';
import UiSelectMultiple from '../../components/form/selectMultiple/UiSelectMultiple.vue';

const meta: Meta<typeof UiSelectMultiple> = {
  title: 'Form/UiSelectMultiple',
  tags: ['autodocs'],
  component: UiSelectMultiple,
};

export default meta;

type Story = StoryObj<typeof UiSelectMultiple>;

export const Primary: Story = {
  render: (args: any) => ({
    components: { UiSelectMultiple },
    setup() {
      const model = ref(null);
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-select-multiple v-bind="args" :model="model" @update:model="updateModel"></ui-select-multiple></div>',
  }),
  args: {
    name: 'dropdownId',
    options: [{ id: 0, title: 'Vue' }, { id: 1, title: 'React' }, { id: 2, title: 'Angular' }],
    selectStyles: {
      uiSelectDefault: 'ui-select',
      uiSelectInset: 'ui-select__inset',
      label: 'ui-select__label',
      body: 'ui-select__body',
    },
    dropdownStyles: {
      dropdown: 'dropdown',
      disabled: 'dropdown_disabled',
      list: 'dropdown__list',
      item: 'dropdown__item',
      text: 'dropdown__text',
    },
  },
  play: async () => {
    const select = screen.getByText('Выберите из списка');

    await userEvent.click(select);

    const option1 = screen.getByText('Vue');

    await userEvent.hover(option1);

    await userEvent.click(option1);

    const option2 = screen.getByText('React');

    await userEvent.hover(option2);

    await userEvent.click(option2);
  },
};
export const Disabled: Story = {
  render: (args: any) => ({
    components: { UiSelectMultiple },
    setup() {
      return { args };
    },
    template: '<div style="width: 600px;"><ui-select-multiple v-bind="args"></ui-select-multiple></div>',
  }),
  args: {
    name: 'dropdownId',
    options: [{ id: 0, title: 'Vue' }, { id: 1, title: 'React' }, { id: 2, title: 'Angular' }],
    selectStyles: {
      uiSelectDefault: 'ui-select',
      uiSelectInset: 'ui-select__inset',
      label: 'ui-select__label',
      body: 'ui-select__body',
    },
    dropdownStyles: {
      dropdown: 'dropdown',
      disabled: 'dropdown_disabled',
      list: 'dropdown__list',
      item: 'dropdown__item',
      text: 'dropdown__text',
    },
    disabled: true,
  },
};
