import type { Meta, StoryObj } from '@storybook/vue3';
import { ref } from 'vue';
import { userEvent, screen } from '@storybook/testing-library';
import UiRadioGroup from '../../components/form/radio-group/UiRadioGroup.vue';

const meta: Meta<typeof UiRadioGroup> = {
  title: 'Form/UiRadioGroup',
  tags: ['autodocs'],
  component: UiRadioGroup,
};

export default meta;

type Story = StoryObj<typeof UiRadioGroup>;

export const Primary: Story = {
  render: (args: any) => ({
    components: { UiRadioGroup },
    setup() {
      const model = ref('');
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-radio-group v-bind="args" :model="model" @update:model="updateModel"></ui-radio-group></div>',
  }),
  args: {
    radioGroupStyles: {
      radioGroup: 'ui-radio-group',
      radioGroupLabel: 'ui-radio-group__label',
      radioGroupContainer: 'ui-radio-group__container',
      radioGroupContainerError: 'ui-radio-group__container_error',
      radioGroupErrorMessage: 'ui-radio-group__error-message',
      radioButton: 'ui-radio-button',
      radioButtonTitle: 'ui-radio-button_title',
      radioButtonButtonsContainer: 'ui-radio-button_buttons-container',
      radioButtonDisabled: 'ui-radio-button_disabled',
      radioButtonInput: 'ui-radio-button__input',
      radioButtonCheckmark: 'ui-radio-button__checkmark',
      radioButtonCheckmarkChecked: 'ui-radio-button__checkmark_checked',
      radioButtonCheckmarkDisabled: 'ui-radio-button__checkmark_disabled',
      radioButtonLabel: 'ui-radio-button__label',
    },
    label: 'Radio Group',
    options: ['Vue', 'Angular', 'React'],
    model: '',
  },
  play: async () => {
    const firstButton = screen.getByText('Vue');
    const secondButton = screen.getByText('Angular');
    const thirdButton = screen.getByText('React');

    userEvent.click(firstButton);

    setTimeout(() => userEvent.click(thirdButton), 3000);
    setTimeout(() => userEvent.click(secondButton), 5000);
  },
};

export const Disabled: Story = {
  render: (args: any) => ({
    setup() {
      return { args };
    },
    components: { UiRadioGroup },
    template: '<div style="width: 600px;"><ui-radio-group v-bind="args"></ui-radio-group></div>',
  }),
  args: {
    radioGroupStyles: {
      radioGroup: 'ui-radio-group',
      radioGroupLabel: 'ui-radio-group__label',
      radioGroupContainer: 'ui-radio-group__container',
      radioGroupContainerError: 'ui-radio-group__container_error',
      radioGroupErrorMessage: 'ui-radio-group__error-message',
      radioButton: 'ui-radio-button',
      radioButtonTitle: 'ui-radio-button_title',
      radioButtonButtonsContainer: 'ui-radio-button_buttons-container',
      radioButtonDisabled: 'ui-radio-button_disabled',
      radioButtonInput: 'ui-radio-button__input',
      radioButtonCheckmark: 'ui-radio-button__checkmark',
      radioButtonCheckmarkChecked: 'ui-radio-button__checkmark_checked',
      radioButtonCheckmarkDisabled: 'ui-radio-button__checkmark_disabled',
      radioButtonLabel: 'ui-radio-button__label',
    },
    label: 'Radio Group',
    options: ['Vue', 'Angular', 'React'],
    disabled: true,
    model: 'Vue',
  },
};

export const Error: Story = {
  render: (args: any) => ({
    setup() {
      return { args };
    },
    components: { UiRadioGroup },
    template: '<div style="width: 600px;"><ui-radio-group v-bind="args"></ui-radio-group></div>',
  }),
  args: {
    radioGroupStyles: {
      radioGroup: 'ui-radio-group',
      radioGroupLabel: 'ui-radio-group__label',
      radioGroupContainer: 'ui-radio-group__container',
      radioGroupContainerError: 'ui-radio-group__container_error',
      radioGroupErrorMessage: 'ui-radio-group__error-message',
      radioButton: 'ui-radio-button',
      radioButtonTitle: 'ui-radio-button_title',
      radioButtonButtonsContainer: 'ui-radio-button_buttons-container',
      radioButtonDisabled: 'ui-radio-button_disabled',
      radioButtonInput: 'ui-radio-button__input',
      radioButtonCheckmark: 'ui-radio-button__checkmark',
      radioButtonCheckmarkChecked: 'ui-radio-button__checkmark_checked',
      radioButtonCheckmarkDisabled: 'ui-radio-button__checkmark_disabled',
      radioButtonLabel: 'ui-radio-button__label',
    },
    label: 'Radio Group',
    options: ['Vue', 'Angular', 'React'],
    errorMessage: 'Some Error',
    model: '',
  },
};
