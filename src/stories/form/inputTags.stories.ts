import type { Meta, StoryObj } from '@storybook/vue3';
import { ref } from 'vue';
import { userEvent, screen } from '@storybook/testing-library';
import UiInputTags from '../../components/form/input-tags/UiInputTags.vue';

const meta: Meta<typeof UiInputTags> = {
  title: 'Form/UiInputTags',
  tags: ['autodocs'],
  component: UiInputTags,
};

export default meta;

type Story = StoryObj<typeof UiInputTags>;

export const Primary: Story = {
  render: (args: any) => ({
    components: { UiInputTags },
    setup() {
      const model = ref('');
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-input-tags v-bind="args" :model="model" @update:model="updateModel"></ui-input-tags></div>',
  }),
  args: {
    inputTagsStyles: {
      inputTags: 'input-tags',
      inputTagsLabel: 'input-tags-label',
      inputTagsWrapper: 'input-tags-wrapper',
      inputTagsTag: 'input-tags-tag',
      inputTagsIcon: 'input-tags-icon',
      inputTagsInput: 'input-tags-input',
      inputTagsInputError: 'input-tags-input--error',
      inputTagsError: 'input-tags-error',
    },
    label: 'Input Tags',
  },
  play: async () => {
    const input = screen.getByRole('input');

    userEvent.click(input);
    await userEvent.type(input, 'something', { delay: 200 });
    await userEvent.keyboard('{Enter}', { delay: 100 });
    await userEvent.type(input, 'something123', { delay: 200 });
    await userEvent.keyboard('{Enter}', { delay: 10220 });
    await userEvent.keyboard('{Backspace}', { delay: 10220 });
    await userEvent.type(input, 'something1238', { delay: 200 });
    await userEvent.keyboard('{Enter}', { delay: 10220 });
  },
};

export const Disabled: Story = {
  render: (args: any) => ({
    components: { UiInputTags },
    setup() {
      const model = ref(['vue', 'react', 'angular']);
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-input-tags v-bind="args" :model="model" @update:model="updateModel"></ui-input-tags></div>',
  }),
  args: {
    inputTagsStyles: {
      inputTags: 'input-tags',
      inputTagsLabel: 'input-tags-label',
      inputTagsWrapper: 'input-tags-wrapper',
      inputTagsTag: 'input-tags-tag',
      inputTagsIcon: 'input-tags-icon',
      inputTagsInput: 'input-tags-input',
      inputTagsInputError: 'input-tags-input--error',
      inputTagsError: 'input-tags-error',
    },
    label: 'Input Tags',
    disabled: true,
  },
};

export const Error: Story = {
  render: (args: any) => ({
    components: { UiInputTags },
    setup() {
      const model = ref();
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-input-tags v-bind="args" :model="model" @update:model="updateModel"></ui-input-tags></div>',
  }),
  args: {
    inputTagsStyles: {
      inputTags: 'input-tags',
      inputTagsLabel: 'input-tags-label',
      inputTagsWrapper: 'input-tags-wrapper',
      inputTagsTag: 'input-tags-tag',
      inputTagsIcon: 'input-tags-icon',
      inputTagsInput: 'input-tags-input',
      inputTagsInputError: 'input-tags-input--error',
      inputTagsError: 'input-tags-error',
    },
    label: 'Input Tags',
    errorMessage: 'Some error',
  },
};
