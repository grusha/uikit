import type { Meta, StoryObj } from '@storybook/vue3';
import { ref } from 'vue';
import { userEvent, screen } from '@storybook/testing-library';
import UiDatePicker from '../../components/form/datePicker/UiDatePicker.vue';

const meta: Meta<typeof UiDatePicker> = {
  title: 'Form/UiDatePicker',
  tags: ['autodocs'],
  component: UiDatePicker,
};

export default meta;

type Story = StoryObj<typeof UiDatePicker>;

export const Primary: Story = {
  render: (args: any) => ({
    components: { UiDatePicker },
    setup() {
      const model = ref('');
      const updateModel = (event: any) => { model.value = event; };

      return { args, updateModel, model };
    },
    template: '<div style="width: 600px;"><ui-date-picker v-bind="args" :model="model" @update:model="updateModel" /></div>',
  }),
  args: {
    dateRangeStyles: {
      dateRange: 'ui-daterange',
      label: 'ui-daterange__label',
      required: 'ui-date-range__required',
      error: 'ui-daterange__error',
      input: 'ui-date-range__input',
      inputError: 'ui-date-range__input_error',
    },
  },
  play: async () => {
    const input = screen.getByRole('datePicker');

    await userEvent.click(input);

    // const date = screen.getByRole('document');

    // console.log(date);
    // userEvent.click(date);
  },
};
