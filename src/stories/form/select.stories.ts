import type { Meta, StoryObj } from '@storybook/vue3';
import { ref } from 'vue';
import { userEvent, screen } from '@storybook/testing-library';
import UiSelect from '../../components/form/select/UiSelect.vue';

const meta: Meta<typeof UiSelect> = {
  title: 'Form/UiSelect',
  tags: ['autodocs'],
  component: UiSelect,
};

export default meta;

type Story = StoryObj<typeof UiSelect>;

export const Primary: Story = {
  render: (args: any) => ({
    components: { UiSelect },
    setup() {
      const model = ref('');
      const options = ref([
        { id: '1', collectionName: 'test', title: 'Vue' },
        { id: '2', collectionName: 'test', title: 'React' },
        { id: '3', collectionName: 'test', title: 'Angular' },
      ]);

      const updateModel = (event: any) => { model.value = event; };
      const updateOptions = (event: any) => {
        const newOption = {
          id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10),
          title: event.title.trim(),
          collectionName: event.collectionName,
        };

        options.value.push(newOption);
      };

      return {
        args, updateModel, updateOptions, model, options,
      };
    },
    template: '<div style="width: 600px;"><ui-select v-bind="args" :options=options @update:options="updateOptions" :model="model" @update:model="updateModel"></ui-select></div>',
  }),
  args: {
    name: 'dropdownId',
    options: [
      { id: '1', collectionName: 'test', title: 'Vue' },
      { id: '2', collectionName: 'test', title: 'React' },
      { id: '3', collectionName: 'test', title: 'Angular' },
    ],
    selectStyles: {
      selectBig: 'ui-select-big',
      label: 'ui-select-big__label',
      input: 'ui-select-big__select__input',
      inputDisabled: 'ui-select-big__select__input_disabled',
      required: 'ui-select-big__required',
      selectDefault: 'ui-select-big__select',
      selectError: 'ui-select-big__select--error',
      selectValue: 'ui-select-big__select__value',
      error: 'ui-select-big__error',
      dropdown: {
        dropdown: 'dropdown',
        disabled: 'dropdown_disabled',
        itemFocused: 'dropdown__item_focused',
        list: 'dropdown__list',
        item: 'dropdown__item',
        text: 'dropdown__text',
      },
    },
    filterable: true,
    expandable: true,
  },
  play: async () => {
    const select = screen.getByTestId('uiSelectInput');

    await userEvent.click(select);

    const option = screen.getByText('Vue');

    await userEvent.hover(option);

    await userEvent.click(option);
  },
};

export const filterable: Story = {
  render: (args: any) => ({
    components: { UiSelect },
    setup() {
      const model = ref('');
      const options = ref([
        { id: '1', collectionName: 'test', title: 'Vue' },
        { id: '2', collectionName: 'test', title: 'React' },
        { id: '3', collectionName: 'test', title: 'Angular' },
      ]);

      const updateModel = (event: any) => { model.value = event; };
      const updateOptions = (event: any) => {
        const newOption = {
          id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10),
          title: event.title.trim(),
          collectionName: event.collectionName,
        };

        options.value.push(newOption);
      };

      return {
        args, updateModel, updateOptions, model, options,
      };
    },
    template: '<div style="width: 600px;"><ui-select v-bind="args" :options=options @update:options="updateOptions" :model="model" @update:model="updateModel"></ui-select></div>',
  }),
  args: {
    name: 'dropdownId',
    options: [
      { id: '1', collectionName: 'test', title: 'Vue' },
      { id: '2', collectionName: 'test', title: 'React' },
      { id: '3', collectionName: 'test', title: 'Angular' },
    ],
    selectStyles: {
      selectBig: 'ui-select-big',
      label: 'ui-select-big__label',
      input: 'ui-select-big__select__input',
      inputDisabled: 'ui-select-big__select__input_disabled',
      required: 'ui-select-big__required',
      selectDefault: 'ui-select-big__select',
      selectError: 'ui-select-big__select--error',
      selectValue: 'ui-select-big__select__value',
      error: 'ui-select-big__error',
      dropdown: {
        dropdown: 'dropdown',
        disabled: 'dropdown_disabled',
        itemFocused: 'dropdown__item_focused',
        list: 'dropdown__list',
        item: 'dropdown__item',
        text: 'dropdown__text',
      },
    },
    filterable: true,
  },
};

export const Disabled: Story = {
  render: (args: any) => ({
    components: { UiSelect },
    setup() {
      return { args };
    },
    template: '<div style="width: 600px;"><ui-select v-bind="args"></ui-select></div>',
  }),
  args: {
    name: 'dropdownId',
    options: [
      { id: '1', collectionName: 'test', title: 'Vue' },
      { id: '2', collectionName: 'test', title: 'React' },
      { id: '3', collectionName: 'test', title: 'Angular' },
    ],
    selectStyles: {
      selectBig: 'ui-select-big',
      label: 'ui-select-big__label',
      input: 'ui-select-big__select__input',
      inputDisabled: 'ui-select-big__select__input_disabled',
      required: 'ui-select-big__required',
      selectDefault: 'ui-select-big__select',
      selectError: 'ui-select-big__select--error',
      selectValue: 'ui-select-big__select__value',
      error: 'ui-select-big__error',
      dropdown: {
        dropdown: 'dropdown',
        disabled: 'dropdown_disabled',
        list: 'dropdown__list',
        item: 'dropdown__item',
        text: 'dropdown__text',
      },
    },
    disabled: true,
  },
};

export const Error: Story = {
  render: (args: any) => ({
    components: { UiSelect },
    setup() {
      return { args };
    },
    template: '<div style="width: 600px;"><ui-select v-bind="args"></ui-select></div>',
  }),
  args: {
    name: 'dropdownId',
    options: [
      { id: '1', collectionName: 'test', title: 'Vue' },
      { id: '2', collectionName: 'test', title: 'React' },
      { id: '3', collectionName: 'test', title: 'Angular' },
    ],
    selectStyles: {
      selectBig: 'ui-select-big',
      label: 'ui-select-big__label',
      input: 'ui-select-big__select__input',
      inputDisabled: 'ui-select-big__select__input_disabled',
      required: 'ui-select-big__required',
      selectDefault: 'ui-select-big__select',
      selectError: 'ui-select-big__select--error',
      selectValue: 'ui-select-big__select__value',
      error: 'ui-select-big__error',
      dropdown: {
        dropdown: 'dropdown',
        disabled: 'dropdown_disabled',
        list: 'dropdown__list',
        item: 'dropdown__item',
        text: 'dropdown__text',
      },
    },
    disabled: true,
    errorMessage: '123'
  },
};
