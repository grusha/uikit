import { createApp } from 'vue';
import App from './App.vue';
// eslint-disable-next-line import/no-unresolved
import 'virtual:svg-icons-register';
import '../dist/theme.css';
import '../dist/indexCore.css';

createApp(App).mount('#app');
