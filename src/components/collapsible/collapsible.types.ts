export interface uiCollapsibleStyles {
  collapsibleWrapper?: string;
  collapsibleWrapperActive?: string,
  collapsible?: string;
  collapsibleContent?: string;
  icon?: string;
  color?: string;
}
