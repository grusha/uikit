export interface UiExternalLinkStyles {
    linkHeaderMenuTheme?: string,
}

export interface Link {
    href?: string;
    target?: string;
    rel?: string;
}

export interface UiLinkStyles {
    linkDefaultTheme?: string;
    linkSecondaryTheme?: string;
    linkActiveTheme?: string;
}
export interface UiButtonStyles {
    btn: string;
    defaultTheme: string;
    secondaryTheme: string;
    linkDefaultTheme: string;
    typeDefault: string;
    typeIconCircle: string;
    typeSquare: string;
}
export interface Button {
    uiButton: UiButtonStyles;
    uiLink: UiLinkStyles;
    uiExternalLink: UiExternalLinkStyles;
}
