export interface UiRoutesStyles {
    routes: string,
    wrapper: string,
    itemDefault: string,
    itemActive: string,
    itemMargin: string,
    itemLink: string,
}
