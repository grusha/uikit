import { computed, ref } from 'vue';
import {
  DropzoneEmits, DropzoneError, DropzoneProps, ReceivedFilesOptions,
} from './dropzone.types';

const checkFilesByAcceptedExt = (files: File[], acceptableExtensions: string[]): DropzoneError[] => {
  const errors: DropzoneError[] = [];

  files.forEach((file) => {
    const extFile = file.type?.split('/')[1];

    if (!acceptableExtensions.some((ext: string) => ext === extFile)) {
      errors.push(
        {
          errorMessage: 'Недопустимое расширение!',
          fileName: file.name,
          fileExt: extFile,
          fileType: file.type,
          fileSize: file.size,
        },
      );
    }
  });

  return errors;
};

const checkFilesBySize = (files: File[], acceptableSize: number): DropzoneError[] => {
  const errors: DropzoneError[] = [];

  files.forEach((file) => {
    const fileSize = file.size;

    if (fileSize > acceptableSize) {
      errors.push(
        {
          errorMessage: 'Размер файла превышает допустимый!',
          fileName: file.name,
          fileExt: file.type?.split('/')[1],
          fileType: file.type,
          fileSize: file.size,
        },
      );
    }
  });

  return errors;
};

export const formatBytes = (bytes: number, decimals = 2) => {
  if (!+bytes) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
};

const getMimeTypesToDisplay = (acceptableFileTypes: string[]) => {
  const mimeTypesArray = [
    '.aac', '.abw', '.arc', '.avi', '.azw',
    '.bin', '.bmp', '.bz', '.bz2', '.csh',
    '.css', '.csv', '.doc', '.docx', '.eot',
    '.epub', '.gz', '.gif', '.htm', '.html',
    '.ico', '.ics', '.jar', '.jpeg', '.js',
    '.json', '.jsonld', '.mid', '.mjs', '.mp3',
    '.mpeg', '.mpkg', '.odp', '.ods', '.odt',
    '.oga', '.ogv', '.ogx', '.opus', '.otf',
    '.png', '.pdf', '.php', '.ppt', '.pptx',
    '.rar', '.rtf', '.sh', '.svg', '.swf',
    '.tar', '.tif', '.tiff', '.ts', '.ttf',
    '.txt', '.vsd', '.wav', '.weba', '.webm',
    '.webp', '.woff', '.woff2', '.xhtml', '.xls',
    '.xlsx', '.xml', '.xul', '.zip', '.3gp',
    '.3g2', '.7z', '.jpg',
  ];

  const result: string[] = [];

  acceptableFileTypes.forEach((type) => {
    if (mimeTypesArray.includes(`.${type}`)) {
      result.push(type.toUpperCase());
    }
  });

  return result;
};

const receiveFiles = (files: File[], receivedFilesOptions: ReceivedFilesOptions, emits: DropzoneEmits) => {
  const receivedFilesErrors = [
    ...checkFilesByAcceptedExt(files, receivedFilesOptions.acceptableExtensions),
    ...checkFilesBySize(files, receivedFilesOptions.acceptableFileSize),
  ];

  const filesWithErrors = receivedFilesErrors.map((error: DropzoneError) => error.fileName);

  const validFiles = files.filter((file: File) => !filesWithErrors.includes(file.name));

  if (receivedFilesErrors.length > 0) {
    emits('handle-upload-files-errors', receivedFilesErrors);
  }

  if (receivedFilesOptions.multiple && validFiles.length > receivedFilesOptions.limit) {
    validFiles.length = receivedFilesOptions.limit;
  }

  receivedFilesOptions.proxyFile.value = validFiles;

  emits('on-upload-files', validFiles);
};

const cleanUpClasses = (event: any, dragleaveClass: string, dragoverClass: string) => {
  event.currentTarget.classList.add(dragleaveClass);
  event.currentTarget.classList.remove(dragoverClass);
};

export const useDropzone = (props: DropzoneProps, emits: DropzoneEmits) => {
  const input = ref(null);
  const fileInputLabel = ref('Выберите');

  const fileId = computed(() => `file_id_${props.name}`);

  const sizeLimit = computed(() => formatBytes(props.acceptableFileSize ?? 0));

  const acceptableExtensionsToDisplay = computed(() => getMimeTypesToDisplay(props.acceptableExtensions ?? []));

  const acceptableExtensionsParam = computed(() => props.acceptableExtensions?.map((ext) => `.${ext}`).join(','));

  const proxyFile = computed({
    get: () => props.model,
    set: (value) => {
      if (value) emits('update:model', value);
    },
  });

  const calculateDropzoneFooterContent = () => {
    const extendsMessage = `Поддерживаемые форматы: ${acceptableExtensionsToDisplay.value.slice(0, 2).join(', ')} ${acceptableExtensionsToDisplay.value.length > 2 ? ' и ' : ''}`;

    const extendsPopupMessage = `еще ${acceptableExtensionsToDisplay.value.length - 2}`;

    const limitMessage = `Максимум: ${sizeLimit.value} / ${props.limit} файла/ов`;

    return {
      extendsMessage,
      extendsPopupMessage,
      limitMessage,
    };
  };

  const onChange = (event: Event) => {
    const target = event.target as HTMLInputElement; // Плохая типизация интерфейсов

    const files = target.files ? Array.from(target.files) : [];

    input.value = null;

    (event.target as HTMLInputElement).value = '';

    const filesOptions: ReceivedFilesOptions = {
      multiple: props.multiple ?? false,
      acceptableExtensions: props.acceptableExtensions ?? [],
      acceptableFileSize: props.acceptableFileSize ?? 0,
      limit: props.limit ?? 4,
      proxyFile,
    };

    receiveFiles(files, filesOptions, emits);
  };

  const dragover = (event: Event) => {
    event.preventDefault();
    if (event.currentTarget instanceof HTMLElement && !event.currentTarget?.classList.contains('dropzone_dragover')) {
      event.currentTarget.classList.remove('dropzone_dragleave');
      event.currentTarget.classList.add('dropzone_dragover');
    }
  };

  const dragleave = (event: Event) => {
    cleanUpClasses(event, 'dropzone_dragleave', 'dropzone_dragover');
  };

  const drop = (event: any) => {
    event.preventDefault();
    const files: any[] = event ? Array.from(event.dataTransfer.files) : [];

    input.value = null;

    const filesOptions: ReceivedFilesOptions = {
      multiple: props.multiple ?? false,
      acceptableExtensions: props.acceptableExtensions ?? [],
      acceptableFileSize: props.acceptableFileSize ?? 0,
      limit: props.limit ?? 4,
      proxyFile,
    };

    receiveFiles(files, filesOptions, emits);
    cleanUpClasses(event, 'dropzone_dragleave', 'dropzone_dragover');
  };

  return {
    input,
    fileInputLabel,
    fileId,
    sizeLimit,
    acceptableExtensionsToDisplay,
    acceptableExtensionsParam,
    calculateDropzoneFooterContent,
    onChange,
    dragover,
    dragleave,
    drop,
  };
};
