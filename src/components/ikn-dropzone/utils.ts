export const mime2type = (mime: string) => {
  const mimeDict = {
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'pptx',
    'application/msword': 'doc',
    'application/zip': 'zip',
    'application/vnd.ms-excel.sheet.macroenabled.12': 'xlsx',
    'application/pdf': 'pdf',
    'image/webp': 'webp',
    'image/png': 'png',
    'image/jpg': 'jpg',
    'image/jpeg': 'jpeg',
  };

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const type = mimeDict[mime];

  if (!type) {
    console.error(`Неизвестный mime "${mime}"`);

    return '-';
  }

  return type;
};

export const type2icon = (type: string) => {
  const iconDict = {
    zip: 'extension-file',
    xlsx: 'extension-excel',
    docx: 'extension-word',
    doc: 'extension-word',
    pdf: 'extension-pdf',
    webp: 'extension-webp',
    png: 'extension-png',
    jpg: 'extension-jpeg',
    jpeg: 'extension-jpeg',
    pptx: 'extension-powerPoint',
  };
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const icon = iconDict[type];

  if (!icon) {
    console.error(`Неизвестный тип "${type}"`);

    return '#000';
  }

  return icon;
};
