import {
  computed,
  defineComponent, h, resolveComponent,
} from 'vue';
import { BarChart } from 'vue-chart-3';
import { GRAY_30 } from '../../core/color';

export default defineComponent({
  name: 'UiChartBar',
  components: {
    BarChart,
  },
  props: {
    data: {
      type: Object,
      required: true,
    },
    hasLabels: {
      type: Boolean,
      default: false,
    },
  },
  setup(props) {
    const options = computed(() => ({
      maintainAspectRatio: false,
      responsive: true,
      layout: {
        padding: {
          top: 24,
          right: 8,
          bottom: 24,
          left: 8,
        },
      },
      plugins: {
        tooltip: {
          enabled: false,
        },
        legend: {
          display: false,
        },
        datalabels: {
          color: GRAY_30,
          labels: {
            title: {
              anchor: 'end',
              align: 'top',
            },
            value: props.hasLabels ? {
              anchor: 'start',
              align: 'start',
              formatter(value: any, context: any) {
                if (context?.dataIndex > -1) {
                  return context?.chart?.data?.labels[context?.dataIndex];
                }

                return value;
              },
            } : { display: false },
          },
          font: {
            weight: 'bold',
            size: 16,
          },
        },
      },
      title: {
        display: false,
        text: 'Bar Chart',
      },
      scales: {
        xAxes: {
          display: false,
        },
        yAxes: {
          display: false,
        },
      },
    }));

    return { options };
  },
  render() {
    return h(resolveComponent('BarChart'), {
      options: this.options,
      chartData: this.data,
    });
  },
});
