import { h } from 'vue';
import { DoughnutChart } from 'vue-chart-3';

const UiChartDoughnut = (props: any) => {
  const chartOptions = {
    responsive: true,
    layout: {
      padding: {
        top: 24,
        right: 8,
        bottom: 24,
        left: 8,
      },
    },
    plugins: {
      tooltip: {
        enabled: false,
      },
      legend: {
        display: false,
      },
      title: {
        display: false,
        text: 'Doughnut Chart',
      },
      datalabels: {
        display: false,
      },
    },
  };

  return h(DoughnutChart, {
    options: chartOptions,
    chartData: props.data,
  });
};

UiChartDoughnut.props = {
  data: {
    type: Object,
    required: true,
  },
};

export default UiChartDoughnut;
