import { SelectOption } from '../form/select/select.types';

export type PopupSelectStyles = {
  select?: string;
  tagList?: string;
  tag?: string;
  input?: string;
  optionsList?: string;
  option?: string;
  optionChosen?: string;
  checkbox?: string;
  checkboxActive?: string;
}

export type PopupSelectProps = {
  selectOptions: SelectOption[];
  model?: string[];
  disabled?: boolean;
  styles?: PopupSelectStyles;
}

export type PopupSelectEmits = {
  (event: 'update:model', payload: string[]): void;
}
