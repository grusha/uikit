export type IUiRadioGroupStyles = {
  radioGroup?: string;
  radioGroupLabel?: string;
  radioGroupContainer?: string;
  radioGroupContainerError?: string;
  radioGroupErrorMessage?: string;
  radioButton?: string;
  radioButtonTitle?: string;
  radioButtonButtonsContainer?: string;
  radioButtonDisabled?:string;
  radioButtonInput?: string;
  radioButtonCheckmark?: string;
  radioButtonCheckmarkChecked?: string;
  radioButtonCheckmarkDisabled?: string;
  radioButtonLabel?: string;
}

export type IUiRadioGroupProps = {
  name?: string;
  label?: string;
  title?: string;
  errorMessage?: string;
  disabled?: boolean;
  model?: string;
  options?: string[];
  radioGroupStyles?: IUiRadioGroupStyles;
}

export type IUiRadioGroupEmits = {
  (event: 'update:model', value: string): void;
}
