import { UiDropdownStyles } from '../dropdown/dropdown.types';

export interface UiSelectMultipleStyles {
  selectWrapper?: string;
  uiSelectDefault?: string;
  uiSelectInset?: string;
  selectError?: string;
  body?: string;
  label?: string;
  dropdown?: UiDropdownStyles;
  color?: string;
  error?: string;
}
