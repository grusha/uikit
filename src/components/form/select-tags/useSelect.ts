import {
  ComputedRef,
  computed,
  onBeforeUnmount,
  onMounted,
  ref,
  watch,
} from 'vue';
import { SelectTagsProps, SelectTagsEmits, IUiSelectTagsStyles } from './select.types';
import { SelectOption } from '../select/select.types';

export const useSelect = (props: SelectTagsProps, emits: SelectTagsEmits, proxyStyles: ComputedRef<IUiSelectTagsStyles | undefined>) => {
  const inputValue = ref(props.multiple ? '' : props.model[0]?.title ?? '');

  const isActive = ref(false);

  const isLoading = ref(false);

  const activeDropdownItem = ref(-1);

  const filteredDropdownItems = ref(props.options);

  const filterDropdownItems = () => {
    activeDropdownItem.value = -1;

    isActive.value = true;

    const filteredItems = props.options?.filter(
      (option) => option.title.toLowerCase().includes(inputValue?.value?.toLowerCase()?.trim()),
    );

    filteredDropdownItems.value = props.filterable ? filteredItems : props.options;

    return props.filterable ? filteredItems : props.options;
  };

  const placeholderValue = computed(() => (props.disabled ? '' : props.placeholder));

  const dropdownItems = computed(() => (props.filterable ? props.options?.filter(
    (option) => option.title.toLowerCase().includes(inputValue?.value?.toLowerCase()?.trim()),
  ) : props.options));

  const canAddNewDropdownOption = computed(() => props.expandable && props.options?.length
  && !props.options.map((option) => option.title.toLowerCase().trim()).includes(inputValue.value.toLowerCase().trim())
  && inputValue.value.trim().length > 0);

  const selectedOptionsIdList = computed(() => {
    if (!props.model || !props.model.length) {
      return [];
    }

    const modelIdList = props.model?.map((option) => option.id);

    return props.options?.filter((option) => modelIdList.includes(option.id)).map((filteredOption) => filteredOption.id);
  });

  const onBlur = () => {
    isActive.value = false;

    activeDropdownItem.value = -1;
  };

  const onFocusin = () => {
    isActive.value = true;
  };

  const onEnter = () => {
    if (props.disabled) return;
    const currentFocusedElement = document.activeElement;

    const focusedItem = document.querySelector(`.${proxyStyles.value?.dropdownOptionFocused}` ?? '.ui-select__dropdown-item_option--focused');

    if ((currentFocusedElement?.id === `${props.id}__select-wrapper`
      || currentFocusedElement?.id === `${props.id}__select-input`)
      && !focusedItem) {
      isActive.value = !isActive.value;
    }

    if (inputValue.value && !focusedItem) {
      if (props.expandable && !props.options?.map((option) => option.title.toLocaleLowerCase().trim()).includes(inputValue.value.toLocaleLowerCase().trim())) {
        emits('update:options', {
          collectionName: props.options?.length ? props.options[0].collectionName : props.collectionName,
          title: inputValue.value.trim(),
        });
        isLoading.value = true;
      } else if (props.options?.map((option) => option.title.toLocaleLowerCase().trim()).includes(inputValue.value.toLocaleLowerCase().trim())) {
        const selectedItems = props.multiple ? [...props.model] : [];

        const option = props.options?.find((dropdownOption) => dropdownOption.title.toLocaleLowerCase().trim() === inputValue.value.toLocaleLowerCase().trim());

        if (!option) return;

        selectedItems.push(option);
        emits('update:model', selectedItems);

        if (props.multiple) {
          inputValue.value = '';
        }
      }
    }

    if (focusedItem) {
      let selectedItems = props.multiple ? [...props.model] : [];

      if (!selectedItems.map((item) => `${item.id}`).includes(focusedItem.id)) {
        const elementToPush = props.options?.find((option) => `${option.id}` === focusedItem.id);

        if (!elementToPush) return;

        selectedItems.push(elementToPush);
      } else {
        selectedItems = selectedItems.filter((option) => `${option.id}` !== focusedItem.id);
      }

      emits('update:model', selectedItems);

      focusedItem.classList.remove(`.${proxyStyles.value?.dropdownOptionFocused}` ?? '.ui-select__dropdown-item_option--focused');

      activeDropdownItem.value = -1;

      if (props.multiple) {
        inputValue.value = '';
      }

      if (!props.multiple) {
        isActive.value = false;
      }
    }
  };

  const onDelete = (id?: string) => {
    if (id) {
      emits('update:model', props.model.filter((selectedItem) => selectedItem.id !== id));
    } else {
      if (inputValue.value.length || !props.model.length || props.disabled) return;
      emits('update:model', props.model.slice(0, props.model.length - 1));
    }
  };

  const onEscape = () => {
    if (props.disabled) return;

    activeDropdownItem.value = -1;
    isActive.value = false;
  };

  const onSelect = (option: SelectOption) => {
    let selectedItems = props.multiple ? [...props.model] : [];

    if (props.model.map((item) => item.id).includes(option.id)) {
      selectedItems = selectedItems.filter((selectedItem) => selectedItem.id !== option.id);
    } else {
      selectedItems.push(option);
    }

    emits('update:model', selectedItems);

    if (!props.multiple) {
      filteredDropdownItems.value = props.options;
      isActive.value = false;
    } else {
      document.getElementById(`${props.id}__select-wrapper`)?.focus();
    }

    inputValue.value = '';
  };

  const detectClick = (event: Event) => {
    const selectWrapper = document.getElementById(`${props.id}__select-wrapper`) as HTMLElement;
    const listbox = document.getElementById(`${props.id}__listbox`) as HTMLElement;
    const newOptionInputWrapper = document.getElementById(`${props.id}__newOptionWrapper`) as HTMLLIElement;

    const withinBoundaries = event.composedPath().includes(listbox)
      || event.composedPath().includes(selectWrapper)
      || event.composedPath().includes(newOptionInputWrapper);

    if (!withinBoundaries) {
      onBlur();
    }
  };

  const detectFocusIn = (event: Event) => {
    const select = document.getElementById(`${props.id}__select`) as HTMLElement;

    if (!event.composedPath().includes(select)) {
      onBlur();
    }

    const eventTarget = event.target as HTMLElement;

    if (eventTarget.id === `${props.id}__select-wrapper` && (props.filterable || props.expandable)) {
      document.getElementById(`${props.id}__select-input`)?.focus();
      emits('focus');
    }
  };

  const setScrollBehavior = (event: KeyboardEvent) => {
    const scrollHandlers = ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];

    if (scrollHandlers.includes(event.code)) {
      event.preventDefault();
    }
  };

  watch(() => props.options, () => {
    // const newOption = props.options?.length
    //   ? props.options.find((option) => option.title.toLowerCase() === inputValue.value.toLowerCase().trim())
    //   : undefined;

    // if (!newOption) return;

    // const selectedItems = props.multiple ? [...props.model] : [];

    // selectedItems.push(newOption);

    // emits('update:model', selectedItems);

    inputValue.value = '';

    filteredDropdownItems.value = props.options;

    document.getElementById(`${props.id}__select-input`)?.focus();

    isLoading.value = false;
  }, { deep: true });

  watch(() => isActive.value, () => {
    if (isActive.value) {
      document.addEventListener('keydown', setScrollBehavior, false);
    } else {
      document.removeEventListener('keydown', setScrollBehavior);
    }
  });

  watch(() => props.model, async () => {
    if (!props.multiple) {
      inputValue.value = props.model[0]?.title ?? '';
    }

    filteredDropdownItems.value = props.options;
  });

  onMounted(() => {
    if (!props.disabled) {
      document.addEventListener('click', detectClick);
      document.addEventListener('focusin', detectFocusIn);
    }
  });

  onBeforeUnmount(() => {
    document.removeEventListener('click', detectClick);
    document.removeEventListener('focusin', detectFocusIn);
    document.removeEventListener('keydown', setScrollBehavior);
  });

  return {
    inputValue,
    isActive,
    dropdownItems,
    filteredDropdownItems,
    selectedOptionsIdList,
    activeDropdownItem,
    canAddNewDropdownOption,
    isLoading,
    placeholderValue,
    onSelect,
    onBlur,
    onFocusin,
    onEnter,
    onEscape,
    onDelete,
    filterDropdownItems,
  };
};
