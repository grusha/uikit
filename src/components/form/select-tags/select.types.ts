import { RawSelectOption, SelectOption } from "../select/select.types";


export type SelectTagsIcon = {
  name: string;
  color?: string;
  width?: number;
  height?: number;
}

export type SelectTagsTooltip = {
  icon: SelectTagsIcon;
  text: string;
  options: any;
}

export interface IUiSelectTagsStyles {
  wrapper?: string;
  labelContainer?: string;
  label?: string;
  tooltipIcon?: string;
  tooltipText?: string;
  select?: string;
  selectError?: string;
  selectContent?: string;
  selectDisabled?: string;
  selectValue?: string;
  selectInput?: string;
  selectInputDisabled?: string;
  tagsContainer?: string;
  tag?: string;
  dropdownWrapper?: string;
  dropdownFooter?: string;
  dropdownItem?: string;
  dropdownOption?: string;
  dropdownOptionFocused?: string;
  dropdownText?: string;
  dropdownList?: string;
  selectHr?: string;
  errorMessage?: string;
  color?: string;
}

export type SelectTagsProps = {
  name: string;
  id: string;
  model: SelectOption[];
  defaultValue?: SelectOption[];
  options?: SelectOption[];
  label?: string;
  collectionName: string;
  placeholder?: string;
  errorMessage?: string;
  disabled?: boolean;
  filterable?: boolean;
  expandable?: boolean;
  multiple?: boolean;
  tooltip?: SelectTagsTooltip;
  dropdownIcon?: SelectTagsIcon;
  selectTagsStyles?: IUiSelectTagsStyles;
}

export type SelectTagsEmits = {
  (event: 'update:options', payload: RawSelectOption): void;
  (event: 'update:model', payload: SelectOption[]): void;
  (event: 'focus'): void;
}
