import { onBeforeUnmount, watch } from 'vue';
import { Editor } from '@tiptap/vue-3';
import StarterKit from '@tiptap/starter-kit';
import Link from '@tiptap/extension-link';
import Placeholder from '@tiptap/extension-placeholder';

const _setLink = (editor: any) => {
  const previousUrl = editor.getAttributes('link').href;
  const url = window.prompt('URL', previousUrl);

  // cancelled
  if (url === null) {
    return;
  }

  // empty
  if (url === '') {
    editor
      .chain()
      .focus()
      .extendMarkRange('link')
      .unsetLink()
      .run();

    return;
  }

  // update link
  editor
    .chain()
    .focus()
    .extendMarkRange('link')
    .setLink({ href: url, target: '_blank' })
    .run();
};

export const useEditor = (props: any, emit: any) => {
  const editor = new Editor({
    content: props.model,
    editable: !props.disabled,
    editorProps: {
      attributes: {
        class: 'ui-wysiwyg__content body-m',
      },
    },
    extensions: [
      StarterKit,
      Link.configure({
        openOnClick: false,
      }),
      Placeholder.configure({
        placeholder: props.placeholder,
      }),
    ],
  });

  editor.on('update', () => {
    let html = editor.getHTML();

    if (html === '<p></p>') html = '';

    emit('update:model', html);
  });

  onBeforeUnmount(() => {
    editor.destroy();
  });

  watch(() => props.model, (value) => {
    const isTheSame = editor.getHTML() === value;

    if (isTheSame) {
      return;
    }

    editor.commands.setContent(value, false);
  });

  const setLink = () => _setLink(editor);

  const isActive = (name: string) => editor.isActive(name);

  return { editor, setLink, isActive };
};
