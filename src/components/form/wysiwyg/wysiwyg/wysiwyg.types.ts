export default interface WysiwygStyles{
    wysiwyg?: string;
    label?: string;
    labelSpan?: string;
    container?: string;
    containerError?: string;
}
