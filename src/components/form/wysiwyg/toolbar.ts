import { ToolbarButtons } from './index.types';

const toolbarButtons: ToolbarButtons[] = [
  {
    as: 'button',
    type: 'bold',
    iconName: 'text-bold',
  },
  {
    as: 'button',
    type: 'italic',
    iconName: 'text-italic',
  },
  {
    as: 'space',
    type: 'none',
  },
  {
    as: 'button',
    type: 'bulletList',
    iconName: 'bulleted-list',
  },
  {
    as: 'button',
    type: 'orderedList',
    iconName: 'numbered-list',
  },
  {
    as: 'space',
    type: 'none',
  },
  {
    as: 'button',
    type: 'link',
    iconName: 'link',
  },
];

export default toolbarButtons;
