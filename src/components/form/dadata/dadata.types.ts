export default interface UiDadataStyles {
  dadata?: string;
  label?: string;
  required?: string;
}
