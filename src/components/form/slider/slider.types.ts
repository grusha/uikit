export interface UiSliderStyles {
  slider?: string;
  label?: string;
  dataLabel?: string;
  customSlider?: string;
}
