import { computed } from 'vue';

const useSelect = (props: any, emits: any) => {
  const selectedIndex = computed(() => {
    if (props.model === null) {
      return props.options
        .findIndex((item: any) => (item.title ? item.title === props.defaultValue : item === props.defaultValue));
    }

    return props.options
      .findIndex((item: any) => (item.title ? item.title === props.model?.title : item === props.model));
  });

  const dropdownItems = computed(() => props.options.map((option: any) => option.title ?? option));

  const onSelect = (index: number) => {
    emits('update:model', props.options[index]);
  };

  return {
    selectedIndex,
    dropdownItems,
    onSelect,
  };
};

export {
  useSelect,
};
