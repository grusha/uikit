export interface UiInputTagsStyles {
  inputTags?: string;
  inputTagsLabel?: string;
  inputTagsWrapper?: string;
  inputTagsTag?: string;
  inputTagsInput?: string;
  inputTagsInputError?: string;
  inputTagsError?: string;
  inputTagsIcon?: string;
}
