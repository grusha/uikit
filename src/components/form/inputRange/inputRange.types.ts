import UiInputStyles from '../input/input.types';

export interface UiInputRangeStyles {
  inputRange?: string;
  wrapper?: string;
  input?: UiInputStyles;
  label?: string;
  error?: string;
}

type Input = {
  as?: string;
  disabled?: boolean;
  initialValue?: string | number;
  placeholder?: string;
  label?: string;
  name?: string;
  copy?: string;
}

export interface InputField {
  from: Input;
  to: Input;
}

export interface InputModel {
  from: number | null;
  to: number | null;
}
