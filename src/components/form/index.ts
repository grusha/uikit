import UiCheckboxStyles from './checkbox/checkbox.types';
import { Checklist } from './checklist';
import UiDadataStyles from './dadata/dadata.types';
import UiDatePickerStyles from './datePicker/datepicker.types';
import UiDateRangeStyles from './dateRange/daterange.types';
import { UiDropdownStyles } from './dropdown/dropdown.types';
import { UiFieldStyles } from './field/field.types';
import UiInputStyles from './input/input.types';
import { UiInputBigStyles } from './inputBig/inputBig.types';
import { UiInputRangeStyles } from './inputRange/inputRange.types';
import { UiRadioStyles } from './radio/radio.types';
import { IUiSelectStyles } from './select/select.types';
import { UiSelectMultipleStyles } from './selectMultiple/select.multiple.types';
import { UiSwitchStyles } from './switch/switch.types';
import { UiInputTagsStyles } from './input-tags/input-tags.types';
import { UiWysiwyg } from './wysiwyg/index.types';
import { IUiRadioGroupStyles } from './radio-group/radio-group.types';
import { UiSliderStyles } from './slider/slider.types';
import { IUiSelectTagsStyles } from './select-tags/select.types';

export interface Form {
  checklist: Checklist;
  wysiwyg: UiWysiwyg;
  uiCheckbox: UiCheckboxStyles;
  uiDadata: UiDadataStyles;
  uiDateRange: UiDateRangeStyles;
  uiDatePicker: UiDatePickerStyles;
  uiDropdown: UiDropdownStyles;
  uiField: UiFieldStyles;
  uiInput: UiInputStyles;
  uiInputBig: UiInputBigStyles;
  uiInputRange: UiInputRangeStyles;
  uiRadio: UiRadioStyles;
  uiSelect: IUiSelectStyles;
  uiSelectTags: IUiSelectTagsStyles;
  uiSelectMultiple: UiSelectMultipleStyles,
  uiSwitch: UiSwitchStyles;
  uiInputTags: UiInputTagsStyles;
  uiRadioGroup: IUiRadioGroupStyles;
  uiSlider: UiSliderStyles;
}
