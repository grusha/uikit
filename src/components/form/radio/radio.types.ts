export interface UiRadioStyles {
  wrapper: string;
  checked: string;
  input: string;
  checkmark: string;
  body: string;
  direction: string;
  content: string;
  radio: string;
}
