import { UiDropdownStyles } from '../dropdown/dropdown.types';

export type RawSelectOption = {
  collectionName: string;
  title: string;
}

export interface SelectOption extends RawSelectOption {
  id: string
}

export type IUiSelectTooltipIcon = {
  name: string;
  color?: string;
  width?: number;
  height?: number;
}

export type IUiSelectTooltip = {
  icon: IUiSelectTooltipIcon;
  text: string;
  options: any;
}

export interface IUiSelectStyles {
  selectBig?: string;
  label?: string;
  required?: string;
  selectDefault?: string;
  selectError?: string;
  input?: string;
  inputDisabled?: string;
  body?: string;
  dropdown?: UiDropdownStyles;
  selectValue?: string;
  error?: string;
  color?: string;
  tooltipText?: string;
  tooltipIcon?: string;
}

export type IUiSelectProps = {
  model?: any;
  errorMessage?: string;
  options?: SelectOption[];
  name: string;
  placeholder?: string | number;
  label?: string | number;
  inset?: boolean;
  disabled?: boolean;
  filterable?: boolean;
  expandable?: boolean;
  multiple?: boolean;
  tooltip?: IUiSelectTooltip;
  selectStyles?: IUiSelectStyles;
  collectionName?: string;
}

export type IUiSelectEmits = {
  (event: 'update:model', value: SelectOption): void;
  (event: 'update:options', value: { title: string, collectionName: string }): void;
  (event: 'onFocus'): void;
}
