import { InjectionKey } from 'vue';
import { UiAchievementStyles } from './achievement/achievement.types';
import { UiAvatarStyles } from './avatar/avatar.types';
import { UiBreadcrumbsStyles } from './breadcrumbs/breadcrumbs.types';
import { Button } from './button';
import { UiCardStyles } from './card/card.types';
import { uiCollapsibleStyles } from './collapsible/collapsible.types';
import { Dropzone } from './dropzone';
import { Form } from './form';
import { UiImageStyles } from './image/image.types';
import { UiPaginationStyles } from './pagination/pagination.types';
import { UiPreloaderStyles } from './preloader/preloader.types';
import { UiRoutesStyles } from './routes/routes.types';
import { UiSearchStyles } from './search/search.types';
import { DropdownButtonStyles } from './button/dropdownButton/dropdownButton.types';
import UiButtonDropdownStyles from './button-dropdown/button-dropdown.types';
import { PopupSelectStyles } from './popup/popup-select.types';

export interface Styles {
  components: {
    button: Button;
    dropdownButton: DropdownButtonStyles;
    buttonDropdown: UiButtonDropdownStyles;
    dropzone: Dropzone;
    form: Form;
    uiAchievement: UiAchievementStyles;
    uiAvatar: UiAvatarStyles;
    uiCard: UiCardStyles;
    uiImage: UiImageStyles;
    uiPagination: UiPaginationStyles;
    uiPreloader: UiPreloaderStyles;
    uiRoutes: UiRoutesStyles;
    uiSearch: UiSearchStyles;
    uiCollapsible: uiCollapsibleStyles;
    uiBreadcrumbs: UiBreadcrumbsStyles;
    popupSelect: PopupSelectStyles;
  }
}

export type {
  UiAchievementStyles,
  UiAvatarStyles,
  UiBreadcrumbsStyles,
  UiCardStyles,
  UiImageStyles,
  UiPaginationStyles,
  UiPreloaderStyles,
  UiRoutesStyles,
  UiSearchStyles,
  uiCollapsibleStyles,
  UiButtonDropdownStyles,
};

// eslint-disable-next-line symbol-description
export const keyStyles = Symbol() as InjectionKey<Styles>;
