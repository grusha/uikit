type PhotoType = {
    url?: string,
};

export interface UiAvatarStyles{
    avatarDefault: string,
    avatarAdmin: string,
}

export interface DefaultAvatarSize{
    width: number,
    height: number,
}

export interface AccountInfo{
    firstName: string,
    middleName: string,
    lastName: string,
    gender: string,
    photo?: PhotoType | string | undefined
}
