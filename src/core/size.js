export const INDENT_4 = 4;
export const INDENT_8 = 8;
export const INDENT_16 = 16;
export const INDENT_24 = 24;
export const INDENT_32 = 32;
export const INDENT_80 = 80;
export const INDENT_120 = 120;
