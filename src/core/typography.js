export const DESKTOP_FONT_SIZE = {
  S: 12,
  M: 14,
  L: 16,
  M_BOLD: 14,
  L_BOLD: 16,
  H_5: 20,
  H_4: 24,
  H_3: 32,
  H_2: 40,
  H_1: 48,
};

export const DESKTOP_LINE_HEIGHT = {
  S: 16,
  M: 20,
  L: 24,
  M_BOLD: 20,
  L_BOLD: 24,
  H_5: 28,
  H_4: 32,
  H_3: 40,
  H_2: 48,
  H_1: 56,
};
