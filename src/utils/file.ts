import { isRef, Ref } from 'vue';
import { saveAs } from 'file-saver';

export const formatFileSize = (bytes = 0, decimals = 0) => {
  if (bytes === 0) return '0 B';
  const k = 1024;
  const dm = decimals || 2;
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
};

export const dataURLtoFile = (dataUrl: string, fileName: string) => {
  const arr = dataUrl.split(',');
  const mime = arr[0].match(/:(.*?);/)![1];
  const bstr = atob(arr[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);

  // eslint-disable-next-line no-plusplus
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], fileName, { type: mime });
};

export const getExt = (fileName: string) => fileName?.split('.').pop();

export const downloadFile = async (url: string, fileName: string | undefined) => {
  const res = await fetch(url, { mode: 'cors' });
  const blob = await res.blob();

  saveAs(blob, fileName);
};

export const prepareFile = (file:
  Ref<{ _id: string, originalName: string, size: number, url: string, thumbnailUrl: string }> |
  { _id: string, originalName: string, size: number, url: string, thumbnailUrl: string }) => ({
  _id: isRef(file) ? file.value._id : file._id,
  originalName: isRef(file) ? file.value.originalName : file.originalName,
  ext: isRef(file) ? getExt(file.value.originalName) : getExt(file.originalName),
  size: isRef(file) ? formatFileSize(file.value.size) : formatFileSize(file.size),
  url: isRef(file) ? file.value.url : file.url,
  thumbnailUrl: isRef(file) ? file.value.thumbnailUrl : file.thumbnailUrl,
});
