export interface LazyOptions {
    error?: string;
    loading?: string;
    observerOptions?: IntersectionObserverInit;
    log?: boolean;
    // eslint-disable-next-line no-use-before-define
    lifecycle?: Lifecycle;
}

export interface ValueFormatterObject {
    src: string,
    error?: string,
    loading?: string,
    // eslint-disable-next-line no-use-before-define
    lifecycle?: Lifecycle;
}

// eslint-disable-next-line no-shadow
export enum LifecycleEnum {
    LOADING = 'loading',
    LOADED = 'loaded',
    ERROR = 'error'
}

export type Lifecycle = {
    [x in LifecycleEnum]?: (el?:HTMLElement) => void;
};
