import { set } from 'lodash-es';
import { FEMALE_GENDER_NAME, MALE_GENDER_NAME  } from './gender-detector/gender-definitions';
import { FEMALE_DEFAULT_AVATAR_ICON_NAME, MALE_DEFAULT_AVATAR_ICON_NAME } from '../config';
import { detectGender } from './gender-detector';

export const resolveDefaultProfilePic = ({
  firstName, middleName, lastName, gender,
}: { firstName: string, middleName: string, lastName: string, gender: string }) => {
  if (gender === FEMALE_GENDER_NAME) {
    return FEMALE_DEFAULT_AVATAR_ICON_NAME;
  }
  if (gender === MALE_GENDER_NAME) {
    return MALE_DEFAULT_AVATAR_ICON_NAME;
  }
  const detectedGenderByName = detectGender(firstName, middleName, lastName);

  if (detectedGenderByName === FEMALE_GENDER_NAME) {
    return FEMALE_DEFAULT_AVATAR_ICON_NAME;
  }

  return MALE_DEFAULT_AVATAR_ICON_NAME;
};

export const ensureProfileHasPhoto = (profile: any) => {
  if (profile?.photo?.url) return;
  set(profile, 'photo.url', resolveDefaultProfilePic(profile));
};
