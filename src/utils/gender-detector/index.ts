import { isNil } from 'lodash-es';
import {
  FEMALE,
  MALE,
  LAST_NAME,
  MIDDLE_NAME,
  LAST_NAME_COMPLETIONS,
  NAMES,
  MIDDLE_NAME_COMPLETIONS,
  Gender,
  MALE_GENDER_NAME,
  FEMALE_GENDER_NAME
} from './gender-definitions';

const normalizeNamePart = (partOfFullName: string) => {
  if (isNil(partOfFullName)) {
    return '';
  }

  return partOfFullName.toLowerCase().replace(/\s/g, '');
};

const equalCompletion = (partOfFullName: string, completion: string) => partOfFullName
  .slice(-completion.length) === completion;

const isPopularName = (firstName: string, gender: Gender) => {
  const names = NAMES[gender];
  let result = false;

  names.forEach((name: string) => {
    if (firstName === name) {
      result = true;
    }
  });

  return result;
};

const genderByFirstName = (firstName: string) => {
  if (isPopularName(firstName, FEMALE)) {
    return FEMALE;
  }
  if (isPopularName(firstName, MALE)) {
    return MALE;
  }

  return null;
};

const isSuitablePart = (partOfFullName: string, type: number, gender: Gender) => {
  let completions;
  let result = false;

  switch (type) {
    case LAST_NAME:
      completions = LAST_NAME_COMPLETIONS[gender];
      break;
    case MIDDLE_NAME:
      completions = MIDDLE_NAME_COMPLETIONS[gender];
      break;
    default:
      return false;
  }

  completions.forEach((completion: string) => {
    if (equalCompletion(partOfFullName, completion)) {
      result = true;
    }
  });

  return result;
};

const genderByField = (type: number, partOfFullName: string) => {
  if (isSuitablePart(partOfFullName, type, FEMALE)) {
    return FEMALE;
  }
  if (isSuitablePart(partOfFullName, type, MALE)) {
    return MALE;
  }

  return null;
};

const determineGender = (detectedGenders: (string | null)[]) => {
  let male = false;
  let female = false;

  detectedGenders.forEach((gender: any) => {
    if (gender === MALE) {
      male = true;
    }
    if (gender === FEMALE) {
      female = true;
    }
  });

  if (male && !female) {
    return MALE_GENDER_NAME;
  }
  if (!male && female) {
    return FEMALE_GENDER_NAME;
  }

  return null;
};

/**
 * @return {Number} 0 - female, 1 - male, null - can't detect
 */
export const detectGender = (firstName: string, middleName: string, lastName: string) => {
  firstName = normalizeNamePart(firstName);
  middleName = normalizeNamePart(middleName);
  lastName = normalizeNamePart(lastName);
  const detectedGenders = [
    genderByField(LAST_NAME, lastName),
    genderByFirstName(firstName),
    genderByField(MIDDLE_NAME, middleName),
  ];

  return determineGender(detectedGenders);
};
