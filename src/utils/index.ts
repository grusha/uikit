import { Ref } from 'vue';
import {
  omitBy,
  reduce,
} from 'lodash-es';
import sanitizeHtml from 'sanitize-html';

export * from './avatar.resolver';
export * from './file';
// export * from './sudir';
export * from './gender-detector';
// export * from './pagination';
// export * from './notifications';
// export * from './format-date';
// export * from './statuses';

// export const toOption = (options: any) => {
//   const optionsArray = isObject(options)
//     ? Object.values(options)
//     : options;

//   return optionsArray.map((item: any) => {
//     if (item === null) {
//       // eslint-disable-next-line no-console
//       console.error('options', options);
//       throw new Error('item is null');
//     }

//     return isString(item)
//       ? { text: item, value: item }
//       : { text: item?.shortTitle || item?.title, value: item._id };
//   });
// };

export const getFIO = (profile: {lastName: string, firstName: string, middleName: string}) => {
  if (!profile) return '';

  return `${profile.lastName} ${profile.firstName} ${profile.middleName}`;
};

export const getGetObjDifference = (obj1: any, obj2: any, initialValue = {}) => reduce(obj2, (ac: any, value: any, key: any) => {
  if (obj1[key] !== value) ac[key] = value;

  return ac;
}, initialValue);

export const difference = (source: any, target: any) => omitBy(target, (v: any, k: any) => source[k] === v);

export const sanitize = (html: string) => sanitizeHtml(html, {
  allowedTags: ['b', 'i', 'a', 'p', 'div', 'ul', 'ol', 'li'],
  allowedAttributes: {
    a: ['href'],
  },
});

export const vHidden = {
  onMounted(el: HTMLElement, binding: Ref<boolean>) {
    el.style.visibility = binding.value ? 'hidden' : 'visible';
  },
};
