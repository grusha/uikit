# UI-KIT

## Installation

```sh
# 1 step
npm i playwright
# 2 step
npm i
```

## Starting

```sh
npm run storybook
```

## Testing

```sh
# unit tests
npm run test:dev

# ui tests
npm run storybook
npm run test-storybook
```

## Building

```sh
npm run build
```

## Create new release

```sh
# for patches
npm run patch
# for minor updates
npm run minor
# for major
npm run major
```
