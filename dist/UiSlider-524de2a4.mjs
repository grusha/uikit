import Qt, { defineComponent as te, inject as ee, computed as xt, openBlock as Pt, createElementBlock as wt, normalizeClass as et, toDisplayString as Ot, createCommentVNode as re, createVNode as ne, unref as oe, withCtx as ie, createElementVNode as ae } from "vue";
import { o as se, p as le, k as ue } from "./index-381482a8.mjs";
var Dt = { exports: {} };
(function(rt, dt) {
  (function(X, q) {
    rt.exports = q(Qt);
  })(typeof self < "u" ? self : le, function(X) {
    return function() {
      var q = { 388: function(u, h) {
        var s, o, c;
        (function(g, k) {
          o = [], s = k, c = typeof s == "function" ? s.apply(h, o) : s, c === void 0 || (u.exports = c);
        })(typeof self < "u" && self, function() {
          function g() {
            var k = Object.getOwnPropertyDescriptor(document, "currentScript");
            if (!k && "currentScript" in document && document.currentScript || k && k.get !== g && document.currentScript)
              return document.currentScript;
            try {
              throw new Error();
            } catch (W) {
              var w, z, A, L = /.*at [^(]*\((.*):(.+):(.+)\)$/gi, B = /@([^@]*):(\d+):(\d+)\s*$/gi, m = L.exec(W.stack) || B.exec(W.stack), I = m && m[1] || !1, F = m && m[2] || !1, H = document.location.href.replace(document.location.hash, ""), C = document.getElementsByTagName("script");
              I === H && (w = document.documentElement.outerHTML, z = new RegExp("(?:[^\\n]+?\\n){0," + (F - 2) + "}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*", "i"), A = w.replace(z, "$1").trim());
              for (var x = 0; x < C.length; x++)
                if (C[x].readyState === "interactive" || C[x].src === I || I === H && C[x].innerHTML && C[x].innerHTML.trim() === A)
                  return C[x];
              return null;
            }
          }
          return g;
        });
      }, 905: function(u, h, s) {
        s.r(h);
        var o = s(117), c = s.n(o), g = s(488), k = s.n(g), w = k()(c());
        w.push([u.id, ".vue-slider-dot{position:absolute;-webkit-transition:all 0s;transition:all 0s;z-index:5}.vue-slider-dot:focus{outline:none}.vue-slider-dot-tooltip{position:absolute;visibility:hidden}.vue-slider-dot-hover:hover .vue-slider-dot-tooltip,.vue-slider-dot-tooltip-show{visibility:visible}.vue-slider-dot-tooltip-top{top:-10px;left:50%;-webkit-transform:translate(-50%,-100%);transform:translate(-50%,-100%)}.vue-slider-dot-tooltip-bottom{bottom:-10px;left:50%;-webkit-transform:translate(-50%,100%);transform:translate(-50%,100%)}.vue-slider-dot-tooltip-left{left:-10px;top:50%;-webkit-transform:translate(-100%,-50%);transform:translate(-100%,-50%)}.vue-slider-dot-tooltip-right{right:-10px;top:50%;-webkit-transform:translate(100%,-50%);transform:translate(100%,-50%)}", ""]), h.default = w;
      }, 121: function(u, h, s) {
        s.r(h);
        var o = s(117), c = s.n(o), g = s(488), k = s.n(g), w = k()(c());
        w.push([u.id, ".vue-slider-marks{position:relative;width:100%;height:100%}.vue-slider-mark{position:absolute;z-index:1}.vue-slider-ltr .vue-slider-mark,.vue-slider-rtl .vue-slider-mark{width:0;height:100%;top:50%}.vue-slider-ltr .vue-slider-mark-step,.vue-slider-rtl .vue-slider-mark-step{top:0}.vue-slider-ltr .vue-slider-mark-label,.vue-slider-rtl .vue-slider-mark-label{top:100%;margin-top:10px}.vue-slider-ltr .vue-slider-mark{-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.vue-slider-ltr .vue-slider-mark-step{left:0}.vue-slider-ltr .vue-slider-mark-label{left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}.vue-slider-rtl .vue-slider-mark{-webkit-transform:translate(50%,-50%);transform:translate(50%,-50%)}.vue-slider-rtl .vue-slider-mark-step{right:0}.vue-slider-rtl .vue-slider-mark-label{right:50%;-webkit-transform:translateX(50%);transform:translateX(50%)}.vue-slider-btt .vue-slider-mark,.vue-slider-ttb .vue-slider-mark{width:100%;height:0;left:50%}.vue-slider-btt .vue-slider-mark-step,.vue-slider-ttb .vue-slider-mark-step{left:0}.vue-slider-btt .vue-slider-mark-label,.vue-slider-ttb .vue-slider-mark-label{left:100%;margin-left:10px}.vue-slider-btt .vue-slider-mark{-webkit-transform:translate(-50%,50%);transform:translate(-50%,50%)}.vue-slider-btt .vue-slider-mark-step{top:0}.vue-slider-btt .vue-slider-mark-label{top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.vue-slider-ttb .vue-slider-mark{-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.vue-slider-ttb .vue-slider-mark-step{bottom:0}.vue-slider-ttb .vue-slider-mark-label{bottom:50%;-webkit-transform:translateY(50%);transform:translateY(50%)}.vue-slider-mark-label,.vue-slider-mark-step{position:absolute}", ""]), h.default = w;
      }, 207: function(u, h, s) {
        s.r(h);
        var o = s(117), c = s.n(o), g = s(488), k = s.n(g), w = k()(c());
        w.push([u.id, ".vue-slider{position:relative;-webkit-box-sizing:content-box;box-sizing:content-box;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;display:block;-webkit-tap-highlight-color:rgba(0,0,0,0)}.vue-slider-rail{position:relative;width:100%;height:100%;-webkit-transition-property:width,height,left,right,top,bottom;transition-property:width,height,left,right,top,bottom}.vue-slider-process{position:absolute;z-index:1}", ""]), h.default = w;
      }, 488: function(u) {
        u.exports = function(h) {
          var s = [];
          return s.toString = function() {
            return this.map(function(o) {
              var c = "", g = typeof o[5] < "u";
              return o[4] && (c += "@supports (".concat(o[4], ") {")), o[2] && (c += "@media ".concat(o[2], " {")), g && (c += "@layer".concat(o[5].length > 0 ? " ".concat(o[5]) : "", " {")), c += h(o), g && (c += "}"), o[2] && (c += "}"), o[4] && (c += "}"), c;
            }).join("");
          }, s.i = function(o, c, g, k, w) {
            typeof o == "string" && (o = [[null, o, void 0]]);
            var z = {};
            if (g)
              for (var A = 0; A < this.length; A++) {
                var L = this[A][0];
                L != null && (z[L] = !0);
              }
            for (var B = 0; B < o.length; B++) {
              var m = [].concat(o[B]);
              g && z[m[0]] || (typeof w < "u" && (typeof m[5] > "u" || (m[1] = "@layer".concat(m[5].length > 0 ? " ".concat(m[5]) : "", " {").concat(m[1], "}")), m[5] = w), c && (m[2] && (m[1] = "@media ".concat(m[2], " {").concat(m[1], "}")), m[2] = c), k && (m[4] ? (m[1] = "@supports (".concat(m[4], ") {").concat(m[1], "}"), m[4] = k) : m[4] = "".concat(k)), s.push(m));
            }
          }, s;
        };
      }, 117: function(u) {
        u.exports = function(h) {
          return h[1];
        };
      }, 831: function(u, h) {
        h.Z = (s, o) => {
          const c = s.__vccOpts || s;
          for (const [g, k] of o)
            c[g] = k;
          return c;
        };
      }, 466: function(u, h, s) {
        var o = s(905);
        o.__esModule && (o = o.default), typeof o == "string" && (o = [[u.id, o, ""]]), o.locals && (u.exports = o.locals);
        var c = s(959).Z;
        c("50bc1720", o, !0, { sourceMap: !1, shadowMode: !1 });
      }, 18: function(u, h, s) {
        var o = s(121);
        o.__esModule && (o = o.default), typeof o == "string" && (o = [[u.id, o, ""]]), o.locals && (u.exports = o.locals);
        var c = s(959).Z;
        c("10aa5f36", o, !0, { sourceMap: !1, shadowMode: !1 });
      }, 631: function(u, h, s) {
        var o = s(207);
        o.__esModule && (o = o.default), typeof o == "string" && (o = [[u.id, o, ""]]), o.locals && (u.exports = o.locals);
        var c = s(959).Z;
        c("1772934e", o, !0, { sourceMap: !1, shadowMode: !1 });
      }, 959: function(u, h, s) {
        function o(f, b) {
          for (var y = [], v = {}, p = 0; p < b.length; p++) {
            var D = b[p], E = D[0], O = D[1], N = D[2], T = D[3], _ = { id: f + ":" + p, css: O, media: N, sourceMap: T };
            v[E] ? v[E].parts.push(_) : y.push(v[E] = { id: E, parts: [_] });
          }
          return y;
        }
        s.d(h, { Z: function() {
          return F;
        } });
        var c = typeof document < "u";
        if (typeof DEBUG < "u" && DEBUG && !c)
          throw new Error("vue-style-loader cannot be used in a non-browser environment. Use { target: 'node' } in your Webpack config to indicate a server-rendering environment.");
        var g = {}, k = c && (document.head || document.getElementsByTagName("head")[0]), w = null, z = 0, A = !1, L = function() {
        }, B = null, m = "data-vue-ssr-id", I = typeof navigator < "u" && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase());
        function F(f, b, y, v) {
          A = y, B = v || {};
          var p = o(f, b);
          return H(p), function(D) {
            for (var E = [], O = 0; O < p.length; O++) {
              var N = p[O], T = g[N.id];
              T.refs--, E.push(T);
            }
            for (D ? (p = o(f, D), H(p)) : p = [], O = 0; O < E.length; O++)
              if (T = E[O], T.refs === 0) {
                for (var _ = 0; _ < T.parts.length; _++)
                  T.parts[_]();
                delete g[T.id];
              }
          };
        }
        function H(f) {
          for (var b = 0; b < f.length; b++) {
            var y = f[b], v = g[y.id];
            if (v) {
              v.refs++;
              for (var p = 0; p < v.parts.length; p++)
                v.parts[p](y.parts[p]);
              for (; p < y.parts.length; p++)
                v.parts.push(x(y.parts[p]));
              v.parts.length > y.parts.length && (v.parts.length = y.parts.length);
            } else {
              var D = [];
              for (p = 0; p < y.parts.length; p++)
                D.push(x(y.parts[p]));
              g[y.id] = { id: y.id, refs: 1, parts: D };
            }
          }
        }
        function C() {
          var f = document.createElement("style");
          return f.type = "text/css", k.appendChild(f), f;
        }
        function x(f) {
          var b, y, v = document.querySelector("style[" + m + '~="' + f.id + '"]');
          if (v) {
            if (A)
              return L;
            v.parentNode.removeChild(v);
          }
          if (I) {
            var p = z++;
            v = w || (w = C()), b = U.bind(null, v, p, !1), y = U.bind(null, v, p, !0);
          } else
            v = C(), b = nt.bind(null, v), y = function() {
              v.parentNode.removeChild(v);
            };
          return b(f), function(D) {
            if (D) {
              if (D.css === f.css && D.media === f.media && D.sourceMap === f.sourceMap)
                return;
              b(f = D);
            } else
              y();
          };
        }
        var W = function() {
          var f = [];
          return function(b, y) {
            return f[b] = y, f.filter(Boolean).join(`
`);
          };
        }();
        function U(f, b, y, v) {
          var p = y ? "" : v.css;
          if (f.styleSheet)
            f.styleSheet.cssText = W(b, p);
          else {
            var D = document.createTextNode(p), E = f.childNodes;
            E[b] && f.removeChild(E[b]), E.length ? f.insertBefore(D, E[b]) : f.appendChild(D);
          }
        }
        function nt(f, b) {
          var y = b.css, v = b.media, p = b.sourceMap;
          if (v && f.setAttribute("media", v), B.ssrId && f.setAttribute(m, b.id), p && (y += `
/*# sourceURL=` + p.sources[0] + " */", y += `
/*# sourceMappingURL=data:application/json;base64,` + btoa(unescape(encodeURIComponent(JSON.stringify(p)))) + " */"), f.styleSheet)
            f.styleSheet.cssText = y;
          else {
            for (; f.firstChild; )
              f.removeChild(f.firstChild);
            f.appendChild(document.createTextNode(y));
          }
        }
      }, 927: function(u) {
        u.exports = X;
      } }, Z = {};
      function P(u) {
        var h = Z[u];
        if (h !== void 0)
          return h.exports;
        var s = Z[u] = { id: u, exports: {} };
        return q[u].call(s.exports, s, s.exports, P), s.exports;
      }
      (function() {
        P.n = function(u) {
          var h = u && u.__esModule ? function() {
            return u.default;
          } : function() {
            return u;
          };
          return P.d(h, { a: h }), h;
        };
      })(), function() {
        P.d = function(u, h) {
          for (var s in h)
            P.o(h, s) && !P.o(u, s) && Object.defineProperty(u, s, { enumerable: !0, get: h[s] });
        };
      }(), function() {
        P.o = function(u, h) {
          return Object.prototype.hasOwnProperty.call(u, h);
        };
      }(), function() {
        P.r = function(u) {
          typeof Symbol < "u" && Symbol.toStringTag && Object.defineProperty(u, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(u, "__esModule", { value: !0 });
        };
      }(), function() {
        P.p = "";
      }();
      var G = {};
      return function() {
        if (P.d(G, { default: function() {
          return Zt;
        } }), typeof window < "u") {
          var u = window.document.currentScript, h = P(388);
          u = h(), "currentScript" in document || Object.defineProperty(document, "currentScript", { get: h });
          var s = u && u.src.match(/(.+\/)[^/]+\.js(\?.*)?$/);
          s && (P.p = s[1]);
        }
        var o = P(927);
        function c(t, e, r) {
          return e in t ? Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = r, t;
        }
        var g = { key: 0, class: "vue-slider-marks" };
        function k(t, e, r, n, i, a) {
          var l = (0, o.resolveComponent)("vue-slider-mark"), j = (0, o.resolveComponent)("vue-slider-dot");
          return (0, o.openBlock)(), (0, o.createElementBlock)("div", (0, o.mergeProps)({ ref: "container", class: t.containerClasses, style: t.containerStyles, onClick: e[2] || (e[2] = function() {
            return t.clickHandle && t.clickHandle.apply(t, arguments);
          }), onTouchstartPassive: e[3] || (e[3] = function() {
            return t.dragStartOnProcess && t.dragStartOnProcess.apply(t, arguments);
          }), onMousedownPassive: e[4] || (e[4] = function() {
            return t.dragStartOnProcess && t.dragStartOnProcess.apply(t, arguments);
          }) }, t.$attrs), [(0, o.createElementVNode)("div", { class: "vue-slider-rail", style: (0, o.normalizeStyle)(t.railStyle) }, [((0, o.openBlock)(!0), (0, o.createElementBlock)(o.Fragment, null, (0, o.renderList)(t.processArray, function(d, R) {
            return (0, o.renderSlot)(t.$slots, "process", (0, o.normalizeProps)((0, o.guardReactiveProps)(d)), function() {
              return [((0, o.openBlock)(), (0, o.createElementBlock)("div", { class: "vue-slider-process", key: "process-".concat(R), style: (0, o.normalizeStyle)(d.style) }, null, 4))];
            });
          }), 256)), t.sliderMarks && t.control ? ((0, o.openBlock)(), (0, o.createElementBlock)("div", g, [((0, o.openBlock)(!0), (0, o.createElementBlock)(o.Fragment, null, (0, o.renderList)(t.control.markList, function(d, R) {
            return (0, o.renderSlot)(t.$slots, "mark", (0, o.normalizeProps)((0, o.guardReactiveProps)(d)), function() {
              var M;
              return [((0, o.openBlock)(), (0, o.createBlock)(l, { key: "mark-".concat(R), mark: d, hideLabel: t.hideLabel, style: (0, o.normalizeStyle)((M = {}, c(M, t.isHorizontal ? "height" : "width", "100%"), c(M, t.isHorizontal ? "width" : "height", t.tailSize), c(M, t.mainDirection, "".concat(d.pos, "%")), M)), stepStyle: t.stepStyle, stepActiveStyle: t.stepActiveStyle, labelStyle: t.labelStyle, labelActiveStyle: t.labelActiveStyle, onPressLabel: e[0] || (e[0] = function(K) {
                return t.clickable && t.setValueByPos(K);
              }) }, { step: (0, o.withCtx)(function() {
                return [(0, o.renderSlot)(t.$slots, "step", (0, o.normalizeProps)((0, o.guardReactiveProps)(d)))];
              }), label: (0, o.withCtx)(function() {
                return [(0, o.renderSlot)(t.$slots, "label", (0, o.normalizeProps)((0, o.guardReactiveProps)(d)))];
              }), _: 2 }, 1032, ["mark", "hideLabel", "style", "stepStyle", "stepActiveStyle", "labelStyle", "labelActiveStyle"]))];
            });
          }), 256))])) : (0, o.createCommentVNode)("", !0), ((0, o.openBlock)(!0), (0, o.createElementBlock)(o.Fragment, null, (0, o.renderList)(t.dots, function(d, R) {
            var M;
            return (0, o.openBlock)(), (0, o.createBlock)(j, (0, o.mergeProps)({ ref_for: !0, ref: "dot-".concat(R), key: "dot-".concat(R), value: d.value, disabled: d.disabled, focus: d.focus, "dot-style": [d.style, d.disabled ? d.disabledStyle : null, d.focus ? d.focusStyle : null], tooltip: d.tooltip || t.tooltip, "tooltip-style": [t.tooltipStyle, d.tooltipStyle, d.disabled ? d.tooltipDisabledStyle : null, d.focus ? d.tooltipFocusStyle : null], "tooltip-formatter": Array.isArray(t.sliderTooltipFormatter) ? t.sliderTooltipFormatter[R] : t.sliderTooltipFormatter, "tooltip-placement": t.tooltipDirections[R], style: [t.dotBaseStyle, (M = {}, c(M, t.mainDirection, "".concat(d.pos, "%")), c(M, "transition", "".concat(t.mainDirection, " ").concat(t.animateTime, "s")), M)], onDragStart: function() {
              return t.dragStart(R);
            }, role: "slider", "aria-valuenow": d.value, "aria-valuemin": t.min, "aria-valuemax": t.max, "aria-orientation": t.isHorizontal ? "horizontal" : "vertical", tabindex: "0", onFocus: function() {
              return t.focus(d, R);
            }, onBlur: e[1] || (e[1] = function() {
              return t.blur();
            }) }, t.dotAttrs), { dot: (0, o.withCtx)(function() {
              return [(0, o.renderSlot)(t.$slots, "dot", (0, o.normalizeProps)((0, o.guardReactiveProps)(d)))];
            }), tooltip: (0, o.withCtx)(function() {
              return [(0, o.renderSlot)(t.$slots, "tooltip", (0, o.normalizeProps)((0, o.guardReactiveProps)(d)))];
            }), _: 2 }, 1040, ["value", "disabled", "focus", "dot-style", "tooltip", "tooltip-style", "tooltip-formatter", "tooltip-placement", "style", "onDragStart", "aria-valuenow", "aria-valuemin", "aria-valuemax", "aria-orientation", "onFocus"]);
          }), 128))], 4), (0, o.renderSlot)(t.$slots, "default", { value: t.getValue() })], 16);
        }
        var w = ["aria-valuetext"], z = { class: "vue-slider-dot-tooltip-text" };
        function A(t, e, r, n, i, a) {
          var l;
          return (0, o.openBlock)(), (0, o.createElementBlock)("div", { ref: "dot", class: (0, o.normalizeClass)(t.dotClasses), "aria-valuetext": (l = t.tooltipValue) === null || l === void 0 ? void 0 : l.toString(), onMousedownPassive: e[0] || (e[0] = function() {
            return t.dragStart && t.dragStart.apply(t, arguments);
          }), onTouchstartPassive: e[1] || (e[1] = function() {
            return t.dragStart && t.dragStart.apply(t, arguments);
          }) }, [(0, o.renderSlot)(t.$slots, "dot", {}, function() {
            return [(0, o.createElementVNode)("div", { class: (0, o.normalizeClass)(t.handleClasses), style: (0, o.normalizeStyle)(t.dotStyle) }, null, 6)];
          }), t.tooltip !== "none" ? ((0, o.openBlock)(), (0, o.createElementBlock)("div", { key: 0, class: (0, o.normalizeClass)(t.tooltipClasses) }, [(0, o.renderSlot)(t.$slots, "tooltip", {}, function() {
            return [(0, o.createElementVNode)("div", { class: (0, o.normalizeClass)(t.tooltipInnerClasses), style: (0, o.normalizeStyle)(t.tooltipStyle) }, [(0, o.createElementVNode)("span", z, (0, o.toDisplayString)(t.tooltipValue), 1)], 6)];
          })], 2)) : (0, o.createCommentVNode)("", !0)], 42, w);
        }
        P(466);
        var L = (0, o.defineComponent)({ name: "VueSliderDot", emits: ["drag-start"], props: { value: { type: [String, Number], default: 0 }, tooltip: { type: String, required: !0 }, tooltipPlacement: { type: String, validator: function(t) {
          return ["top", "right", "bottom", "left"].indexOf(t) > -1;
        }, required: !0 }, tooltipFormatter: { type: [String, Function] }, focus: { type: Boolean, default: !1 }, disabled: { type: Boolean, default: !1 }, dotStyle: { type: Object }, tooltipStyle: { type: Object } }, computed: { dotClasses: function() {
          return ["vue-slider-dot", { "vue-slider-dot-hover": this.tooltip === "hover" || this.tooltip === "active", "vue-slider-dot-disabled": this.disabled, "vue-slider-dot-focus": this.focus }];
        }, handleClasses: function() {
          return ["vue-slider-dot-handle", { "vue-slider-dot-handle-disabled": this.disabled, "vue-slider-dot-handle-focus": this.focus }];
        }, tooltipClasses: function() {
          return ["vue-slider-dot-tooltip", ["vue-slider-dot-tooltip-".concat(this.tooltipPlacement)], { "vue-slider-dot-tooltip-show": this.showTooltip }];
        }, tooltipInnerClasses: function() {
          return ["vue-slider-dot-tooltip-inner", ["vue-slider-dot-tooltip-inner-".concat(this.tooltipPlacement)], { "vue-slider-dot-tooltip-inner-disabled": this.disabled, "vue-slider-dot-tooltip-inner-focus": this.focus }];
        }, showTooltip: function() {
          switch (this.tooltip) {
            case "always":
              return !0;
            case "none":
              return !1;
            case "focus":
            case "active":
              return !!this.focus;
            default:
              return !1;
          }
        }, tooltipValue: function() {
          return this.tooltipFormatter ? typeof this.tooltipFormatter == "string" ? this.tooltipFormatter.replace(/\{value\}/, String(this.value)) : this.tooltipFormatter(this.value) : this.value;
        } }, methods: { dragStart: function() {
          if (this.disabled)
            return !1;
          this.$emit("drag-start");
        } } }), B = P(831), I = (0, B.Z)(L, [["render", A]]);
        function F(t, e, r, n, i, a) {
          return (0, o.openBlock)(), (0, o.createElementBlock)("div", { class: (0, o.normalizeClass)(t.marksClasses) }, [(0, o.renderSlot)(t.$slots, "step", {}, function() {
            return [(0, o.createElementVNode)("div", { class: (0, o.normalizeClass)(t.stepClasses), style: (0, o.normalizeStyle)([t.stepStyle, t.mark.style || {}, t.mark.active && t.stepActiveStyle ? t.stepActiveStyle : {}, t.mark.active && t.mark.activeStyle ? t.mark.activeStyle : {}]) }, null, 6)];
          }), t.hideLabel ? (0, o.createCommentVNode)("", !0) : (0, o.renderSlot)(t.$slots, "label", { key: 0 }, function() {
            return [(0, o.createElementVNode)("div", { class: (0, o.normalizeClass)(t.labelClasses), style: (0, o.normalizeStyle)([t.labelStyle, t.mark.labelStyle || {}, t.mark.active && t.labelActiveStyle ? t.labelActiveStyle : {}, t.mark.active && t.mark.labelActiveStyle ? t.mark.labelActiveStyle : {}]), onClick: e[0] || (e[0] = function() {
              return t.labelClickHandle && t.labelClickHandle.apply(t, arguments);
            }) }, (0, o.toDisplayString)(t.mark.label), 7)];
          })], 2);
        }
        P(18);
        var H = (0, o.defineComponent)({ name: "VueSliderMark", emits: ["press-label"], props: { mark: { type: Object, required: !0 }, hideLabel: { type: Boolean }, stepStyle: { type: Object, default: function() {
          return {};
        } }, stepActiveStyle: { type: Object, default: function() {
          return {};
        } }, labelStyle: { type: Object, default: function() {
          return {};
        } }, labelActiveStyle: { type: Object, default: function() {
          return {};
        } } }, computed: { marksClasses: function() {
          return ["vue-slider-mark", { "vue-slider-mark-active": this.mark.active }];
        }, stepClasses: function() {
          return ["vue-slider-mark-step", { "vue-slider-mark-step-active": this.mark.active }];
        }, labelClasses: function() {
          return ["vue-slider-mark-label", { "vue-slider-mark-label-active": this.mark.active }];
        } }, methods: { labelClickHandle: function(t) {
          t.stopPropagation(), this.$emit("press-label", this.mark.pos);
        } } });
        const C = (0, B.Z)(H, [["render", F]]);
        var x, W = C, U = function(t) {
          return typeof t == "number" ? "".concat(t, "px") : t;
        }, nt = function(t) {
          var e = document.documentElement, r = document.body, n = t.getBoundingClientRect(), i = { y: n.top + (window.pageYOffset || e.scrollTop) - (e.clientTop || r.clientTop || 0), x: n.left + (window.pageXOffset || e.scrollLeft) - (e.clientLeft || r.clientLeft || 0) };
          return i;
        }, f = function(t, e, r) {
          var n = arguments.length > 3 && arguments[3] !== void 0 ? arguments[3] : 1, i = "targetTouches" in t ? t.targetTouches[0] : t, a = nt(e), l = { x: i.pageX - a.x, y: i.pageY - a.y };
          return { x: r ? e.offsetWidth * n - l.x : l.x, y: r ? e.offsetHeight * n - l.y : l.y };
        };
        (function(t) {
          t[t.PAGE_UP = 33] = "PAGE_UP", t[t.PAGE_DOWN = 34] = "PAGE_DOWN", t[t.END = 35] = "END", t[t.HOME = 36] = "HOME", t[t.LEFT = 37] = "LEFT", t[t.UP = 38] = "UP", t[t.RIGHT = 39] = "RIGHT", t[t.DOWN = 40] = "DOWN";
        })(x || (x = {}));
        var b = function(t, e) {
          if (e.hook) {
            var r = e.hook(t);
            if (typeof r == "function")
              return r;
            if (!r)
              return null;
          }
          switch (t.keyCode) {
            case x.UP:
              return function(n) {
                return e.direction === "ttb" ? n - 1 : n + 1;
              };
            case x.RIGHT:
              return function(n) {
                return e.direction === "rtl" ? n - 1 : n + 1;
              };
            case x.DOWN:
              return function(n) {
                return e.direction === "ttb" ? n + 1 : n - 1;
              };
            case x.LEFT:
              return function(n) {
                return e.direction === "rtl" ? n + 1 : n - 1;
              };
            case x.END:
              return function() {
                return e.max;
              };
            case x.HOME:
              return function() {
                return e.min;
              };
            case x.PAGE_UP:
              return function(n) {
                return n + 10;
              };
            case x.PAGE_DOWN:
              return function(n) {
                return n - 10;
              };
            default:
              return null;
          }
        };
        function y(t, e) {
          if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function");
        }
        function v(t, e) {
          for (var r = 0; r < e.length; r++) {
            var n = e[r];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
          }
        }
        function p(t, e, r) {
          return e && v(t.prototype, e), r && v(t, r), Object.defineProperty(t, "prototype", { writable: !1 }), t;
        }
        function D(t, e, r) {
          return e in t ? Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = r, t;
        }
        var E, O, N = function() {
          function t(e) {
            y(this, t), D(this, "num", void 0), this.num = e;
          }
          return p(t, [{ key: "decimal", value: function(e, r) {
            var n = this.num, i = this.getDecimalLen(n), a = this.getDecimalLen(e), l = 0;
            switch (r) {
              case "+":
                l = this.getExponent(i, a), this.num = (this.safeRoundUp(n, l) + this.safeRoundUp(e, l)) / l;
                break;
              case "-":
                l = this.getExponent(i, a), this.num = (this.safeRoundUp(n, l) - this.safeRoundUp(e, l)) / l;
                break;
              case "*":
                this.num = this.safeRoundUp(this.safeRoundUp(n, this.getExponent(i)), this.safeRoundUp(e, this.getExponent(a))) / this.getExponent(i + a);
                break;
              case "/":
                l = this.getExponent(i, a), this.num = this.safeRoundUp(n, l) / this.safeRoundUp(e, l);
                break;
              case "%":
                l = this.getExponent(i, a), this.num = this.safeRoundUp(n, l) % this.safeRoundUp(e, l) / l;
                break;
            }
            return this;
          } }, { key: "plus", value: function(e) {
            return this.decimal(e, "+");
          } }, { key: "minus", value: function(e) {
            return this.decimal(e, "-");
          } }, { key: "multiply", value: function(e) {
            return this.decimal(e, "*");
          } }, { key: "divide", value: function(e) {
            return this.decimal(e, "/");
          } }, { key: "remainder", value: function(e) {
            return this.decimal(e, "%");
          } }, { key: "toNumber", value: function() {
            return this.num;
          } }, { key: "getDecimalLen", value: function(e) {
            var r = "".concat(e).split("e");
            return ("".concat(r[0]).split(".")[1] || "").length - (r[1] ? +r[1] : 0);
          } }, { key: "getExponent", value: function(e, r) {
            return Math.pow(10, r !== void 0 ? Math.max(e, r) : e);
          } }, { key: "safeRoundUp", value: function(e, r) {
            return Math.round(e * r);
          } }]), t;
        }();
        function T(t, e) {
          return Rt(t) || Et(t, e) || ft(t, e) || _();
        }
        function _() {
          throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`);
        }
        function Et(t, e) {
          var r = t == null ? null : typeof Symbol < "u" && t[Symbol.iterator] || t["@@iterator"];
          if (r != null) {
            var n, i, a = [], l = !0, j = !1;
            try {
              for (r = r.call(t); !(l = (n = r.next()).done) && (a.push(n.value), !(e && a.length === e)); l = !0)
                ;
            } catch (d) {
              j = !0, i = d;
            } finally {
              try {
                l || r.return == null || r.return();
              } finally {
                if (j)
                  throw i;
              }
            }
            return a;
          }
        }
        function Rt(t) {
          if (Array.isArray(t))
            return t;
        }
        function ht(t, e) {
          var r = Object.keys(t);
          if (Object.getOwnPropertySymbols) {
            var n = Object.getOwnPropertySymbols(t);
            e && (n = n.filter(function(i) {
              return Object.getOwnPropertyDescriptor(t, i).enumerable;
            })), r.push.apply(r, n);
          }
          return r;
        }
        function At(t) {
          for (var e = 1; e < arguments.length; e++) {
            var r = arguments[e] != null ? arguments[e] : {};
            e % 2 ? ht(Object(r), !0).forEach(function(n) {
              S(t, n, r[n]);
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(r)) : ht(Object(r)).forEach(function(n) {
              Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(r, n));
            });
          }
          return t;
        }
        function Y(t) {
          return jt(t) || Ct(t) || ft(t) || Vt();
        }
        function Vt() {
          throw new TypeError(`Invalid attempt to spread non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`);
        }
        function ft(t, e) {
          if (t) {
            if (typeof t == "string")
              return ot(t, e);
            var r = Object.prototype.toString.call(t).slice(8, -1);
            return r === "Object" && t.constructor && (r = t.constructor.name), r === "Map" || r === "Set" ? Array.from(t) : r === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(r) ? ot(t, e) : void 0;
          }
        }
        function Ct(t) {
          if (typeof Symbol < "u" && t[Symbol.iterator] != null || t["@@iterator"] != null)
            return Array.from(t);
        }
        function jt(t) {
          if (Array.isArray(t))
            return ot(t);
        }
        function ot(t, e) {
          (e == null || e > t.length) && (e = t.length);
          for (var r = 0, n = new Array(e); r < e; r++)
            n[r] = t[r];
          return n;
        }
        function Bt(t, e) {
          if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function");
        }
        function pt(t, e) {
          for (var r = 0; r < e.length; r++) {
            var n = e[r];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
          }
        }
        function Mt(t, e, r) {
          return e && pt(t.prototype, e), r && pt(t, r), Object.defineProperty(t, "prototype", { writable: !1 }), t;
        }
        function S(t, e, r) {
          return e in t ? Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = r, t;
        }
        (function(t) {
          t[t.VALUE = 1] = "VALUE", t[t.INTERVAL = 2] = "INTERVAL", t[t.MIN = 3] = "MIN", t[t.MAX = 4] = "MAX", t[t.ORDER = 5] = "ORDER";
        })(O || (O = {}));
        var Nt = (E = {}, S(E, O.VALUE, 'The type of the "value" is illegal'), S(E, O.INTERVAL, 'The prop "interval" is invalid, "(max - min)" must be divisible by "interval"'), S(E, O.MIN, 'The "value" must be greater than or equal to the "min".'), S(E, O.MAX, 'The "value" must be less than or equal to the "max".'), S(E, O.ORDER, 'When "order" is false, the parameters "minRange", "maxRange", "fixed", "enabled" are invalid.'), E), zt = function() {
          function t(e) {
            Bt(this, t), S(this, "dotsPos", []), S(this, "dotsValue", []), S(this, "data", void 0), S(this, "enableCross", void 0), S(this, "fixed", void 0), S(this, "max", void 0), S(this, "min", void 0), S(this, "interval", void 0), S(this, "minRange", void 0), S(this, "maxRange", void 0), S(this, "order", void 0), S(this, "marks", void 0), S(this, "included", void 0), S(this, "process", void 0), S(this, "adsorb", void 0), S(this, "dotOptions", void 0), S(this, "onError", void 0), S(this, "cacheRangeDir", {}), this.data = e.data, this.max = e.max, this.min = e.min, this.interval = e.interval, this.order = e.order, this.marks = e.marks, this.included = e.included, this.process = e.process, this.adsorb = e.adsorb, this.dotOptions = e.dotOptions, this.onError = e.onError, this.order ? (this.minRange = e.minRange || 0, this.maxRange = e.maxRange || 0, this.enableCross = e.enableCross, this.fixed = e.fixed) : ((e.minRange || e.maxRange || !e.enableCross || e.fixed) && this.emitError(O.ORDER), this.minRange = 0, this.maxRange = 0, this.enableCross = !0, this.fixed = !1), this.setValue(e.value);
          }
          return Mt(t, [{ key: "setValue", value: function(e) {
            this.setDotsValue(Array.isArray(e) ? Y(e) : [e], !0);
          } }, { key: "setDotsValue", value: function(e, r) {
            this.dotsValue = e, r && this.syncDotsPos();
          } }, { key: "setDotsPos", value: function(e) {
            var r = this, n = this.order ? Y(e).sort(function(i, a) {
              return i - a;
            }) : e;
            this.dotsPos = n, this.setDotsValue(n.map(function(i) {
              return r.getValueByPos(i);
            }), this.adsorb);
          } }, { key: "getValueByPos", value: function(e) {
            var r = this.parsePos(e);
            if (this.included) {
              var n = 100;
              this.markList.forEach(function(i) {
                var a = Math.abs(i.pos - e);
                a < n && (n = a, r = i.value);
              });
            }
            return r;
          } }, { key: "syncDotsPos", value: function() {
            var e = this;
            this.dotsPos = this.dotsValue.map(function(r) {
              return e.parseValue(r);
            });
          } }, { key: "markList", get: function() {
            var e = this;
            if (!this.marks)
              return [];
            var r = function(n, i) {
              var a = e.parseValue(n);
              return At({ pos: a, value: n, label: n, active: e.isActiveByPos(a) }, i);
            };
            return this.marks === !0 ? this.getValues().map(function(n) {
              return r(n);
            }) : Object.prototype.toString.call(this.marks) === "[object Object]" ? Object.keys(this.marks).sort(function(n, i) {
              return +n - +i;
            }).map(function(n) {
              var i = e.marks[n];
              return r(n, typeof i != "string" ? i : { label: i });
            }) : Array.isArray(this.marks) ? this.marks.map(function(n) {
              return r(n);
            }) : typeof this.marks == "function" ? this.getValues().map(function(n) {
              return { value: n, result: e.marks(n) };
            }).filter(function(n) {
              var i = n.result;
              return !!i;
            }).map(function(n) {
              var i = n.value, a = n.result;
              return r(i, a);
            }) : [];
          } }, { key: "getRecentDot", value: function(e) {
            var r = this.dotsPos.map(function(n) {
              return Math.abs(n - e);
            });
            return r.indexOf(Math.min.apply(Math, Y(r)));
          } }, { key: "getIndexByValue", value: function(e) {
            return this.data ? this.data.indexOf(e) : new N(+e).minus(this.min).divide(this.interval).toNumber();
          } }, { key: "getValueByIndex", value: function(e) {
            return e < 0 ? e = 0 : e > this.total && (e = this.total), this.data ? this.data[e] : new N(e).multiply(this.interval).plus(this.min).toNumber();
          } }, { key: "setDotPos", value: function(e, r) {
            e = this.getValidPos(e, r).pos;
            var n = e - this.dotsPos[r];
            if (n) {
              var i = new Array(this.dotsPos.length);
              this.fixed ? i = this.getFixedChangePosArr(n, r) : this.minRange || this.maxRange ? i = this.getLimitRangeChangePosArr(e, n, r) : i[r] = n, this.setDotsPos(this.dotsPos.map(function(a, l) {
                return a + (i[l] || 0);
              }));
            }
          } }, { key: "getFixedChangePosArr", value: function(e, r) {
            var n = this;
            return this.dotsPos.forEach(function(i, a) {
              if (a !== r) {
                var l = n.getValidPos(i + e, a), j = l.pos, d = l.inRange;
                d || (e = Math.min(Math.abs(j - i), Math.abs(e)) * (e < 0 ? -1 : 1));
              }
            }), this.dotsPos.map(function(i) {
              return e;
            });
          } }, { key: "getLimitRangeChangePosArr", value: function(e, r, n) {
            var i = this, a = [{ index: n, changePos: r }], l = r;
            return [this.minRange, this.maxRange].forEach(function(j, d) {
              if (!j)
                return !1;
              var R = d === 0, M = r > 0, K = 0;
              K = R ? M ? 1 : -1 : M ? -1 : 1;
              for (var qt = function(Kt, Jt) {
                var St = Math.abs(Kt - Jt);
                return R ? St < i.minRangeDir : St > i.maxRangeDir;
              }, J = n + K, Q = i.dotsPos[J], gt = e; i.isPos(Q) && qt(Q, gt); ) {
                var Yt = i.getValidPos(Q + l, J), kt = Yt.pos;
                a.push({ index: J, changePos: kt - Q }), J += K, gt = kt, Q = i.dotsPos[J];
              }
            }), this.dotsPos.map(function(j, d) {
              var R = a.filter(function(M) {
                return M.index === d;
              });
              return R.length ? R[0].changePos : 0;
            });
          } }, { key: "isPos", value: function(e) {
            return typeof e == "number";
          } }, { key: "getValidPos", value: function(e, r) {
            var n = this.valuePosRange[r], i = !0;
            return e < n[0] ? (e = n[0], i = !1) : e > n[1] && (e = n[1], i = !1), { pos: e, inRange: i };
          } }, { key: "parseValue", value: function(e) {
            if (this.data)
              e = this.data.indexOf(e);
            else if (typeof e == "number" || typeof e == "string") {
              if (e = +e, e < this.min)
                return this.emitError(O.MIN), 0;
              if (e > this.max)
                return this.emitError(O.MAX), 0;
              if (typeof e != "number" || e !== e)
                return this.emitError(O.VALUE), 0;
              e = new N(e).minus(this.min).divide(this.interval).toNumber();
            }
            var r = new N(e).multiply(this.gap).toNumber();
            return r < 0 ? 0 : r > 100 ? 100 : r;
          } }, { key: "parsePos", value: function(e) {
            var r = Math.round(e / this.gap);
            return this.getValueByIndex(r);
          } }, { key: "isActiveByPos", value: function(e) {
            return this.processArray.some(function(r) {
              var n = T(r, 2), i = n[0], a = n[1];
              return e >= i && e <= a;
            });
          } }, { key: "getValues", value: function() {
            if (this.data)
              return this.data;
            for (var e = [], r = 0; r <= this.total; r++)
              e.push(new N(r).multiply(this.interval).plus(this.min).toNumber());
            return e;
          } }, { key: "getRangeDir", value: function(e) {
            return e ? new N(e).divide(new N(this.data ? this.data.length - 1 : this.max).minus(this.data ? 0 : this.min).toNumber()).multiply(100).toNumber() : 100;
          } }, { key: "emitError", value: function(e) {
            this.onError && this.onError(e, Nt[e]);
          } }, { key: "processArray", get: function() {
            if (this.process) {
              if (typeof this.process == "function")
                return this.process(this.dotsPos);
              if (this.dotsPos.length === 1)
                return [[0, this.dotsPos[0]]];
              if (this.dotsPos.length > 1)
                return [[Math.min.apply(Math, Y(this.dotsPos)), Math.max.apply(Math, Y(this.dotsPos))]];
            }
            return [];
          } }, { key: "total", get: function() {
            var e = 0;
            return e = this.data ? this.data.length - 1 : new N(this.max).minus(this.min).divide(this.interval).toNumber(), e - Math.floor(e) !== 0 ? (this.emitError(O.INTERVAL), 0) : e;
          } }, { key: "gap", get: function() {
            return 100 / this.total;
          } }, { key: "minRangeDir", get: function() {
            return this.cacheRangeDir[this.minRange] ? this.cacheRangeDir[this.minRange] : this.cacheRangeDir[this.minRange] = this.getRangeDir(this.minRange);
          } }, { key: "maxRangeDir", get: function() {
            return this.cacheRangeDir[this.maxRange] ? this.cacheRangeDir[this.maxRange] : this.cacheRangeDir[this.maxRange] = this.getRangeDir(this.maxRange);
          } }, { key: "getDotRange", value: function(e, r, n) {
            if (!this.dotOptions)
              return n;
            var i = Array.isArray(this.dotOptions) ? this.dotOptions[e] : this.dotOptions;
            return i && i[r] !== void 0 ? this.parseValue(i[r]) : n;
          } }, { key: "valuePosRange", get: function() {
            var e = this, r = this.dotsPos, n = [];
            return r.forEach(function(i, a) {
              n.push([Math.max(e.minRange ? e.minRangeDir * a : 0, e.enableCross ? 0 : r[a - 1] || 0, e.getDotRange(a, "min", 0)), Math.min(e.minRange ? 100 - e.minRangeDir * (r.length - 1 - a) : 100, e.enableCross ? 100 : r[a + 1] || 100, e.getDotRange(a, "max", 100))]);
            }), n;
          } }, { key: "dotsIndex", get: function() {
            var e = this;
            return this.dotsValue.map(function(r) {
              return e.getIndexByValue(r);
            });
          } }]), t;
        }();
        function Lt(t, e) {
          if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function");
        }
        function mt(t, e) {
          for (var r = 0; r < e.length; r++) {
            var n = e[r];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
          }
        }
        function It(t, e, r) {
          return e && mt(t.prototype, e), r && mt(t, r), Object.defineProperty(t, "prototype", { writable: !1 }), t;
        }
        function vt(t, e, r) {
          return e in t ? Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = r, t;
        }
        var Tt = function() {
          function t(e) {
            Lt(this, t), vt(this, "map", void 0), vt(this, "states", 0), this.map = e;
          }
          return It(t, [{ key: "add", value: function(e) {
            this.states |= e;
          } }, { key: "delete", value: function(e) {
            this.states &= ~e;
          } }, { key: "toggle", value: function(e) {
            this.has(e) ? this.delete(e) : this.add(e);
          } }, { key: "has", value: function(e) {
            return !!(this.states & e);
          } }]), t;
        }();
        P(631);
        function it(t) {
          return $t(t) || Ht(t) || bt(t) || Ft();
        }
        function Ft() {
          throw new TypeError(`Invalid attempt to spread non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`);
        }
        function Ht(t) {
          if (typeof Symbol < "u" && t[Symbol.iterator] != null || t["@@iterator"] != null)
            return Array.from(t);
        }
        function $t(t) {
          if (Array.isArray(t))
            return lt(t);
        }
        function at(t) {
          return at = typeof Symbol == "function" && typeof Symbol.iterator == "symbol" ? function(e) {
            return typeof e;
          } : function(e) {
            return e && typeof Symbol == "function" && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
          }, at(t);
        }
        function yt(t, e) {
          var r = Object.keys(t);
          if (Object.getOwnPropertySymbols) {
            var n = Object.getOwnPropertySymbols(t);
            e && (n = n.filter(function(i) {
              return Object.getOwnPropertyDescriptor(t, i).enumerable;
            })), r.push.apply(r, n);
          }
          return r;
        }
        function tt(t) {
          for (var e = 1; e < arguments.length; e++) {
            var r = arguments[e] != null ? arguments[e] : {};
            e % 2 ? yt(Object(r), !0).forEach(function(n) {
              $(t, n, r[n]);
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(r)) : yt(Object(r)).forEach(function(n) {
              Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(r, n));
            });
          }
          return t;
        }
        function $(t, e, r) {
          return e in t ? Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = r, t;
        }
        function st(t, e) {
          return Gt(t) || _t(t, e) || bt(t, e) || Ut();
        }
        function Ut() {
          throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`);
        }
        function bt(t, e) {
          if (t) {
            if (typeof t == "string")
              return lt(t, e);
            var r = Object.prototype.toString.call(t).slice(8, -1);
            return r === "Object" && t.constructor && (r = t.constructor.name), r === "Map" || r === "Set" ? Array.from(t) : r === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(r) ? lt(t, e) : void 0;
          }
        }
        function lt(t, e) {
          (e == null || e > t.length) && (e = t.length);
          for (var r = 0, n = new Array(e); r < e; r++)
            n[r] = t[r];
          return n;
        }
        function _t(t, e) {
          var r = t == null ? null : typeof Symbol < "u" && t[Symbol.iterator] || t["@@iterator"];
          if (r != null) {
            var n, i, a = [], l = !0, j = !1;
            try {
              for (r = r.call(t); !(l = (n = r.next()).done) && (a.push(n.value), !(e && a.length === e)); l = !0)
                ;
            } catch (d) {
              j = !0, i = d;
            } finally {
              try {
                l || r.return == null || r.return();
              } finally {
                if (j)
                  throw i;
              }
            }
            return a;
          }
        }
        function Gt(t) {
          if (Array.isArray(t))
            return t;
        }
        var V = { None: 0, Drag: 2, Focus: 4 }, ut = 4, Wt = (0, o.defineComponent)({ name: "VueSlider", components: { VueSliderDot: I, VueSliderMark: W }, emits: ["change", "drag-start", "dragging", "drag-end", "error", "update:modelValue"], data: function() {
          return { control: null, states: new Tt(V), scale: 1, focusDotIndex: 0 };
        }, props: { modelValue: { type: [Number, String, Array], default: 0 }, silent: { type: Boolean, default: !1 }, direction: { type: String, default: "ltr", validator: function(t) {
          return ["ltr", "rtl", "ttb", "btt"].indexOf(t) > -1;
        } }, width: { type: [Number, String] }, height: { type: [Number, String] }, dotSize: { type: [Number, Array], default: 14 }, contained: { type: Boolean, default: !1 }, min: { type: Number, default: 0 }, max: { type: Number, default: 100 }, interval: { type: Number, default: 1 }, disabled: { type: Boolean, default: !1 }, clickable: { type: Boolean, default: !0 }, dragOnClick: { type: Boolean, default: !1 }, duration: { type: Number, default: 0.5 }, data: { type: [Object, Array] }, dataValue: { type: String, default: "value" }, dataLabel: { type: String, default: "label" }, lazy: { type: Boolean, default: !1 }, tooltip: { type: String, default: "active", validator: function(t) {
          return ["none", "always", "focus", "hover", "active"].indexOf(t) > -1;
        } }, tooltipPlacement: { type: [String, Array], validator: function(t) {
          return (Array.isArray(t) ? t : [t]).every(function(e) {
            return ["top", "right", "bottom", "left"].indexOf(e) > -1;
          });
        } }, tooltipFormatter: { type: [String, Array, Function] }, useKeyboard: { type: Boolean, default: !0 }, keydownHook: { type: Function }, enableCross: { type: Boolean, default: !0 }, fixed: { type: Boolean, default: !1 }, order: { type: Boolean, default: !0 }, minRange: { type: Number }, maxRange: { type: Number }, marks: { type: [Boolean, Object, Array, Function], default: !1 }, process: { type: [Boolean, Function], default: !0 }, zoom: { type: Number }, included: { type: Boolean }, adsorb: { type: Boolean }, hideLabel: { type: Boolean }, dotOptions: { type: [Object, Array] }, dotAttrs: { type: Object }, railStyle: { type: Object }, processStyle: { type: Object }, dotStyle: { type: Object }, tooltipStyle: { type: Object }, stepStyle: { type: Object }, stepActiveStyle: { type: Object }, labelStyle: { type: Object }, labelActiveStyle: { type: Object } }, computed: { isHorizontal: function() {
          return this.direction === "ltr" || this.direction === "rtl";
        }, isReverse: function() {
          return this.direction === "rtl" || this.direction === "btt";
        }, tailSize: function() {
          return U((this.isHorizontal ? this.height : this.width) || ut);
        }, containerClasses: function() {
          return ["vue-slider", ["vue-slider-".concat(this.direction)], { "vue-slider-disabled": this.disabled }];
        }, containerStyles: function() {
          var t = Array.isArray(this.dotSize) ? this.dotSize : [this.dotSize, this.dotSize], e = st(t, 2), r = e[0], n = e[1], i = this.width ? U(this.width) : this.isHorizontal ? "auto" : U(ut), a = this.height ? U(this.height) : this.isHorizontal ? U(ut) : "auto";
          return { padding: this.contained ? "".concat(n / 2, "px ").concat(r / 2, "px") : this.isHorizontal ? "".concat(n / 2, "px 0") : "0 ".concat(r / 2, "px"), width: i, height: a };
        }, processArray: function() {
          var t = this;
          return this.control.processArray.map(function(e, r) {
            var n, i = st(e, 3), a = i[0], l = i[1], j = i[2];
            if (a > l) {
              var d = [l, a];
              a = d[0], l = d[1];
            }
            var R = t.isHorizontal ? "width" : "height";
            return { start: a, end: l, index: r, style: tt(tt((n = {}, $(n, t.isHorizontal ? "height" : "width", "100%"), $(n, t.isHorizontal ? "top" : "left", 0), $(n, t.mainDirection, "".concat(a, "%")), $(n, R, "".concat(l - a, "%")), $(n, "transitionProperty", "".concat(R, ",").concat(t.mainDirection)), $(n, "transitionDuration", "".concat(t.animateTime, "s")), n), t.processStyle), j) };
          });
        }, dotBaseStyle: function() {
          var t, e = Array.isArray(this.dotSize) ? this.dotSize : [this.dotSize, this.dotSize], r = st(e, 2), n = r[0], i = r[1];
          return t = this.isHorizontal ? $({ transform: "translate(".concat(this.isReverse ? "50%" : "-50%", ", -50%)"), WebkitTransform: "translate(".concat(this.isReverse ? "50%" : "-50%", ", -50%)"), top: "50%" }, this.direction === "ltr" ? "left" : "right", "0") : $({ transform: "translate(-50%, ".concat(this.isReverse ? "50%" : "-50%", ")"), WebkitTransform: "translate(-50%, ".concat(this.isReverse ? "50%" : "-50%", ")"), left: "50%" }, this.direction === "btt" ? "bottom" : "top", "0"), tt({ width: "".concat(n, "px"), height: "".concat(i, "px") }, t);
        }, mainDirection: function() {
          switch (this.direction) {
            case "ltr":
              return "left";
            case "rtl":
              return "right";
            case "btt":
              return "bottom";
            case "ttb":
              return "top";
            default:
              return "left";
          }
        }, tooltipDirections: function() {
          var t = this.tooltipPlacement || (this.isHorizontal ? "top" : "left");
          return Array.isArray(t) ? t : this.dots.map(function() {
            return t;
          });
        }, dots: function() {
          var t = this;
          return this.control.dotsPos.map(function(e, r) {
            return tt({ pos: e, index: r, value: t.control.dotsValue[r], focus: t.states.has(V.Focus) && t.focusDotIndex === r, disabled: t.disabled, style: t.dotStyle }, (Array.isArray(t.dotOptions) ? t.dotOptions[r] : t.dotOptions) || {});
          });
        }, animateTime: function() {
          return this.states.has(V.Drag) ? 0 : this.duration;
        }, canSort: function() {
          return this.order && !this.minRange && !this.maxRange && !this.fixed && this.enableCross;
        }, sliderData: function() {
          var t = this;
          return this.isObjectArrayData(this.data) ? this.data.map(function(e) {
            return e[t.dataValue];
          }) : this.isObjectData(this.data) ? Object.keys(this.data) : this.data;
        }, sliderMarks: function() {
          var t = this;
          return this.marks ? this.marks : this.isObjectArrayData(this.data) ? function(e) {
            var r = { label: e };
            return t.data.some(function(n) {
              return n[t.dataValue] === e && (r.label = n[t.dataLabel], !0);
            }), r;
          } : this.isObjectData(this.data) ? this.data : void 0;
        }, sliderTooltipFormatter: function() {
          var t = this;
          if (this.tooltipFormatter)
            return this.tooltipFormatter;
          if (this.isObjectArrayData(this.data))
            return function(r) {
              var n = "" + r;
              return t.data.some(function(i) {
                return i[t.dataValue] === r && (n = i[t.dataLabel], !0);
              }), n;
            };
          if (this.isObjectData(this.data)) {
            var e = this.data;
            return function(r) {
              return e[r];
            };
          }
        }, isNotSync: function() {
          var t = this.control.dotsValue;
          return Array.isArray(this.modelValue) ? this.modelValue.length !== t.length || this.modelValue.some(function(e, r) {
            return e !== t[r];
          }) : this.modelValue !== t[0];
        }, dragRange: function() {
          var t = this.dots[this.focusDotIndex - 1], e = this.dots[this.focusDotIndex + 1];
          return [t ? t.pos : -1 / 0, e ? e.pos : 1 / 0];
        } }, watch: { modelValue: function() {
          this.control && !this.states.has(V.Drag) && this.isNotSync && this.control.setValue(this.modelValue);
        } }, methods: { isObjectData: function(t) {
          return !!t && Object.prototype.toString.call(t) === "[object Object]";
        }, isObjectArrayData: function(t) {
          return !!t && Array.isArray(t) && t.length > 0 && at(t[0]) === "object";
        }, bindEvent: function() {
          document.addEventListener("touchmove", this.dragMove, { passive: !1 }), document.addEventListener("touchend", this.dragEnd, { passive: !1 }), document.addEventListener("mousedown", this.blurHandle), document.addEventListener("mousemove", this.dragMove), document.addEventListener("mouseup", this.dragEnd), document.addEventListener("mouseleave", this.dragEnd), document.addEventListener("keydown", this.keydownHandle);
        }, unbindEvent: function() {
          document.removeEventListener("touchmove", this.dragMove), document.removeEventListener("touchend", this.dragEnd), document.removeEventListener("mousedown", this.blurHandle), document.removeEventListener("mousemove", this.dragMove), document.removeEventListener("mouseup", this.dragEnd), document.removeEventListener("mouseleave", this.dragEnd), document.removeEventListener("keydown", this.keydownHandle);
        }, setScale: function() {
          this.scale = new N(Math.floor(this.isHorizontal ? this.$el.offsetWidth : this.$el.offsetHeight)).multiply(this.zoom || 1).divide(100).toNumber();
        }, initControl: function() {
          var t = this;
          this.control = new zt({ value: this.modelValue, data: this.sliderData, enableCross: this.enableCross, fixed: this.fixed, max: this.max, min: this.min, interval: this.interval, minRange: this.minRange, maxRange: this.maxRange, order: this.order, marks: this.sliderMarks, included: this.included, process: this.process, adsorb: this.adsorb, dotOptions: this.dotOptions, onError: this.emitError }), ["data", "enableCross", "fixed", "max", "min", "interval", "minRange", "maxRange", "order", "marks", "process", "adsorb", "included", "dotOptions"].forEach(function(e) {
            t.$watch(e, function(r) {
              if (e === "data" && Array.isArray(t.control.data) && Array.isArray(r) && t.control.data.length === r.length && r.every(function(n, i) {
                return n === t.control.data[i];
              }))
                return !1;
              switch (e) {
                case "data":
                case "dataLabel":
                case "dataValue":
                  t.control.data = t.sliderData;
                  break;
                case "mark":
                  t.control.marks = t.sliderMarks;
                  break;
                default:
                  t.control[e] = r;
              }
              ["data", "max", "min", "interval"].indexOf(e) > -1 && t.control.syncDotsPos();
            });
          });
        }, syncValueByPos: function() {
          var t = this.control.dotsValue;
          if (this.isDiff(t, Array.isArray(this.modelValue) ? this.modelValue : [this.modelValue])) {
            var e = t.length === 1 ? t[0] : it(t);
            this.$emit("change", e, this.focusDotIndex), this.$emit("update:modelValue", e);
          }
        }, isDiff: function(t, e) {
          return t.length !== e.length || t.some(function(r, n) {
            return r !== e[n];
          });
        }, emitError: function(t, e) {
          this.silent || console.error("[VueSlider error]: ".concat(e)), this.$emit("error", t, e);
        }, dragStartOnProcess: function(t) {
          if (this.dragOnClick) {
            this.setScale();
            var e = this.getPosByEvent(t), r = this.control.getRecentDot(e);
            if (this.dots[r].disabled)
              return;
            this.dragStart(r), this.control.setDotPos(e, this.focusDotIndex), this.lazy || this.syncValueByPos();
          }
        }, dragStart: function(t) {
          this.focusDotIndex = t, this.setScale(), this.states.add(V.Drag), this.states.add(V.Focus), this.$emit("drag-start", this.focusDotIndex);
        }, dragMove: function(t) {
          if (!this.states.has(V.Drag))
            return !1;
          t.preventDefault();
          var e = this.getPosByEvent(t);
          this.isCrossDot(e), this.control.setDotPos(e, this.focusDotIndex), this.lazy || this.syncValueByPos();
          var r = this.control.dotsValue;
          this.$emit("dragging", r.length === 1 ? r[0] : it(r), this.focusDotIndex);
        }, isCrossDot: function(t) {
          if (this.canSort) {
            var e = this.focusDotIndex, r = t;
            if (r > this.dragRange[1] ? (r = this.dragRange[1], this.focusDotIndex++) : r < this.dragRange[0] && (r = this.dragRange[0], this.focusDotIndex--), e !== this.focusDotIndex) {
              var n = this.$refs["dot-".concat(this.focusDotIndex)];
              n && n.$el && n.$el.focus(), this.control.setDotPos(r, e);
            }
          }
        }, dragEnd: function(t) {
          var e = this;
          if (!this.states.has(V.Drag))
            return !1;
          setTimeout(function() {
            e.lazy && e.syncValueByPos(), e.included && e.isNotSync ? e.control.setValue(e.modelValue) : e.control.syncDotsPos(), e.states.delete(V.Drag), e.useKeyboard && !("targetTouches" in t) || e.states.delete(V.Focus), e.$emit("drag-end", e.focusDotIndex);
          });
        }, blurHandle: function(t) {
          if (!this.states.has(V.Focus) || !this.$refs.container || this.$refs.container.contains(t.target))
            return !1;
          this.states.delete(V.Focus);
        }, clickHandle: function(t) {
          if (!this.clickable || this.disabled)
            return !1;
          if (!this.states.has(V.Drag)) {
            this.setScale();
            var e = this.getPosByEvent(t);
            this.setValueByPos(e);
          }
        }, focus: function(t) {
          var e = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : 0;
          t.disabled || (this.states.add(V.Focus), this.focusDotIndex = e);
        }, blur: function() {
          this.states.delete(V.Focus);
        }, getValue: function() {
          var t = this.control.dotsValue;
          return t.length === 1 ? t[0] : t;
        }, getIndex: function() {
          var t = this.control.dotsIndex;
          return t.length === 1 ? t[0] : t;
        }, setValue: function(t) {
          this.control.setValue(Array.isArray(t) ? it(t) : [t]), this.syncValueByPos();
        }, setIndex: function(t) {
          var e = this, r = Array.isArray(t) ? t.map(function(n) {
            return e.control.getValueByIndex(n);
          }) : this.control.getValueByIndex(t);
          this.setValue(r);
        }, setValueByPos: function(t) {
          var e = this, r = this.control.getRecentDot(t);
          if (this.disabled || this.dots[r].disabled)
            return !1;
          this.focusDotIndex = r, this.control.setDotPos(t, r), this.syncValueByPos(), this.useKeyboard && this.states.add(V.Focus), setTimeout(function() {
            e.included && e.isNotSync ? e.control.setValue(e.modelValue) : e.control.syncDotsPos();
          });
        }, keydownHandle: function(t) {
          var e = this;
          if (!this.useKeyboard || !this.states.has(V.Focus))
            return !1;
          var r = this.included && this.marks, n = b(t, { direction: this.direction, max: r ? this.control.markList.length - 1 : this.control.total, min: 0, hook: this.keydownHook });
          if (n) {
            t.preventDefault();
            var i = -1, a = 0;
            r ? (this.control.markList.some(function(l, j) {
              return l.value === e.control.dotsValue[e.focusDotIndex] && (i = n(j), !0);
            }), i < 0 ? i = 0 : i > this.control.markList.length - 1 && (i = this.control.markList.length - 1), a = this.control.markList[i].pos) : (i = n(this.control.getIndexByValue(this.control.dotsValue[this.focusDotIndex])), a = this.control.parseValue(this.control.getValueByIndex(i))), this.isCrossDot(a), this.control.setDotPos(a, this.focusDotIndex), this.syncValueByPos();
          }
        }, getPosByEvent: function(t) {
          return f(t, this.$el, this.isReverse, this.zoom)[this.isHorizontal ? "x" : "y"] / this.scale;
        }, renderSlot: function(t, e, r) {
          var n = this.$slots[t];
          return n ? n(e) : r;
        } }, created: function() {
          this.initControl();
        }, mounted: function() {
          this.bindEvent();
        }, beforeUnmount: function() {
          this.unbindEvent();
        } }), ct = (0, B.Z)(Wt, [["render", k]]);
        ct.VueSliderMark = W, ct.VueSliderDot = I;
        var Xt = ct, Zt = Xt;
      }(), G = G.default, G;
    }();
  });
})(Dt);
var ce = Dt.exports;
const de = /* @__PURE__ */ se(ce);
const me = /* @__PURE__ */ te({
  __name: "UiSlider",
  props: {
    name: {},
    model: {},
    disabled: { type: Boolean },
    label: {},
    data: {},
    sliderStyles: {}
  },
  emits: ["update:model"],
  setup(rt, { emit: dt }) {
    const X = rt, q = dt, Z = ee(ue), P = xt(() => X.sliderStyles ? X.sliderStyles : Z == null ? void 0 : Z.components.form.uiSlider), G = xt({
      get: () => X.model,
      set: (A) => {
        q("update:model", A);
      }
    }), u = { backgroundColor: "#5F8BFF" }, h = { backgroundColor: "#E5E7E8" }, s = { border: "none" }, o = { width: "14px", height: "14px" }, c = { top: "-5px", left: "-5px" }, g = { boxShadow: "none" }, k = { ...u, ...s }, w = { ...h, ...s }, z = { ...o, ...c };
    return (A, L) => {
      var B, m, I;
      return Pt(), wt("div", {
        class: et([(B = P.value) == null ? void 0 : B.slider])
      }, [
        A.label ? (Pt(), wt("div", {
          key: 0,
          class: et([(m = P.value) == null ? void 0 : m.label])
        }, Ot(A.label), 3)) : re("", !0),
        ne(oe(de), {
          modelValue: G.value,
          "onUpdate:modelValue": L[0] || (L[0] = (F) => G.value = F),
          class: et((I = P.value) == null ? void 0 : I.customSlider),
          tooltip: "none",
          "dot-size": 14,
          "dot-style": k,
          "process-style": k,
          "rail-style": w,
          "step-style": { ...w, ...z },
          "step-active-style": { ...k, ...g, ...z },
          data: A.data,
          "data-label": "label",
          "data-value": "value"
        }, {
          label: ie(({ active: F, label: H }) => {
            var C;
            return [
              ae("div", {
                class: et(["vue-slider-mark-label", (C = P.value) == null ? void 0 : C.dataLabel, { active: F }])
              }, Ot(H), 3)
            ];
          }),
          _: 1
        }, 8, ["modelValue", "class", "step-style", "step-active-style", "data"])
      ], 2);
    };
  }
});
export {
  me as default
};
