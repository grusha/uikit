export declare const DESKTOP_FONT_SIZE: {
    S: number;
    M: number;
    L: number;
    M_BOLD: number;
    L_BOLD: number;
    H_5: number;
    H_4: number;
    H_3: number;
    H_2: number;
    H_1: number;
};
export declare const DESKTOP_LINE_HEIGHT: {
    S: number;
    M: number;
    L: number;
    M_BOLD: number;
    L_BOLD: number;
    H_5: number;
    H_4: number;
    H_3: number;
    H_2: number;
    H_1: number;
};
