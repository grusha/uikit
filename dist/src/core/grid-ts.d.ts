export declare const DESKTOP_COLUMN: {
    AMOUNT: number;
    SIZE: number;
    GAP: number;
};
export declare const DESKTOP_WIDTH: number;
export declare const TABLET_LANDSCAPE_COLUMN: {
    AMOUNT: number;
    SIZE: number;
    GAP: number;
};
export declare const TABLET_LANDSCAPE_WIDTH: number;
export declare const TABLET_PORTRAIT_COLUMN: {
    AMOUNT: number;
    SIZE: number;
    GAP: number;
};
export declare const TABLET_PORTRAIT_WIDTH: number;
export declare const MOBILE_COLUMN: {
    AMOUNT: number;
    SIZE: number;
    GAP: number;
};
export declare const MOBILE_WIDTH: number;
