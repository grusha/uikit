export declare const INDENT_4 = 4;
export declare const INDENT_8 = 8;
export declare const INDENT_16 = 16;
export declare const INDENT_24 = 24;
export declare const INDENT_32 = 32;
export declare const INDENT_80 = 80;
export declare const INDENT_120 = 120;
