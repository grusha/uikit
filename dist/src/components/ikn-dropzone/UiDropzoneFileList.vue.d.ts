import { DropzoneFile } from './dropzone.types';
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    styles: {
        type: import("vue").PropType<import("./dropzone.types").DropzoneFileStyles>;
    };
    tags: {
        type: import("vue").PropType<import("../form/select/select.types").SelectOption[]>;
        required: true;
    };
    files: {
        type: import("vue").PropType<DropzoneFile[]>;
        required: true;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "delete-file": (id: string) => void;
    "download-file": (payload: {
        name: string;
        link: string;
    }) => void;
    "update-file-tags": (payload: {
        id: string;
        tags: string[];
    }) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    styles: {
        type: import("vue").PropType<import("./dropzone.types").DropzoneFileStyles>;
    };
    tags: {
        type: import("vue").PropType<import("../form/select/select.types").SelectOption[]>;
        required: true;
    };
    files: {
        type: import("vue").PropType<DropzoneFile[]>;
        required: true;
    };
}>> & {
    "onDelete-file"?: ((id: string) => any) | undefined;
    "onDownload-file"?: ((payload: {
        name: string;
        link: string;
    }) => any) | undefined;
    "onUpdate-file-tags"?: ((payload: {
        id: string;
        tags: string[];
    }) => any) | undefined;
}, {
    disabled: boolean;
}, {}>;
export default _default;
