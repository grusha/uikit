import { DropzoneEmits, DropzoneProps } from './dropzone.types';
export declare const formatBytes: (bytes: number, decimals?: number) => string;
export declare const useDropzone: (props: DropzoneProps, emits: DropzoneEmits) => {
    input: import("vue").Ref<null>;
    fileInputLabel: import("vue").Ref<string>;
    fileId: import("vue").ComputedRef<string>;
    sizeLimit: import("vue").ComputedRef<string>;
    acceptableExtensionsToDisplay: import("vue").ComputedRef<string[]>;
    acceptableExtensionsParam: import("vue").ComputedRef<string | undefined>;
    calculateDropzoneFooterContent: () => {
        extendsMessage: string;
        extendsPopupMessage: string;
        limitMessage: string;
    };
    onChange: (event: Event) => void;
    dragover: (event: Event) => void;
    dragleave: (event: Event) => void;
    drop: (event: any) => void;
};
