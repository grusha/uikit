import { WritableComputedRef } from 'vue';
import { SelectOption } from '../form/select/select.types';
export type DropzoneError = {
    errorMessage: string;
    fileExt: string;
    fileName: string;
    fileSize: number;
    fileType: string;
};
export type IKNDropzoneStyles = {
    label?: string;
    dropzone?: string;
    input?: string;
    inputPlaceholder?: string;
    inputLabel?: string;
    footer?: string;
    extensions?: string;
    extensionsList?: string;
    extension?: string;
    popup?: string;
};
export type DropzoneProps = {
    model?: File | File[];
    label?: string;
    placeholder?: string;
    placeholderIcon?: string;
    name?: string;
    errorMessage?: string;
    acceptableExtensions?: string[];
    acceptableFileSize?: number;
    multiple?: boolean;
    disabled?: boolean;
    loading?: boolean;
    limit?: number;
    styles?: IKNDropzoneStyles;
};
export type ReceivedFilesOptions = {
    multiple: boolean;
    limit: number;
    acceptableExtensions: string[];
    acceptableFileSize: number;
    proxyFile: WritableComputedRef<File | File[] | undefined>;
};
export type DropzoneEmits = {
    (event: 'update:model', payload: File | File[]): void;
    (event: 'on-upload-files', payload: File | File[]): void;
    (event: 'handle-upload-files-errors', errors: DropzoneError[]): void;
};
export type DropzoneFileStyles = {
    fileList?: string;
    file?: string;
    type?: string;
    info?: string;
    infoContainer?: string;
    attributes?: string;
    text?: string;
    textPrimary?: string;
    textSecondary?: string;
    tagList?: string;
    tag?: string;
    popup?: string;
    option?: string;
    hr?: string;
    popupSelect?: string;
};
export type DropzoneFile = {
    id: string;
    name: string;
    size: number;
    type: string;
    link: string;
    thumbnailUrl?: string;
    tags?: any[];
};
export type DropzoneFileListProps = {
    tags: SelectOption[];
    disabled?: boolean;
    files: DropzoneFile[];
    styles?: DropzoneFileStyles;
};
export type DropzoneFileListEmits = {
    (event: 'delete-file', id: string): void;
    (event: 'download-file', payload: {
        name: string;
        link: string;
    }): void;
    (event: 'update-file-tags', payload: {
        id: string;
        tags: string[];
    }): void;
};
