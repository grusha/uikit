declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    styles: {
        type: import("vue").PropType<import("./dropzone.types").IKNDropzoneStyles>;
    };
    loading: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    limit: {
        type: import("vue").PropType<number>;
        default: number;
    };
    model: {
        type: import("vue").PropType<File | File[]>;
        default: () => never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
    };
    placeholderIcon: {
        type: import("vue").PropType<string>;
        default: string;
    };
    acceptableExtensions: {
        type: import("vue").PropType<string[]>;
        default: () => string[];
    };
    acceptableFileSize: {
        type: import("vue").PropType<number>;
        default: number;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (payload: File | File[]) => void;
    "on-upload-files": (payload: File | File[]) => void;
    "handle-upload-files-errors": (errors: import("./dropzone.types").DropzoneError[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    styles: {
        type: import("vue").PropType<import("./dropzone.types").IKNDropzoneStyles>;
    };
    loading: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    limit: {
        type: import("vue").PropType<number>;
        default: number;
    };
    model: {
        type: import("vue").PropType<File | File[]>;
        default: () => never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
    };
    placeholderIcon: {
        type: import("vue").PropType<string>;
        default: string;
    };
    acceptableExtensions: {
        type: import("vue").PropType<string[]>;
        default: () => string[];
    };
    acceptableFileSize: {
        type: import("vue").PropType<number>;
        default: number;
    };
}>> & {
    "onUpdate:model"?: ((payload: File | File[]) => any) | undefined;
    "onOn-upload-files"?: ((payload: File | File[]) => any) | undefined;
    "onHandle-upload-files-errors"?: ((errors: import("./dropzone.types").DropzoneError[]) => any) | undefined;
}, {
    name: string;
    label: string;
    disabled: boolean;
    placeholder: string;
    loading: boolean;
    multiple: boolean;
    limit: number;
    model: File | File[];
    placeholderIcon: string;
    acceptableExtensions: string[];
    acceptableFileSize: number;
}, {}>;
export default _default;
