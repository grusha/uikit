import UiButtonDropdownStyles, { IButtonDropdownItem } from './button-dropdown.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<any>;
        default: undefined;
    };
    isSelectMultiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectItems: {
        type: import("vue").PropType<IButtonDropdownItem[]>;
        default: () => never[];
    };
    hideArrow: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    buttonDropdownStyles: {
        type: import("vue").PropType<UiButtonDropdownStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (payload: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<any>;
        default: undefined;
    };
    isSelectMultiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectItems: {
        type: import("vue").PropType<IButtonDropdownItem[]>;
        default: () => never[];
    };
    hideArrow: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    buttonDropdownStyles: {
        type: import("vue").PropType<UiButtonDropdownStyles>;
        default: undefined;
    };
}>> & {
    "onUpdate:model"?: ((payload: any) => any) | undefined;
}, {
    disabled: boolean;
    placeholder: string;
    model: any;
    isSelectMultiple: boolean;
    selectItems: IButtonDropdownItem[];
    hideArrow: boolean;
    buttonDropdownStyles: UiButtonDropdownStyles;
}, {}>, {
    default?(_: {
        slotModel: any;
        updateSlotModel: (value: any) => void;
    }): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
