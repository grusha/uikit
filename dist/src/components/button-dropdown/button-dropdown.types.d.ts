export default interface UiButtonDropdownStyles {
    index?: string;
    titleWrapper?: string;
    title?: string;
    color?: string;
    sideButtonIcon?: string;
    sideButton?: string;
    sideButtonActive?: string;
    dropdownSlot?: string;
    dropdownList?: string;
    dropdownItem?: string;
    dropdownItemTitle?: string;
    dropdownCheckbox?: string;
    dropdownCheckboxActive?: string;
    dropdownItemColor?: string;
}
export type IButtonDropdownItem = {
    id: string;
    title?: string;
    icon?: string;
};
