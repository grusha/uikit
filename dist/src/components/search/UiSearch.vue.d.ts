import { UiSearchStyles, SearchIcon } from './search.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        default: string;
    };
    searchStyles: {
        type: import("vue").PropType<UiSearchStyles>;
        default: undefined;
    };
    searchIcon: {
        type: import("vue").PropType<SearchIcon>;
        default: () => SearchIcon;
    };
    clearIcon: {
        type: import("vue").PropType<SearchIcon>;
        default: () => SearchIcon;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: any) => void;
    search: (value?: number | undefined) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        default: string;
    };
    searchStyles: {
        type: import("vue").PropType<UiSearchStyles>;
        default: undefined;
    };
    searchIcon: {
        type: import("vue").PropType<SearchIcon>;
        default: () => SearchIcon;
    };
    clearIcon: {
        type: import("vue").PropType<SearchIcon>;
        default: () => SearchIcon;
    };
}>> & {
    "onUpdate:model"?: ((value: any) => any) | undefined;
    onSearch?: ((value?: number | undefined) => any) | undefined;
}, {
    placeholder: string;
    model: string;
    searchStyles: UiSearchStyles;
    searchIcon: SearchIcon;
    clearIcon: SearchIcon;
}, {}>, {
    select?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
