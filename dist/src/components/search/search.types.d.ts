export interface UiSearchStyles {
    wrapper?: string;
    search?: string;
    label?: string;
    labelIcon?: string;
    input?: string;
    actions?: string;
    action?: string;
    actionIcon?: string;
    actionsDivider?: string;
    select?: string;
    hidden?: string;
}
export type SearchIcon = {
    name: string;
    color: string;
};
