export interface UiAchievementStyles {
    achievement?: string;
    body?: string;
    emoji?: string;
    title?: string;
    qty?: string;
}
export interface Achievement {
    emoji: string;
    title: string;
    qty: number;
}
