import { Achievement, UiAchievementStyles } from './achievement.types';
declare const _default: import("vue").DefineComponent<{
    achievement: {
        type: import("vue").PropType<Achievement>;
        default: () => {
            emoji: string;
            title: string;
            qty: number;
        };
    };
    achievementStyles: {
        type: import("vue").PropType<UiAchievementStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    achievement: {
        type: import("vue").PropType<Achievement>;
        default: () => {
            emoji: string;
            title: string;
            qty: number;
        };
    };
    achievementStyles: {
        type: import("vue").PropType<UiAchievementStyles>;
    };
}>>, {
    achievement: Achievement;
}, {}>;
export default _default;
