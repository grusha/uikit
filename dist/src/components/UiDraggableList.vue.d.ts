declare const _default: import("vue").DefineComponent<{
    list: {
        type: ArrayConstructor;
        default: () => never[];
    };
    itemClass: {
        type: StringConstructor;
        default: string;
    };
    canSort: {
        type: BooleanConstructor;
        default: boolean;
    };
    sortBy: {
        type: StringConstructor;
        default: string;
    };
}, {
    drag: import("vue").Ref<boolean>;
    onChange: () => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "on-update-sort"[], "on-update-sort", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    list: {
        type: ArrayConstructor;
        default: () => never[];
    };
    itemClass: {
        type: StringConstructor;
        default: string;
    };
    canSort: {
        type: BooleanConstructor;
        default: boolean;
    };
    sortBy: {
        type: StringConstructor;
        default: string;
    };
}>> & {
    "onOn-update-sort"?: ((...args: any[]) => any) | undefined;
}, {
    list: unknown[];
    canSort: boolean;
    itemClass: string;
    sortBy: string;
}, {}>;
export default _default;
