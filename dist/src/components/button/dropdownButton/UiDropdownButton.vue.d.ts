import { DropdownButtonItem } from './dropdownButton.types';
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    dropdownButtonItems: {
        type: import("vue").PropType<DropdownButtonItem[]>;
        required: true;
    };
    dropdownButtonStyles: {
        type: import("vue").PropType<import("./dropdownButton.types").DropdownButtonStyles>;
        default: undefined;
    };
    defaultItem: {
        type: import("vue").PropType<DropdownButtonItem>;
        required: true;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    triggerEvent: (payload: Function) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    dropdownButtonItems: {
        type: import("vue").PropType<DropdownButtonItem[]>;
        required: true;
    };
    dropdownButtonStyles: {
        type: import("vue").PropType<import("./dropdownButton.types").DropdownButtonStyles>;
        default: undefined;
    };
    defaultItem: {
        type: import("vue").PropType<DropdownButtonItem>;
        required: true;
    };
}>> & {
    onTriggerEvent?: ((payload: Function) => any) | undefined;
}, {
    disabled: boolean;
    dropdownButtonStyles: import("./dropdownButton.types").DropdownButtonStyles;
}, {}>;
export default _default;
