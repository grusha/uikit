export type DropdownButtonItem = {
    _id: string;
    title?: string;
    icon?: string;
    isDefaultValue?: boolean;
    callback: Function;
};
export type DropdownButtonStyles = {
    buttonContainer?: string;
    button?: string;
    sideButton?: string;
    sideButtonActive?: string;
    sideButtonIcon?: string;
    dropdown?: string;
    dropdownList?: string;
    dropdownItem?: string;
    color?: string;
    itemColor?: string;
};
export type UiDropdownButtonProps = {
    dropdownButtonItems: DropdownButtonItem[];
    dropdownButtonStyles?: DropdownButtonStyles;
    defaultItem: DropdownButtonItem;
    disabled?: boolean;
};
export type UiDropdownEmits = {
    (event: 'triggerEvent', payload: Function): void;
};
