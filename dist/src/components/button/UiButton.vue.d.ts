declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    styles: {
        type: import("vue").PropType<string[]>;
    };
    color: {
        type: import("vue").PropType<string>;
    };
    link: {
        type: import("vue").PropType<string>;
        default: string;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "disabled-click": (value: any) => void;
    click: (value?: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    styles: {
        type: import("vue").PropType<string[]>;
    };
    color: {
        type: import("vue").PropType<string>;
    };
    link: {
        type: import("vue").PropType<string>;
        default: string;
    };
}>> & {
    onClick?: ((value?: any) => any) | undefined;
    "onDisabled-click"?: ((value: any) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    link: string;
}, {}>, {
    default?(_: {
        color: string | undefined;
    }): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
