import { UiExternalLinkStyles } from './index';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    color: {
        type: import("vue").PropType<string>;
    };
    link: {
        type: import("vue").PropType<string>;
        default: string;
    };
    linkhStyles: {
        type: import("vue").PropType<UiExternalLinkStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    color: {
        type: import("vue").PropType<string>;
    };
    link: {
        type: import("vue").PropType<string>;
        default: string;
    };
    linkhStyles: {
        type: import("vue").PropType<UiExternalLinkStyles>;
    };
}>>, {
    link: string;
}, {}>, {
    default?(_: {
        color: string | undefined;
    }): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
