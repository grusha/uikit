declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    to: {
        type: (ObjectConstructor | StringConstructor)[];
        required: true;
    };
    target: {
        type: StringConstructor;
        default: string;
    };
    styles: {
        type: ObjectConstructor;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    to: {
        type: (ObjectConstructor | StringConstructor)[];
        required: true;
    };
    target: {
        type: StringConstructor;
        default: string;
    };
    styles: {
        type: ObjectConstructor;
    };
}>>, {
    target: string;
}, {}>, {
    default?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
