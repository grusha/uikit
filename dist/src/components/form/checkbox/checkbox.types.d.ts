export default interface UiCheckboxStyles {
    checkbox?: string;
    input?: string;
    trigger?: string;
    container?: string;
    triggerChecked?: string;
    checked?: string;
    label?: string;
}
