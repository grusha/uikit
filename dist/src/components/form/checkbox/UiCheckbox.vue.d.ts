import UiCheckboxStyles from './checkbox.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    model: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    checkboxStyles: {
        type: import("vue").PropType<UiCheckboxStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "update:model"[], "update:model", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    model: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    checkboxStyles: {
        type: import("vue").PropType<UiCheckboxStyles>;
    };
}>> & {
    "onUpdate:model"?: ((...args: any[]) => any) | undefined;
}, {
    name: string;
    label: string;
    disabled: boolean;
    model: boolean;
}, {}>, {
    default?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
