declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<string[]>;
        default: () => never[];
    };
    title: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    radioGroupStyles: {
        type: import("vue").PropType<import("./radio-group.types").IUiRadioGroupStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: string) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<string[]>;
        default: () => never[];
    };
    title: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    radioGroupStyles: {
        type: import("vue").PropType<import("./radio-group.types").IUiRadioGroupStyles>;
        default: undefined;
    };
}>> & {
    "onUpdate:model"?: ((value: string) => any) | undefined;
}, {
    name: string;
    label: string;
    disabled: boolean;
    options: string[];
    title: string;
    model: string;
    errorMessage: string;
    radioGroupStyles: import("./radio-group.types").IUiRadioGroupStyles;
}, {}>;
export default _default;
