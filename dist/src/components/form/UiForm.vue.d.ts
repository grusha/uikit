declare const _default: import("vue").DefineComponent<{
    schema: {
        type: ObjectConstructor;
        default: () => null;
    };
    initialValues: {
        type: ObjectConstructor;
        default: () => null;
    };
}, {
    form: import("vue").Ref<null>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    schema: {
        type: ObjectConstructor;
        default: () => null;
    };
    initialValues: {
        type: ObjectConstructor;
        default: () => null;
    };
}>>, {
    initialValues: Record<string, any>;
    schema: Record<string, any>;
}, {}>;
export default _default;
