import { InputField, InputModel, UiInputRangeStyles } from './inputRange.types';
import UiInputStyles from '../input/input.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        required: true;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    maskitoOptions: {
        type: import("vue").PropType<any>;
        required: true;
        default: Required<import("@maskito/core").MaskitoOptions>;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    copy: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<InputModel>;
        required: true;
        default: () => {
            from: null;
            to: null;
        };
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputStyles: {
        type: import("vue").PropType<UiInputStyles>;
        default: undefined;
    };
    field: {
        type: import("vue").PropType<InputField>;
        required: true;
    };
    inputRangeStyles: {
        type: import("vue").PropType<UiInputRangeStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        required: true;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    maskitoOptions: {
        type: import("vue").PropType<any>;
        required: true;
        default: Required<import("@maskito/core").MaskitoOptions>;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    copy: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<InputModel>;
        required: true;
        default: () => {
            from: null;
            to: null;
        };
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputStyles: {
        type: import("vue").PropType<UiInputStyles>;
        default: undefined;
    };
    field: {
        type: import("vue").PropType<InputField>;
        required: true;
    };
    inputRangeStyles: {
        type: import("vue").PropType<UiInputRangeStyles>;
        default: undefined;
    };
}>> & {
    "onUpdate:model"?: ((value: any) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    maskitoOptions: any;
    type: string;
    copy: string;
    model: InputModel;
    errorMessage: string;
    inputStyles: UiInputStyles;
    inputRangeStyles: UiInputRangeStyles;
}, {}>;
export default _default;
