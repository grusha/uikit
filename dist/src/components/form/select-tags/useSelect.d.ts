import { ComputedRef } from 'vue';
import { SelectTagsProps, SelectTagsEmits, IUiSelectTagsStyles } from './select.types';
import { SelectOption } from '../select/select.types';
export declare const useSelect: (props: SelectTagsProps, emits: SelectTagsEmits, proxyStyles: ComputedRef<IUiSelectTagsStyles | undefined>) => {
    inputValue: import("vue").Ref<string>;
    isActive: import("vue").Ref<boolean>;
    dropdownItems: ComputedRef<SelectOption[] | undefined>;
    filteredDropdownItems: import("vue").Ref<{
        id: string;
        collectionName: string;
        title: string;
    }[] | undefined>;
    selectedOptionsIdList: ComputedRef<string[] | undefined>;
    activeDropdownItem: import("vue").Ref<number>;
    canAddNewDropdownOption: ComputedRef<boolean | 0 | undefined>;
    isLoading: import("vue").Ref<boolean>;
    placeholderValue: ComputedRef<string | undefined>;
    onSelect: (option: SelectOption) => void;
    onBlur: () => void;
    onFocusin: () => void;
    onEnter: () => void;
    onEscape: () => void;
    onDelete: (id?: string) => void;
    filterDropdownItems: () => SelectOption[] | undefined;
};
