import { SelectTagsIcon } from './select.types';
import { SelectOption } from '../select/select.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<SelectOption[]>;
        default: () => SelectOption[];
    };
    tooltip: {
        type: import("vue").PropType<import("./select.types").SelectTagsTooltip>;
        default: undefined;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    id: {
        type: import("vue").PropType<string>;
        required: true;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    model: {
        type: import("vue").PropType<SelectOption[]>;
        required: true;
        default: () => SelectOption[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    collectionName: {
        type: import("vue").PropType<string>;
        required: true;
    };
    expandable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    filterable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    defaultValue: {
        type: import("vue").PropType<SelectOption[]>;
    };
    dropdownIcon: {
        type: import("vue").PropType<SelectTagsIcon>;
        default: () => SelectTagsIcon;
    };
    selectTagsStyles: {
        type: import("vue").PropType<import("./select.types").IUiSelectTagsStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:options": (payload: import("../select/select.types").RawSelectOption) => void;
    "update:model": (payload: SelectOption[]) => void;
    focus: () => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<SelectOption[]>;
        default: () => SelectOption[];
    };
    tooltip: {
        type: import("vue").PropType<import("./select.types").SelectTagsTooltip>;
        default: undefined;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    id: {
        type: import("vue").PropType<string>;
        required: true;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    model: {
        type: import("vue").PropType<SelectOption[]>;
        required: true;
        default: () => SelectOption[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    collectionName: {
        type: import("vue").PropType<string>;
        required: true;
    };
    expandable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    filterable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    defaultValue: {
        type: import("vue").PropType<SelectOption[]>;
    };
    dropdownIcon: {
        type: import("vue").PropType<SelectTagsIcon>;
        default: () => SelectTagsIcon;
    };
    selectTagsStyles: {
        type: import("vue").PropType<import("./select.types").IUiSelectTagsStyles>;
        default: undefined;
    };
}>> & {
    onFocus?: (() => any) | undefined;
    "onUpdate:model"?: ((payload: SelectOption[]) => any) | undefined;
    "onUpdate:options"?: ((payload: import("../select/select.types").RawSelectOption) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    options: SelectOption[];
    tooltip: import("./select.types").SelectTagsTooltip;
    placeholder: string;
    multiple: boolean;
    model: SelectOption[];
    errorMessage: string;
    expandable: boolean;
    filterable: boolean;
    dropdownIcon: SelectTagsIcon;
    selectTagsStyles: import("./select.types").IUiSelectTagsStyles;
}, {}>, {
    label?(_: {}): any;
    dropdownOption?(_: {
        selectedOptionIdList: string[] | undefined;
        option: {
            id: string;
            collectionName: string;
            title: string;
        };
    }): any;
    errorMessage?(_: {
        errorMessage: string;
    }): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
