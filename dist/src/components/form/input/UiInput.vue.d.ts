import UiInputStyles from "./input.types";
declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    maskitoOptions: {
        type: import("vue").PropType<any>;
        required: true;
        default: Required<import("@maskito/core").MaskitoOptions>;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<any>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputStyles: {
        type: import("vue").PropType<UiInputStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    maskitoOptions: {
        type: import("vue").PropType<any>;
        required: true;
        default: Required<import("@maskito/core").MaskitoOptions>;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<any>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputStyles: {
        type: import("vue").PropType<UiInputStyles>;
        default: undefined;
    };
}>> & {
    "onUpdate:model"?: ((value: any) => any) | undefined;
}, {
    name: string;
    label: string;
    disabled: boolean;
    maskitoOptions: any;
    placeholder: string;
    type: string;
    model: any;
    errorMessage: string;
    inputStyles: UiInputStyles;
}, {}>;
export default _default;
