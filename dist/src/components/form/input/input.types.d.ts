export default interface UiInputStyles {
    defaultInput?: string;
    errorInput?: string;
    defaultLabel?: string;
    errorLabel?: string;
    activeLabel?: string;
    bodyInput?: string;
    wrapper?: string;
    input?: string;
    icon?: string;
    error?: string;
}
export type InputMaskaOptions = {
    maska: string;
    maskaTokkens: string;
};
