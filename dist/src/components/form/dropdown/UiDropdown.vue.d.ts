import { UiDropdownStyles } from './dropdown.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectedIndexList: {
        type: import("vue").PropType<number[]>;
        default: () => number[];
    };
    items: {
        type: import("vue").PropType<any>;
        default: never[];
    };
    dropdownStyles: {
        type: import("vue").PropType<UiDropdownStyles>;
        default: undefined;
    };
    isSelectMultiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    select: (value: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectedIndexList: {
        type: import("vue").PropType<number[]>;
        default: () => number[];
    };
    items: {
        type: import("vue").PropType<any>;
        default: never[];
    };
    dropdownStyles: {
        type: import("vue").PropType<UiDropdownStyles>;
        default: undefined;
    };
    isSelectMultiple: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
}>> & {
    onSelect?: ((value: any) => any) | undefined;
}, {
    disabled: boolean;
    selectedIndexList: number[];
    items: any;
    dropdownStyles: UiDropdownStyles;
    isSelectMultiple: boolean;
}, {}>, {
    activator?(_: {
        on: {
            click: () => void;
        };
        isActive: boolean;
    }): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
