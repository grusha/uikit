import { UiInputTagsStyles } from './input-tags.types';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string[]>;
        default: () => never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputTagsStyles: {
        type: import("vue").PropType<UiInputTagsStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: string[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string[]>;
        default: () => never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputTagsStyles: {
        type: import("vue").PropType<UiInputTagsStyles>;
        default: undefined;
    };
}>> & {
    "onUpdate:model"?: ((value: string[]) => any) | undefined;
}, {
    name: string;
    label: string;
    disabled: boolean;
    placeholder: string;
    model: string[];
    errorMessage: string;
    inputTagsStyles: UiInputTagsStyles;
}, {}>;
export default _default;
