export default interface UiDateRangeStyles {
    dateRange?: string;
    label?: string;
    required?: string;
    error?: string;
    input: string;
    inputError: string;
}
