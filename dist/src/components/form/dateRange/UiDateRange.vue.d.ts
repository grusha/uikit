import UiDateRangeStyles from './daterange.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    styles: {
        type: import("vue").PropType<UiDateRangeStyles>;
        default: undefined;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<any>;
        default: never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: Date[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    styles: {
        type: import("vue").PropType<UiDateRangeStyles>;
        default: undefined;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<any>;
        default: never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
}>> & {
    "onUpdate:model"?: ((value: Date[]) => any) | undefined;
}, {
    label: string;
    placeholder: string;
    styles: UiDateRangeStyles;
    type: string;
    model: any;
    errorMessage: string;
}, {}>;
export default _default;
