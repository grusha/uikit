import { Value } from 'vue-slider-component';
import { UiSliderStyles } from './slider.types';
type SliderData = {
    value: number;
    label: string;
};
declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
    };
    model: {
        type: import("vue").PropType<Value>;
        required: true;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        required: true;
    };
    label: {
        type: import("vue").PropType<string>;
    };
    data: {
        type: import("vue").PropType<SliderData[]>;
        required: true;
    };
    sliderStyles: {
        type: import("vue").PropType<UiSliderStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: Value) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
    };
    model: {
        type: import("vue").PropType<Value>;
        required: true;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        required: true;
    };
    label: {
        type: import("vue").PropType<string>;
    };
    data: {
        type: import("vue").PropType<SliderData[]>;
        required: true;
    };
    sliderStyles: {
        type: import("vue").PropType<UiSliderStyles>;
    };
}>> & {
    "onUpdate:model"?: ((value: Value) => any) | undefined;
}, {}, {}>;
export default _default;
