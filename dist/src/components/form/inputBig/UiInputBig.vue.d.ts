import { UiInputBigStyles } from './inputBig.types';
import { IUiSelectTooltip } from '../select/select.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
    };
    maskitoOptions: {
        type: import("vue").PropType<any>;
        required: true;
        default: Required<import("@maskito/core").MaskitoOptions>;
    };
    tooltip: {
        type: import("vue").PropType<IUiSelectTooltip>;
        default: undefined;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string | number>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputBigStyles: {
        type: import("vue").PropType<UiInputBigStyles>;
        default: undefined;
    };
    getRawValue: {
        type: import("vue").PropType<any>;
        required: true;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: any) => void;
    copy: (value: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
    };
    maskitoOptions: {
        type: import("vue").PropType<any>;
        required: true;
        default: Required<import("@maskito/core").MaskitoOptions>;
    };
    tooltip: {
        type: import("vue").PropType<IUiSelectTooltip>;
        default: undefined;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string | number>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    inputBigStyles: {
        type: import("vue").PropType<UiInputBigStyles>;
        default: undefined;
    };
    getRawValue: {
        type: import("vue").PropType<any>;
        required: true;
        default: undefined;
    };
}>> & {
    onCopy?: ((value: any) => any) | undefined;
    "onUpdate:model"?: ((value: any) => any) | undefined;
}, {
    label: string;
    maskitoOptions: any;
    tooltip: IUiSelectTooltip;
    placeholder: string;
    type: string;
    model: string | number;
    errorMessage: string;
    inputBigStyles: UiInputBigStyles;
    getRawValue: any;
}, {}>;
export default _default;
