export interface UiInputBigStyles {
    uiInputBig?: string;
    label?: string;
    required?: string;
    wrapper?: string;
    input?: string;
    inputError?: string;
    icon?: string;
    error?: string;
    tooltipText?: string;
    tooltipIcon?: string;
}
