export declare const useInput: (props: any, emit?: any) => {
    inputValue: import("vue").WritableComputedRef<any>;
    isActive: import("vue").ComputedRef<boolean>;
    onFocus: () => void;
};
