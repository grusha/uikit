import { SelectOption } from '../select/select.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
    };
    selectedIndexList: {
        type: import("vue").PropType<any[]>;
        default: () => number[];
    };
    items: {
        type: import("vue").PropType<SelectOption[]>;
        default: () => SelectOption[];
    };
    dropdownStyles: {
        type: import("vue").PropType<import("./dropdown.types").UiDropdownStyles>;
        default: undefined;
    };
    dropdownId: {
        type: import("vue").PropType<string>;
        required: true;
    };
    optionsLength: {
        type: import("vue").PropType<number>;
        required: true;
    };
    inputValue: {
        type: import("vue").PropType<string>;
        default: string;
    };
    collectionName: {
        type: import("vue").PropType<string>;
        required: true;
        default: string;
    };
    expandable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    select: (value: any) => void;
    focus: () => void;
    addOption: (value: string) => void;
    "update:inputValue": (value: string) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
    };
    selectedIndexList: {
        type: import("vue").PropType<any[]>;
        default: () => number[];
    };
    items: {
        type: import("vue").PropType<SelectOption[]>;
        default: () => SelectOption[];
    };
    dropdownStyles: {
        type: import("vue").PropType<import("./dropdown.types").UiDropdownStyles>;
        default: undefined;
    };
    dropdownId: {
        type: import("vue").PropType<string>;
        required: true;
    };
    optionsLength: {
        type: import("vue").PropType<number>;
        required: true;
    };
    inputValue: {
        type: import("vue").PropType<string>;
        default: string;
    };
    collectionName: {
        type: import("vue").PropType<string>;
        required: true;
        default: string;
    };
    expandable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
}>> & {
    onFocus?: (() => any) | undefined;
    onSelect?: ((value: any) => any) | undefined;
    onAddOption?: ((value: string) => any) | undefined;
    "onUpdate:inputValue"?: ((value: string) => any) | undefined;
}, {
    disabled: boolean;
    selectedIndexList: any[];
    items: SelectOption[];
    dropdownStyles: import("./dropdown.types").UiDropdownStyles;
    inputValue: string;
    collectionName: string;
    expandable: boolean;
}, {}>, {
    activator?(_: {
        on: {
            click: (event: Event) => void;
            keydown: (event: KeyboardEvent) => void;
            keyup: (event: KeyboardEvent) => void;
        };
        isActive: boolean;
    }): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
