import WysiwygToolbarSpace from './wysiwyg-toolbarSpace.types';
declare const _default: import("vue").DefineComponent<{
    wysiwygToolbarSpaceStyles: {
        type: import("vue").PropType<WysiwygToolbarSpace>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    wysiwygToolbarSpaceStyles: {
        type: import("vue").PropType<WysiwygToolbarSpace>;
    };
}>>, {}, {}>;
export default _default;
