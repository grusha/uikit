import WysiwygToolbar from './wysiwyg-toolbar.types';
import { ToolbarSchema } from '../index.types';
import WysiwygToolbarSpace from '../wysiwyg-toolbar-space/wysiwyg-toolbarSpace.types';
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    wysiwygToolbarSpaceStyles: {
        type: import("vue").PropType<WysiwygToolbarSpace>;
    };
    toolbarSchema: {
        type: import("vue").PropType<ToolbarSchema[]>;
        required: true;
    };
    wysiwygToolbarStyles: {
        type: import("vue").PropType<WysiwygToolbar>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "onToggleToolbarButton"[], "onToggleToolbarButton", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    wysiwygToolbarSpaceStyles: {
        type: import("vue").PropType<WysiwygToolbarSpace>;
    };
    toolbarSchema: {
        type: import("vue").PropType<ToolbarSchema[]>;
        required: true;
    };
    wysiwygToolbarStyles: {
        type: import("vue").PropType<WysiwygToolbar>;
    };
}>> & {
    onOnToggleToolbarButton?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
}, {}>;
export default _default;
