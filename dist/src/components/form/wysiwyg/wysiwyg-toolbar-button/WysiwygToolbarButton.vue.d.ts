import WysiwygToolbarButton from './wysiwyg-toolbarButton.types';
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    iconName: {
        type: import("vue").PropType<string>;
        default: string;
    };
    isActive: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    wysiwygToolbarButtonStyles: {
        type: import("vue").PropType<WysiwygToolbarButton>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    iconName: {
        type: import("vue").PropType<string>;
        default: string;
    };
    isActive: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    wysiwygToolbarButtonStyles: {
        type: import("vue").PropType<WysiwygToolbarButton>;
    };
}>> & {
    onClick?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    iconName: string;
    isActive: boolean;
}, {}>;
export default _default;
