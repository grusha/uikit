export default interface WysiwygToolbarButton {
    btn?: string;
    toolbarButton?: string;
    defaultTheme?: string;
    activeTheme?: string;
    typeIconSquare?: string;
}
