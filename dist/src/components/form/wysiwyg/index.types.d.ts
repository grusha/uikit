import WysiwygErrorStyles from './wysiwyg-error/wysiwyg-error.types';
import WysiwygToolbarButton from './wysiwyg-toolbar-button/wysiwyg-toolbarButton.types';
import WysiwygToolbarSpace from './wysiwyg-toolbar-space/wysiwyg-toolbarSpace.types';
import WysiwygToolbar from './wysiwyg-toolbar/wysiwyg-toolbar.types';
import WysiwygStyles from './wysiwyg/wysiwyg.types';
type As = 'button' | 'space';
export type ToolbarButtons = {
    as: As;
    type: string;
    iconName?: string;
};
export interface UiWysiwyg {
    uiWysiwyg?: WysiwygStyles;
    wysiwygError?: WysiwygErrorStyles;
    wysiwygToolbar?: WysiwygToolbar;
    wysiwygToolbarButton?: WysiwygToolbarButton;
    wysiwygToolbarSpace?: WysiwygToolbarSpace;
}
export interface ToolbarSchema extends ToolbarButtons {
    isActive: boolean;
}
export {};
