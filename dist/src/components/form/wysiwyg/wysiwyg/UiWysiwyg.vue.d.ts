import { UiWysiwyg } from '../index.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    wysiwygStyles: {
        type: import("vue").PropType<UiWysiwyg>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "update:model"[], "update:model", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    wysiwygStyles: {
        type: import("vue").PropType<UiWysiwyg>;
    };
}>> & {
    "onUpdate:model"?: ((...args: any[]) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    placeholder: string;
    model: string;
    errorMessage: string;
}, {}>;
export default _default;
