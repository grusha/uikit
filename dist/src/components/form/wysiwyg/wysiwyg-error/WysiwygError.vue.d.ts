import WysiwygErrorStyles from './wysiwyg-error.types';
declare const _default: import("vue").DefineComponent<{
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    wysiwygErrorStyles: {
        type: import("vue").PropType<WysiwygErrorStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    wysiwygErrorStyles: {
        type: import("vue").PropType<WysiwygErrorStyles>;
    };
}>>, {
    errorMessage: string;
}, {}>;
export default _default;
