import { Editor } from '@tiptap/vue-3';
export declare const useEditor: (props: any, emit: any) => {
    editor: Editor;
    setLink: () => void;
    isActive: (name: string) => boolean;
};
