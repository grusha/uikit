import { BoundsType, LocationOptions, VueDadataClasses, HighlightOptions } from 'vue-dadata';
import UiDadataStyles from './dadata.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        required: true;
    };
    autocomplete: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    url: {
        type: import("vue").PropType<string>;
        default: undefined;
    };
    token: {
        type: import("vue").PropType<string>;
        required: true;
    };
    debounceWait: {
        type: import("vue").PropType<string>;
        default: string;
    };
    fromBound: {
        type: import("vue").PropType<BoundsType>;
        default: undefined;
    };
    toBound: {
        type: import("vue").PropType<BoundsType>;
        default: undefined;
    };
    inputName: {
        type: import("vue").PropType<string>;
        default: undefined;
    };
    locationOptions: {
        type: import("vue").PropType<LocationOptions>;
        default: undefined;
    };
    classes: {
        type: import("vue").PropType<VueDadataClasses>;
        default: undefined;
    };
    highlightOptions: {
        type: import("vue").PropType<HighlightOptions>;
        default: undefined;
    };
    dadataStyles: {
        type: import("vue").PropType<UiDadataStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: string) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string>;
        required: true;
    };
    autocomplete: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    url: {
        type: import("vue").PropType<string>;
        default: undefined;
    };
    token: {
        type: import("vue").PropType<string>;
        required: true;
    };
    debounceWait: {
        type: import("vue").PropType<string>;
        default: string;
    };
    fromBound: {
        type: import("vue").PropType<BoundsType>;
        default: undefined;
    };
    toBound: {
        type: import("vue").PropType<BoundsType>;
        default: undefined;
    };
    inputName: {
        type: import("vue").PropType<string>;
        default: undefined;
    };
    locationOptions: {
        type: import("vue").PropType<LocationOptions>;
        default: undefined;
    };
    classes: {
        type: import("vue").PropType<VueDadataClasses>;
        default: undefined;
    };
    highlightOptions: {
        type: import("vue").PropType<HighlightOptions>;
        default: undefined;
    };
    dadataStyles: {
        type: import("vue").PropType<UiDadataStyles>;
    };
}>> & {
    "onUpdate:model"?: ((value: string) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    placeholder: string;
    autocomplete: boolean;
    url: string;
    debounceWait: string;
    fromBound: BoundsType;
    toBound: BoundsType;
    inputName: string;
    locationOptions: LocationOptions;
    classes: VueDadataClasses;
    highlightOptions: HighlightOptions;
}, {}>;
export default _default;
