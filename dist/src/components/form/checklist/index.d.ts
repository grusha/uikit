import CheckListItemStyles from './checklistitem/checklist-item.types';
import { UiCheckListStyles } from './UiChecklist/checklist.types';
export interface Checklist {
    uiChecklist: UiCheckListStyles;
    checklistItem: CheckListItemStyles;
}
