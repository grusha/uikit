import { UiCheckListStyles } from './checklist.types';
import CheckListItemStyles from '../checklistitem/checklist-item.types';
declare const _default: import("vue").DefineComponent<{
    options: {
        type: import("vue").PropType<string[]>;
        required: true;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string[]>;
        required: true;
        default: () => string[];
    };
    checklistStyles: {
        type: import("vue").PropType<UiCheckListStyles>;
    };
    checklistItemStyles: {
        type: import("vue").PropType<CheckListItemStyles>;
    };
    defaultOption: {
        type: import("vue").PropType<string>;
        default: string;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (model: string[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    options: {
        type: import("vue").PropType<string[]>;
        required: true;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string[]>;
        required: true;
        default: () => string[];
    };
    checklistStyles: {
        type: import("vue").PropType<UiCheckListStyles>;
    };
    checklistItemStyles: {
        type: import("vue").PropType<CheckListItemStyles>;
    };
    defaultOption: {
        type: import("vue").PropType<string>;
        default: string;
    };
}>> & {
    "onUpdate:model"?: ((model: string[]) => any) | undefined;
}, {
    type: string;
    model: string[];
    defaultOption: string;
}, {}>;
export default _default;
