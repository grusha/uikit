import { UiField, UiFieldStyles } from './field.types';
declare const _default: import("vue").DefineComponent<{
    field: {
        type: import("vue").PropType<UiField>;
        required: true;
    };
    fieldStyles: {
        type: import("vue").PropType<UiFieldStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update-options": (value: {
        collectionName: string;
        title: string;
    }) => void;
    "change-field": (value: any) => void;
    "on-upload-files": (value?: any) => void;
    focus: () => void;
    "handle-upload-files-errors": (value: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    field: {
        type: import("vue").PropType<UiField>;
        required: true;
    };
    fieldStyles: {
        type: import("vue").PropType<UiFieldStyles>;
    };
}>> & {
    onFocus?: (() => any) | undefined;
    "onOn-upload-files"?: ((value?: any) => any) | undefined;
    "onHandle-upload-files-errors"?: ((value: any) => any) | undefined;
    "onUpdate-options"?: ((value: {
        collectionName: string;
        title: string;
    }) => any) | undefined;
    "onChange-field"?: ((value: any) => any) | undefined;
}, {}, {}>;
export default _default;
