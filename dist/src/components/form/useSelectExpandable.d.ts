import { Ref } from 'vue';
import { IUiSelectEmits, SelectOption } from './select/select.types';
declare const useSelect: (props: any, isLoading: Ref<boolean>, emits: IUiSelectEmits) => {
    selectedIndex: import("vue").ComputedRef<any>;
    inputValue: Ref<any>;
    dropdownItems: import("vue").ComputedRef<any>;
    filteredDropdownItems: Ref<any>;
    filterDropdownItems: (value: string) => void;
    onSelect: (item: SelectOption) => void;
    onFocus: () => void;
    addOption: (item: string) => void;
};
export { useSelect, };
