import { UiSwitchStyles } from './switch.types';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    model: {
        type: import("vue").PropType<boolean>;
        required: true;
    };
    switchStyles: {
        type: import("vue").PropType<UiSwitchStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: boolean) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    model: {
        type: import("vue").PropType<boolean>;
        required: true;
    };
    switchStyles: {
        type: import("vue").PropType<UiSwitchStyles>;
    };
}>> & {
    "onUpdate:model"?: ((value: boolean) => any) | undefined;
}, {
    name: string;
    disabled: boolean;
}, {}>;
export default _default;
