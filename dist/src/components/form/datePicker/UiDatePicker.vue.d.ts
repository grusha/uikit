import UiDatePickerStyles from './datepicker.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    styles: {
        type: import("vue").PropType<UiDatePickerStyles>;
        default: undefined;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    format: {
        type: import("vue").PropType<(date: Date) => string>;
        required: true;
        default: undefined;
    };
    model: {
        type: import("vue").PropType<Date>;
        default: undefined;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    enableTimePicker: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    allowedDates: {
        type: import("vue").PropType<() => Date[]>;
        default: () => never[];
    };
    isYearPicker: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: Date | undefined) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    styles: {
        type: import("vue").PropType<UiDatePickerStyles>;
        default: undefined;
    };
    type: {
        type: import("vue").PropType<string>;
        default: string;
    };
    format: {
        type: import("vue").PropType<(date: Date) => string>;
        required: true;
        default: undefined;
    };
    model: {
        type: import("vue").PropType<Date>;
        default: undefined;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    enableTimePicker: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    allowedDates: {
        type: import("vue").PropType<() => Date[]>;
        default: () => never[];
    };
    isYearPicker: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
}>> & {
    "onUpdate:model"?: ((value: Date | undefined) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    placeholder: string;
    styles: UiDatePickerStyles;
    type: string;
    format: (date: Date) => string;
    model: Date;
    errorMessage: string;
    enableTimePicker: boolean;
    allowedDates: () => Date[];
    isYearPicker: boolean;
}, {}>;
export default _default;
