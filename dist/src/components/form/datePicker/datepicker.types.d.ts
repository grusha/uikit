export default interface UiDatePickerStyles {
    dateRange?: string;
    label?: string;
    required?: string;
    error?: string;
    input?: string;
    inputError?: string;
}
