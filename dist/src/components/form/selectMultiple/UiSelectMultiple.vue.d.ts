import { UiDropdownStyles } from '../dropdown/dropdown.types';
import { UiSelectMultipleStyles } from './select.multiple.types';
declare const _default: import("vue").DefineComponent<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<string[]>;
        default: () => string[];
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string[]>;
        required: true;
        default: () => never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    dropdownStyles: {
        type: import("vue").PropType<UiDropdownStyles>;
        default: undefined;
    };
    inset: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectStyles: {
        type: import("vue").PropType<UiSelectMultipleStyles>;
        default: undefined;
    };
    defaultValue: {
        type: import("vue").PropType<string>;
        default: string;
    };
    isMultiple: {
        type: import("vue").PropType<boolean>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: string[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<string[]>;
        default: () => string[];
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    model: {
        type: import("vue").PropType<string[]>;
        required: true;
        default: () => never[];
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    dropdownStyles: {
        type: import("vue").PropType<UiDropdownStyles>;
        default: undefined;
    };
    inset: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectStyles: {
        type: import("vue").PropType<UiSelectMultipleStyles>;
        default: undefined;
    };
    defaultValue: {
        type: import("vue").PropType<string>;
        default: string;
    };
    isMultiple: {
        type: import("vue").PropType<boolean>;
    };
}>> & {
    "onUpdate:model"?: ((value: string[]) => any) | undefined;
}, {
    label: string;
    disabled: boolean;
    options: string[];
    placeholder: string;
    model: string[];
    errorMessage: string;
    dropdownStyles: UiDropdownStyles;
    inset: boolean;
    selectStyles: UiSelectMultipleStyles;
    defaultValue: string;
}, {}>;
export default _default;
