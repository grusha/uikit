declare const useSelect: (props: any, emits: any) => {
    selectedIndex: import("vue").ComputedRef<any>;
    dropdownItems: import("vue").ComputedRef<any>;
    onSelect: (index: number) => void;
};
export { useSelect, };
