import { UiRadioStyles } from './radio.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    value: {
        type: import("vue").PropType<string | number>;
        required: true;
    };
    group: {
        type: import("vue").PropType<string>;
        default: string;
    };
    modeValue: {
        type: import("vue").PropType<string | number>;
    };
    radioStyles: {
        type: import("vue").PropType<UiRadioStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:modelValue": (value: string | number) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    value: {
        type: import("vue").PropType<string | number>;
        required: true;
    };
    group: {
        type: import("vue").PropType<string>;
        default: string;
    };
    modeValue: {
        type: import("vue").PropType<string | number>;
    };
    radioStyles: {
        type: import("vue").PropType<UiRadioStyles>;
    };
}>> & {
    "onUpdate:modelValue"?: ((value: string | number) => any) | undefined;
}, {
    name: string;
    label: string;
    group: string;
}, {}>, {
    default?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
