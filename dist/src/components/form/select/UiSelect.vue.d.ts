import { SelectOption } from './select.types';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
        default: string;
    };
    label: {
        type: import("vue").PropType<string | number>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<SelectOption[]>;
        default: () => SelectOption[];
    };
    tooltip: {
        type: import("vue").PropType<import("./select.types").IUiSelectTooltip>;
        default: undefined;
    };
    placeholder: {
        type: import("vue").PropType<string | number>;
        default: string;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
    };
    model: {
        type: import("vue").PropType<any>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    collectionName: {
        type: import("vue").PropType<string>;
        default: string;
    };
    expandable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    inset: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    filterable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectStyles: {
        type: import("vue").PropType<import("./select.types").IUiSelectStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (value: SelectOption) => void;
    "update:options": (value: {
        title: string;
        collectionName: string;
    }) => void;
    onFocus: () => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
        default: string;
    };
    label: {
        type: import("vue").PropType<string | number>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    options: {
        type: import("vue").PropType<SelectOption[]>;
        default: () => SelectOption[];
    };
    tooltip: {
        type: import("vue").PropType<import("./select.types").IUiSelectTooltip>;
        default: undefined;
    };
    placeholder: {
        type: import("vue").PropType<string | number>;
        default: string;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
    };
    model: {
        type: import("vue").PropType<any>;
        default: string;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    collectionName: {
        type: import("vue").PropType<string>;
        default: string;
    };
    expandable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    inset: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    filterable: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    selectStyles: {
        type: import("vue").PropType<import("./select.types").IUiSelectStyles>;
        default: undefined;
    };
}>> & {
    "onUpdate:model"?: ((value: SelectOption) => any) | undefined;
    "onUpdate:options"?: ((value: {
        title: string;
        collectionName: string;
    }) => any) | undefined;
    onOnFocus?: (() => any) | undefined;
}, {
    name: string;
    label: string | number;
    disabled: boolean;
    options: SelectOption[];
    tooltip: import("./select.types").IUiSelectTooltip;
    placeholder: string | number;
    model: any;
    errorMessage: string;
    collectionName: string;
    expandable: boolean;
    inset: boolean;
    filterable: boolean;
    selectStyles: import("./select.types").IUiSelectStyles;
}, {}>;
export default _default;
