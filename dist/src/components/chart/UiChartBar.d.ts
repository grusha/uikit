declare const _default: import("vue").DefineComponent<{
    data: {
        type: ObjectConstructor;
        required: true;
    };
    hasLabels: {
        type: BooleanConstructor;
        default: boolean;
    };
}, {
    options: import("vue").ComputedRef<{
        maintainAspectRatio: boolean;
        responsive: boolean;
        layout: {
            padding: {
                top: number;
                right: number;
                bottom: number;
                left: number;
            };
        };
        plugins: {
            tooltip: {
                enabled: boolean;
            };
            legend: {
                display: boolean;
            };
            datalabels: {
                color: string;
                labels: {
                    title: {
                        anchor: string;
                        align: string;
                    };
                    value: {
                        anchor: string;
                        align: string;
                        formatter(value: any, context: any): any;
                        display?: undefined;
                    } | {
                        display: boolean;
                        anchor?: undefined;
                        align?: undefined;
                    };
                };
                font: {
                    weight: string;
                    size: number;
                };
            };
        };
        title: {
            display: boolean;
            text: string;
        };
        scales: {
            xAxes: {
                display: boolean;
            };
            yAxes: {
                display: boolean;
            };
        };
    }>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ObjectConstructor;
        required: true;
    };
    hasLabels: {
        type: BooleanConstructor;
        default: boolean;
    };
}>>, {
    hasLabels: boolean;
}, {}>;
export default _default;
