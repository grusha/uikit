declare const UiChartDoughnut: {
    (props: any): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    props: {
        data: {
            type: ObjectConstructor;
            required: boolean;
        };
    };
};
export default UiChartDoughnut;
