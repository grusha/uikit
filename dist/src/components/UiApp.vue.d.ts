import { Styles } from "./";
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    styles: {
        type: import("vue").PropType<Styles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    styles: {
        type: import("vue").PropType<Styles>;
    };
}>>, {}, {}>, {
    default?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
