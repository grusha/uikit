import { AccountInfo, DefaultAvatarSize, UiAvatarStyles } from './avatar.types';
declare const _default: import("vue").DefineComponent<{
    accountInfo: {
        type: import("vue").PropType<AccountInfo>;
        required: true;
    };
    defaultAvatarSize: {
        type: import("vue").PropType<DefaultAvatarSize>;
        default: () => {
            height: number;
            width: number;
        };
    };
    isAdminMode: {
        type: import("vue").PropType<boolean>;
        required: true;
        default: boolean;
    };
    avatarStyles: {
        type: import("vue").PropType<UiAvatarStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    accountInfo: {
        type: import("vue").PropType<AccountInfo>;
        required: true;
    };
    defaultAvatarSize: {
        type: import("vue").PropType<DefaultAvatarSize>;
        default: () => {
            height: number;
            width: number;
        };
    };
    isAdminMode: {
        type: import("vue").PropType<boolean>;
        required: true;
        default: boolean;
    };
    avatarStyles: {
        type: import("vue").PropType<UiAvatarStyles>;
    };
}>>, {
    defaultAvatarSize: DefaultAvatarSize;
    isAdminMode: boolean;
}, {}>;
export default _default;
