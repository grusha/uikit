import { uiCollapsibleStyles } from './collapsible/collapsible.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    styles: {
        type: import("vue").PropType<uiCollapsibleStyles>;
        default: undefined;
    };
    index: {
        type: import("vue").PropType<number>;
        required: true;
    };
    isOpen: {
        type: import("vue").PropType<boolean>;
        required: true;
        default: boolean;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:isOpen": (value: boolean, index: number) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    styles: {
        type: import("vue").PropType<uiCollapsibleStyles>;
        default: undefined;
    };
    index: {
        type: import("vue").PropType<number>;
        required: true;
    };
    isOpen: {
        type: import("vue").PropType<boolean>;
        required: true;
        default: boolean;
    };
}>> & {
    "onUpdate:isOpen"?: ((value: boolean, index: number) => any) | undefined;
}, {
    styles: uiCollapsibleStyles;
    isOpen: boolean;
}, {}>, {
    header?(_: {}): any;
    default?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
