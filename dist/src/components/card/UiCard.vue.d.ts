import { UiCardStyles } from './card.types';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    cardStyles: {
        type: import("vue").PropType<UiCardStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    cardStyles: {
        type: import("vue").PropType<UiCardStyles>;
    };
}>>, {}, {}>, {
    default?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
