import { UiRoutesStyles } from './routes.types';
declare const _default: import("vue").DefineComponent<{
    routes: {
        type: import("vue").PropType<any[]>;
        default: () => never[];
    };
    routesStyles: {
        type: import("vue").PropType<UiRoutesStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    routes: {
        type: import("vue").PropType<any[]>;
        default: () => never[];
    };
    routesStyles: {
        type: import("vue").PropType<UiRoutesStyles>;
    };
}>>, {
    routes: any[];
}, {}>;
export default _default;
