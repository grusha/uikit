import { UiPreloaderStyles } from './preloader.types';
declare const _default: import("vue").DefineComponent<{
    isCentered: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    preloaderStyles: {
        type: import("vue").PropType<UiPreloaderStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    isCentered: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    preloaderStyles: {
        type: import("vue").PropType<UiPreloaderStyles>;
    };
}>>, {
    isCentered: boolean;
}, {}>;
export default _default;
