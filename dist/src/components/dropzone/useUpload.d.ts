export declare const useFileInput: ({ props, emit, input, proxyFile, }: {
    props: any;
    emit: any;
    input: any;
    proxyFile: any;
}) => {
    fileId: import("vue").ComputedRef<string>;
    acceptedFiles: import("vue").ComputedRef<any>;
    onChange: (event: any) => void;
};
export declare const useDropzone: ({ props, emit, input, proxyFile, proxyStyles, }: {
    props: any;
    emit: any;
    input: any;
    proxyFile: any;
    proxyStyles: any;
}) => {
    dragover: (event: any) => void;
    dragleave: (event: any) => void;
    drop: (event: any) => void;
};
