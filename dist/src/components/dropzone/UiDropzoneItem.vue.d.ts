import { UiDropzoneItemStyles } from './index';
declare const _default: import("vue").DefineComponent<{
    file: {
        type: import("vue").PropType<any>;
        default: () => void;
    };
    canEdit: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    dropzoneItemStyles: {
        type: import("vue").PropType<UiDropzoneItemStyles>;
    };
    buttonStyles: {
        type: import("vue").PropType<any>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    delete: (value: any) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    file: {
        type: import("vue").PropType<any>;
        default: () => void;
    };
    canEdit: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    dropzoneItemStyles: {
        type: import("vue").PropType<UiDropzoneItemStyles>;
    };
    buttonStyles: {
        type: import("vue").PropType<any>;
    };
}>> & {
    onDelete?: ((value: any) => any) | undefined;
}, {
    file: any;
    canEdit: boolean;
}, {}>;
export default _default;
