import { UiDropzoneStyles } from './index';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
    };
    limit: {
        type: import("vue").PropType<number>;
        default: number;
    };
    accept: {
        type: import("vue").PropType<string[]>;
        default: () => string[];
    };
    model: {
        type: import("vue").PropType<any>;
        default: () => never[];
    };
    loading: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    dropzoneStyles: {
        type: import("vue").PropType<UiDropzoneStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("update:model" | "handle-upload-files-errors" | "on-upload-files")[], "update:model" | "handle-upload-files-errors" | "on-upload-files", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        default: string;
    };
    label: {
        type: import("vue").PropType<string>;
        default: string;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    placeholder: {
        type: import("vue").PropType<string>;
        default: string;
    };
    multiple: {
        type: import("vue").PropType<boolean>;
    };
    limit: {
        type: import("vue").PropType<number>;
        default: number;
    };
    accept: {
        type: import("vue").PropType<string[]>;
        default: () => string[];
    };
    model: {
        type: import("vue").PropType<any>;
        default: () => never[];
    };
    loading: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    errorMessage: {
        type: import("vue").PropType<string>;
        default: string;
    };
    dropzoneStyles: {
        type: import("vue").PropType<UiDropzoneStyles>;
    };
}>> & {
    "onUpdate:model"?: ((...args: any[]) => any) | undefined;
    "onHandle-upload-files-errors"?: ((...args: any[]) => any) | undefined;
    "onOn-upload-files"?: ((...args: any[]) => any) | undefined;
}, {
    name: string;
    label: string;
    disabled: boolean;
    placeholder: string;
    limit: number;
    accept: string[];
    model: any;
    loading: boolean;
    errorMessage: string;
}, {}>;
export default _default;
