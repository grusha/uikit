import { DropzoneFileStyles, IKNDropzoneStyles } from "../ikn-dropzone/dropzone.types";
export interface UiDropzoneStyles {
    label?: string;
    labelRequired?: string;
    dropzone?: string;
    dropzoneFileInput?: string;
    dropzoneLabel?: string;
    dropzoneError?: string;
    dropzoneDragover?: string;
    dropzoneDragleave?: string;
    error?: string;
}
export interface UiDropzoneItemStyles {
    dropzoneItem?: string;
    image?: string;
    icon?: string;
    preparedFile?: string;
    wrapper?: string;
    title?: string;
    wrapperBtn?: string;
    button?: string;
}
export interface Dropzone {
    uiDropzone: UiDropzoneStyles;
    dropzoneItem: UiDropzoneItemStyles;
    iknDropzone: IKNDropzoneStyles;
    iknDropzoneFileList: DropzoneFileStyles;
}
