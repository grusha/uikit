declare const _default: import("vue").DefineComponent<{
    type: {
        type: import("vue").PropType<string>;
        required: true;
    };
    title: {
        type: import("vue").PropType<string>;
        required: true;
    };
    fullPath: {
        type: import("vue").PropType<any[]>;
        required: true;
    };
    path: {
        type: import("vue").PropType<string>;
        required: true;
    };
    router: {
        type: import("vue").PropType<any>;
        required: true;
    };
    route: {
        type: import("vue").PropType<any>;
        required: true;
    };
    styles: {
        type: import("vue").PropType<any>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: import("vue").PropType<string>;
        required: true;
    };
    title: {
        type: import("vue").PropType<string>;
        required: true;
    };
    fullPath: {
        type: import("vue").PropType<any[]>;
        required: true;
    };
    path: {
        type: import("vue").PropType<string>;
        required: true;
    };
    router: {
        type: import("vue").PropType<any>;
        required: true;
    };
    route: {
        type: import("vue").PropType<any>;
        required: true;
    };
    styles: {
        type: import("vue").PropType<any>;
    };
}>>, {}, {}>;
export default _default;
