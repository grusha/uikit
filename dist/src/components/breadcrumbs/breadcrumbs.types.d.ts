export interface UiBreadcrumbsStyles {
    wrapper?: string;
    breadcrumbs?: string;
    content?: string;
    link?: string;
    title?: string;
    route?: string;
    routeActive?: string;
}
