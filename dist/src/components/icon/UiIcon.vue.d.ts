declare const _default: import("vue").DefineComponent<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
    };
    prefix: {
        type: import("vue").PropType<string>;
        default: string;
    };
    color: {
        type: import("vue").PropType<string>;
        default: string;
    };
    strokeColor: {
        type: import("vue").PropType<string>;
        default: string;
    };
    width: {
        type: import("vue").PropType<number>;
        default: number;
    };
    height: {
        type: import("vue").PropType<number>;
        default: number;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: import("vue").PropType<string>;
        required: true;
    };
    prefix: {
        type: import("vue").PropType<string>;
        default: string;
    };
    color: {
        type: import("vue").PropType<string>;
        default: string;
    };
    strokeColor: {
        type: import("vue").PropType<string>;
        default: string;
    };
    width: {
        type: import("vue").PropType<number>;
        default: number;
    };
    height: {
        type: import("vue").PropType<number>;
        default: number;
    };
}>>, {
    prefix: string;
    color: string;
    strokeColor: string;
    width: number;
    height: number;
}, {}>;
export default _default;
