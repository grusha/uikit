import { SelectOption } from '../form/select/select.types';
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    styles: {
        type: import("vue").PropType<import("./popup-select.types").PopupSelectStyles>;
    };
    model: {
        type: import("vue").PropType<string[]>;
    };
    selectOptions: {
        type: import("vue").PropType<SelectOption[]>;
        required: true;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "update:model": (payload: string[]) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    styles: {
        type: import("vue").PropType<import("./popup-select.types").PopupSelectStyles>;
    };
    model: {
        type: import("vue").PropType<string[]>;
    };
    selectOptions: {
        type: import("vue").PropType<SelectOption[]>;
        required: true;
    };
}>> & {
    "onUpdate:model"?: ((payload: string[]) => any) | undefined;
}, {
    disabled: boolean;
}, {}>;
export default _default;
