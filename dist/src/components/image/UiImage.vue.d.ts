import { UiImageStyles } from './image.types';
import { UiPreloaderStyles } from '..';
declare const _default: import("vue").DefineComponent<{
    alt: {
        type: import("vue").PropType<string>;
        default: string;
    };
    src: {
        type: import("vue").PropType<string>;
        required: true;
    };
    preloaderStyles: {
        type: import("vue").PropType<UiPreloaderStyles>;
        default: undefined;
    };
    dataSrc: {
        type: import("vue").PropType<string>;
        default: string;
    };
    preview: {
        type: import("vue").PropType<string>;
        default: string;
    };
    fallback: {
        type: import("vue").PropType<string>;
        default: string;
    };
    preloader: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    imageStyles: {
        type: import("vue").PropType<UiImageStyles>;
        default: undefined;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    alt: {
        type: import("vue").PropType<string>;
        default: string;
    };
    src: {
        type: import("vue").PropType<string>;
        required: true;
    };
    preloaderStyles: {
        type: import("vue").PropType<UiPreloaderStyles>;
        default: undefined;
    };
    dataSrc: {
        type: import("vue").PropType<string>;
        default: string;
    };
    preview: {
        type: import("vue").PropType<string>;
        default: string;
    };
    fallback: {
        type: import("vue").PropType<string>;
        default: string;
    };
    preloader: {
        type: import("vue").PropType<boolean>;
        default: boolean;
    };
    imageStyles: {
        type: import("vue").PropType<UiImageStyles>;
        default: undefined;
    };
}>>, {
    alt: string;
    preloaderStyles: UiPreloaderStyles;
    dataSrc: string;
    preview: string;
    fallback: string;
    preloader: boolean;
    imageStyles: UiImageStyles;
}, {}>;
export default _default;
