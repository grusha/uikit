import { UiPaginationStyles } from './pagination.types';
declare const _default: import("vue").DefineComponent<{
    limit: {
        type: import("vue").PropType<number>;
        default: number;
    };
    total: {
        type: import("vue").PropType<number>;
        default: number;
    };
    page: {
        type: import("vue").PropType<number>;
        default: number;
    };
    maxVisiblePage: {
        type: import("vue").PropType<number>;
        default: number;
    };
    paginationStyles: {
        type: import("vue").PropType<UiPaginationStyles>;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {
    "change-page": (value: any) => void;
    click: (value: number) => void;
}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    limit: {
        type: import("vue").PropType<number>;
        default: number;
    };
    total: {
        type: import("vue").PropType<number>;
        default: number;
    };
    page: {
        type: import("vue").PropType<number>;
        default: number;
    };
    maxVisiblePage: {
        type: import("vue").PropType<number>;
        default: number;
    };
    paginationStyles: {
        type: import("vue").PropType<UiPaginationStyles>;
    };
}>> & {
    onClick?: ((value: number) => any) | undefined;
    "onChange-page"?: ((value: any) => any) | undefined;
}, {
    limit: number;
    total: number;
    page: number;
    maxVisiblePage: number;
}, {}>;
export default _default;
