export interface UiPaginationStyles {
    pagination: string;
    btnDefault: string;
    btnPrev: string;
    btnNext: string;
    btnEllipsis: string;
    btnActive: string;
}
