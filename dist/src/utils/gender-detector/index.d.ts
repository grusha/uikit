/**
 * @return {Number} 0 - female, 1 - male, null - can't detect
 */
export declare const detectGender: (firstName: string, middleName: string, lastName: string) => "Женский" | "Мужской" | null;
