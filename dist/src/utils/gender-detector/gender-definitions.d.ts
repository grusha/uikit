export declare const FEMALE = "female";
export declare const MALE = "male";
export declare const FEMALE_GENDER_NAME = "\u0416\u0435\u043D\u0441\u043A\u0438\u0439";
export declare const MALE_GENDER_NAME = "\u041C\u0443\u0436\u0441\u043A\u043E\u0439";
export declare const LAST_NAME = 0;
export declare const MIDDLE_NAME = 1;
export type Gender = typeof FEMALE | typeof MALE;
export type GenderName = typeof FEMALE_GENDER_NAME | typeof MALE_GENDER_NAME;
export declare const LAST_NAME_COMPLETIONS: {
    female: string[];
    male: string[];
};
export declare const NAMES: {
    female: string[];
    male: string[];
};
export declare const MIDDLE_NAME_COMPLETIONS: {
    female: string[];
    male: string[];
};
