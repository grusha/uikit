export declare const resolveDefaultProfilePic: ({ firstName, middleName, lastName, gender, }: {
    firstName: string;
    middleName: string;
    lastName: string;
    gender: string;
}) => "female_default_avatar" | "male_default_avatar";
export declare const ensureProfileHasPhoto: (profile: any) => void;
