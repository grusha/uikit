import { Ref } from 'vue';
export * from './avatar.resolver';
export * from './file';
export * from './gender-detector';
export declare const getFIO: (profile: {
    lastName: string;
    firstName: string;
    middleName: string;
}) => string;
export declare const getGetObjDifference: (obj1: any, obj2: any, initialValue?: {}) => any;
export declare const difference: (source: any, target: any) => Partial<any>;
export declare const sanitize: (html: string) => string;
export declare const vHidden: {
    onMounted(el: HTMLElement, binding: Ref<boolean>): void;
};
