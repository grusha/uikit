import Lazy from './lazy';
import { LazyOptions } from './types';
export declare const lazy: (options: LazyOptions) => Lazy;
declare const _default: Lazy;
export default _default;
