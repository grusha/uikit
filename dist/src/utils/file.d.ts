import { Ref } from 'vue';
export declare const formatFileSize: (bytes?: number, decimals?: number) => string;
export declare const dataURLtoFile: (dataUrl: string, fileName: string) => File;
export declare const getExt: (fileName: string) => string | undefined;
export declare const downloadFile: (url: string, fileName: string | undefined) => Promise<void>;
export declare const prepareFile: (file: Ref<{
    _id: string;
    originalName: string;
    size: number;
    url: string;
    thumbnailUrl: string;
}> | {
    _id: string;
    originalName: string;
    size: number;
    url: string;
    thumbnailUrl: string;
}) => {
    _id: string;
    originalName: string;
    ext: string | undefined;
    size: string;
    url: string;
    thumbnailUrl: string;
};
