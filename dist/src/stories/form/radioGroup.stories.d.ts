import type { Meta, StoryObj } from '@storybook/vue3';
import UiRadioGroup from '../../components/form/radio-group/UiRadioGroup.vue';
declare const meta: Meta<typeof UiRadioGroup>;
export default meta;
type Story = StoryObj<typeof UiRadioGroup>;
export declare const Primary: Story;
export declare const Disabled: Story;
export declare const Error: Story;
