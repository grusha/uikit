import type { Meta, StoryObj } from '@storybook/vue3';
import UiInputTags from '../../components/form/input-tags/UiInputTags.vue';
declare const meta: Meta<typeof UiInputTags>;
export default meta;
type Story = StoryObj<typeof UiInputTags>;
export declare const Primary: Story;
export declare const Disabled: Story;
export declare const Error: Story;
