import type { Meta, StoryObj } from '@storybook/vue3';
import UiSelectMultiple from '../../components/form/selectMultiple/UiSelectMultiple.vue';
declare const meta: Meta<typeof UiSelectMultiple>;
export default meta;
type Story = StoryObj<typeof UiSelectMultiple>;
export declare const Primary: Story;
export declare const Disabled: Story;
