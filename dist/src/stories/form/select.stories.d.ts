import type { Meta, StoryObj } from '@storybook/vue3';
import UiSelect from '../../components/form/select/UiSelect.vue';
declare const meta: Meta<typeof UiSelect>;
export default meta;
type Story = StoryObj<typeof UiSelect>;
export declare const Primary: Story;
export declare const filterable: Story;
export declare const Disabled: Story;
export declare const Error: Story;
