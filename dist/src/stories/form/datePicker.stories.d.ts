import type { Meta, StoryObj } from '@storybook/vue3';
import UiDatePicker from '../../components/form/datePicker/UiDatePicker.vue';
declare const meta: Meta<typeof UiDatePicker>;
export default meta;
type Story = StoryObj<typeof UiDatePicker>;
export declare const Primary: Story;
