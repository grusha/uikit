import { defineComponent as $, inject as x, computed as n, onMounted as E, openBlock as S, createElementBlock as w, normalizeClass as i, toDisplayString as c, createCommentVNode as U, createVNode as b, withCtx as N, createElementVNode as r, mergeProps as G, toHandlers as L, unref as T, withDirectives as j, vShow as z } from "vue";
import { k as A, c as H, d as O, G as P } from "./index-381482a8.mjs";
const R = { class: "ui-select__value" }, q = /* @__PURE__ */ $({
  __name: "UiSelectMultiple",
  props: {
    model: { default: () => [] },
    defaultValue: { default: "" },
    options: { default: () => [] },
    placeholder: { default: "Выберите из списка" },
    label: { default: "" },
    inset: { type: Boolean, default: !1 },
    disabled: { type: Boolean, default: !1 },
    selectStyles: { default: void 0 },
    isMultiple: { type: Boolean },
    dropdownStyles: { default: void 0 },
    errorMessage: { default: "" }
  },
  emits: ["update:model"],
  setup(g, { emit: M }) {
    const e = g, u = M, d = x(A), a = n(() => e.selectStyles ? e.selectStyles : d == null ? void 0 : d.components.form.uiSelectMultiple), _ = n(() => e.dropdownStyles ? e.dropdownStyles : d == null ? void 0 : d.components.form.uiDropdown), V = n(() => {
      var t, o;
      let l = e.defaultValue.length > 0 ? e.defaultValue : e.placeholder;
      return e.model && e.model.length > 0 && (l = ((t = e.model) == null ? void 0 : t.length) > 1 ? `Выбрано: ${(o = e.model) == null ? void 0 : o.length}` : e.model[0] || e.model[0] || e.placeholder), l;
    }), I = n(() => {
      if (e.model === null)
        return [
          e.options.findIndex((t) => t.title ? t.title === e.defaultValue : t === e.defaultValue)
        ];
      const l = [];
      return e.options.forEach((t, o) => {
        t.title ? e.model.some((s) => s.title === t.title) && l.push(o) : e.model.some((s) => s === t) && l.push(o);
      }), l;
    }), k = n(() => e.options.map((l) => l.title ?? l)), B = (l) => {
      const t = e.options[l];
      if (e.model) {
        const o = e.model.findIndex((s) => t === s);
        if (o !== -1) {
          const s = e.model;
          s.splice(o, 1), u("update:model", s);
        } else
          u("update:model", [...e.model, t]);
      } else
        u("update:model", [t]);
    };
    return E(() => {
      console.warn("UiSelectMultiple is deprecated! Use UiSelectTags!");
    }), (l, t) => {
      var o, s, p;
      return S(), w("label", {
        class: i([(o = a.value) == null ? void 0 : o.selectWrapper])
      }, [
        l.label ? (S(), w("span", {
          key: 0,
          class: i([(s = a.value) == null ? void 0 : s.label])
        }, c(l.label), 3)) : U("", !0),
        b(H, {
          "selected-index-list": I.value,
          items: k.value,
          "is-select-multiple": !0,
          "dropdown-styles": _.value,
          disabled: l.disabled,
          onSelect: B
        }, {
          activator: N(({ on: C, isActive: D }) => {
            var m, f, v, h, y;
            return [
              r("div", G({
                class: [(m = a.value) == null ? void 0 : m.uiSelectDefault, { [`${(f = a.value) == null ? void 0 : f.uiSelectInset}`]: l.inset }, { [`${(v = a.value) == null ? void 0 : v.selectError}`]: l.errorMessage }]
              }, L(C, !0)), [
                r("div", {
                  class: i([(h = a.value) == null ? void 0 : h.body])
                }, [
                  r("div", R, [
                    r("div", null, c(V.value), 1)
                  ]),
                  b(O, {
                    name: D ? "chevron-up" : "chevron-down",
                    color: ((y = a.value) == null ? void 0 : y.color) ?? T(P)
                  }, null, 8, ["name", "color"])
                ], 2)
              ], 16)
            ];
          }),
          _: 1
        }, 8, ["selected-index-list", "items", "dropdown-styles", "disabled"]),
        j(r("span", {
          class: i([(p = a.value) == null ? void 0 : p.error])
        }, c(l.errorMessage), 3), [
          [z, l.errorMessage]
        ])
      ], 2);
    };
  }
});
export {
  q as default
};
