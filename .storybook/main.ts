import type { StorybookConfig } from "@storybook/vue3-vite";
import { mergeConfig } from "vite";
const turbosnap = require('vite-plugin-turbosnap');

const config: StorybookConfig = {
  core: {
    builder: '@storybook/builder-vite',
  },
  async viteFinal(config, { configType }) {
    return mergeConfig(config, {
      plugins: configType === 'PRODUCTION' ? [turbosnap({ rootDir: config.root ?? process.cwd() })] : [],
      // ...And any other config you need to change...
    });
  },
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials" , "@storybook/addon-interactions", 'storybook-dark-mode'],
  framework: {
    name: "@storybook/vue3-vite",
    options: {}
  },
};
export default config;