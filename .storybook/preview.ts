import type { Preview } from "@storybook/vue3";
import '../src/themes/default/index.scss';
import 'virtual:svg-icons-register'

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
