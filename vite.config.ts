import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';
import dts from 'vite-plugin-dts';
import * as path from 'path';

export default () => defineConfig({
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  plugins: [vue(), dts(), createSvgIconsPlugin({
    iconDirs: [path.resolve(process.cwd(), 'src/core/icons')],
    symbolId: 'icon-[name]',
  })],
  build: {
    cssCodeSplit: true,
    lib: {
      // formats: ['umd'],
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      entry: {
        index: resolve(__dirname, 'src/index.ts'),
        theme: resolve(__dirname, 'src/theme.ts'),
        core: resolve(__dirname, 'src/indexCore.ts'),
      },
      name: 'ui-kit',
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      // fileName: (_, fileName) => (fileName === 'main' ? '[name].ts' : '[name].ts'),
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue'],
      output: {
        // inlineDynamicImports: true,
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
});
