import { expect, describe, it } from 'vitest';
import { mount } from '@vue/test-utils';
import UiInputTags from '../../../../src/components/form/input-tags/UiInputTags.vue';

describe('UiInputTags', () => {
  it('check rendering', () => {
    const wrapper = mount(UiInputTags, {
      props: {
        inputTagsStyles: {
          inputTags: 'input-tags',
          inputTagsLabel: 'input-tags-label',
          inputTagsWrapper: 'input-tags-wrapper',
          inputTagsTag: 'input-tags-tag',
          inputTagsIcon: 'input-tags-icon',
          inputTagsInput: 'input-tags-input',
          inputTagsInputError: 'input-tags-input--error',
          inputTagsError: 'input-tags-error',
        },
        label: 'UiInputTags',
        errorMessage: 'Some error',
        model: ['Vue', 'React', 'Angular'],
      },
    });

    expect(wrapper.findAll('.input-tags-tag').length).toBe(3);
    expect(wrapper.find('.input-tags-label').text()).toContain('UiInputTags');
    expect(wrapper.find('.input-tags-error').text()).toContain('Some error');
  });

  it('check is model updating', async () => {
    const wrapper = mount(UiInputTags, {
      props: {
        inputTagsStyles: {
          inputTags: 'input-tags',
          inputTagsLabel: 'input-tags-label',
          inputTagsWrapper: 'input-tags-wrapper',
          inputTagsTag: 'input-tags-tag',
          inputTagsIcon: 'input-tags-icon',
          inputTagsInput: 'input-tags-input',
          inputTagsInputError: 'input-tags-input--error',
          inputTagsError: 'input-tags-error',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
        label: 'UiInputTags',
        errorMessage: 'Some error',
        model: ['Vue', 'React', 'Angular'],
      },
    });

    expect(wrapper.findAll('.input-tags-tag').length).toBe(3);

    const input = wrapper.find('input');

    await input.trigger('click');
    await input.trigger('keydown.backspace');
    expect(wrapper.findAll('.input-tags-tag').length).toBe(2);

    await wrapper.findAll('.input-tags-icon').at(1)?.trigger('click');
    expect(wrapper.findAll('.input-tags-tag').length).toBe(1);

    await input.setValue('some new value');
    await input.trigger('keyup.enter');
    expect(wrapper.findAll('.input-tags-tag').length).toBe(2);
  });

  it('make snapshot', () => {
    const wrapper = mount(UiInputTags, {
      props: {
        inputTagsStyles: {
          inputTags: 'input-tags',
          inputTagsLabel: 'input-tags-label',
          inputTagsWrapper: 'input-tags-wrapper',
          inputTagsTag: 'input-tags-tag',
          inputTagsIcon: 'input-tags-icon',
          inputTagsInput: 'input-tags-input',
          inputTagsInputError: 'input-tags-input--error',
          inputTagsError: 'input-tags-error',
        },
        label: 'UiInputTags',
        errorMessage: 'Some error',
        model: ['Vue', 'React', 'Angular'],
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });
});
