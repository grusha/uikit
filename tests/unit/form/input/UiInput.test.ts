import { expect, describe, it } from 'vitest';
import { mount } from '@vue/test-utils';
import UiInput from '../../../../src/components/form/input/UiInput.vue';
import { maskitoNumberOptionsGenerator } from '@maskito/kit';

describe('UiInput', () => {
  it('Check updating model value', async () => {
    const wrapper = mount(UiInput, {
      props: {
        inputStyles: {
          defaultInput: 'ui-input',
          errorInput: 'ui-input--error',
          defaultLabel: 'ui-input__label',
          errorLabel: 'ui-input__label--error',
          activeLabel: 'ui-input__label--active',
          bodyInput: 'ui-input-body',
          wrapper: 'ui-input__wrapper',
          input: 'ui-input__input',
          icon: 'ui-input__icon',
          error: 'ui-input__error',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    await wrapper.find('input').setValue('some new value');
    expect(wrapper.find('input').element.value).toBe('some new value');
  });

  it('Check disabled value', async () => {
    const wrapper = mount(UiInput, {
      props: {
        disabled: true,
        inputStyles: {
          defaultInput: 'ui-input',
          errorInput: 'ui-input--error',
          defaultLabel: 'ui-input__label',
          errorLabel: 'ui-input__label--error',
          activeLabel: 'ui-input__label--active',
          bodyInput: 'ui-input-body',
          wrapper: 'ui-input__wrapper',
          input: 'ui-input__input',
          icon: 'ui-input__icon',
          error: 'ui-input__error',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    const input = wrapper.find('input');

    expect(input.element.disabled).toBe(true);
  });

  // it('Check mask value', async () => {
  //   const wrapper = mount(UiInput, {
  //     props: {
  //       model: '2222',
  //       maskitoOptions: maskitoNumberOptionsGenerator({
  //         precision: 2,
  //         decimalSeparator: ',',
  //         min: 0,
  //         postfix: ' руб.',
  //       }),
  //       inputStyles: {
  //         defaultInput: 'ui-input',
  //         errorInput: 'ui-input--error',
  //         defaultLabel: 'ui-input__label',
  //         errorLabel: 'ui-input__label--error',
  //         activeLabel: 'ui-input__label--active',
  //         bodyInput: 'ui-input-body',
  //         wrapper: 'ui-input__wrapper',
  //         input: 'ui-input__input',
  //         icon: 'ui-input__icon',
  //         error: 'ui-input__error',
  //       },
  //     },
  //   });

  //   const input = wrapper.find('input');

  //   expect(input.element.value).toBe('2 222 руб.');
  // });

  it('check error message', async () => {
    const wrapper = mount(UiInput, {
      props: {
        errorMessage: 'error',
        inputStyles: {
          defaultInput: 'ui-input',
          errorInput: 'ui-input--error',
          defaultLabel: 'ui-input__label',
          errorLabel: 'ui-input__label--error',
          activeLabel: 'ui-input__label--active',
          bodyInput: 'ui-input-body',
          wrapper: 'ui-input__wrapper',
          input: 'ui-input__input',
          icon: 'ui-input__icon',
          error: 'ui-input__error',
        },
      },
    });
    const error = wrapper.find('.ui-input__error');

    expect(error.text()).toBe('error');

    await wrapper.setProps({ errorMessage: 'new error' });

    expect(error.text()).toBe('new error');
  });

  it('Make snapshot', () => {
    const wrapper = mount(UiInput, {
      props: {
        disabled: true,
        inputStyles: {
          defaultInput: 'ui-input',
          errorInput: 'ui-input--error',
          defaultLabel: 'ui-input__label',
          errorLabel: 'ui-input__label--error',
          activeLabel: 'ui-input__label--active',
          bodyInput: 'ui-input-body',
          wrapper: 'ui-input__wrapper',
          input: 'ui-input__input',
          icon: 'ui-input__icon',
          error: 'ui-input__error',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('Make password snapshot', () => {
    const wrapper = mount(UiInput, {
      props: {
        disabled: true,
        type: 'password',
        inputStyles: {
          defaultInput: 'ui-input',
          errorInput: 'ui-input--error',
          defaultLabel: 'ui-input__label',
          errorLabel: 'ui-input__label--error',
          activeLabel: 'ui-input__label--active',
          bodyInput: 'ui-input-body',
          wrapper: 'ui-input__wrapper',
          input: 'ui-input__input',
          icon: 'ui-input__icon',
          error: 'ui-input__error',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });
});
