import { expect, describe, it } from 'vitest';
import { mount } from '@vue/test-utils';
import UiSelectMultiple from '../../../../src/components/form/selectMultiple/UiSelectMultiple.vue';

describe('UiSelectMultiple', () => {
  it('Check rendering dropdown items', async () => {
    const wrapper = mount(UiSelectMultiple, {
      props: {
        name: 'dropdownId',
        options: ['Vue', 'React', 'Angular'],
        selectStyles: {
          uiSelectDefault: 'ui-select',
          uiSelectInset: 'ui-select__inset',
          label: 'ui-select__label',
          body: 'ui-select__body',
        },
        dropdownStyles: {
          dropdown: 'dropdown',
          disabled: 'dropdown_disabled',
          list: 'dropdown__list',
          item: 'dropdown__item',
          text: 'dropdown__text',
        },
      },
    });

    await wrapper.find('.ui-select__value').trigger('click');

    expect(wrapper.findAll('.dropdown__item')).length(3);
  });

  it('Check change for one selectet value', async () => {
    const wrapper = mount(UiSelectMultiple, {
      props: {
        name: 'dropdownId',
        options: ['Vue', 'React', 'Angular'],
        selectStyles: {
          uiSelectDefault: 'ui-select',
          uiSelectInset: 'ui-select__inset',
          label: 'ui-select__label',
          body: 'ui-select__body',
        },
        dropdownStyles: {
          dropdown: 'dropdown',
          disabled: 'dropdown_disabled',
          list: 'dropdown__list',
          item: 'dropdown__item',
          text: 'dropdown__text',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    await wrapper.find('.ui-select__value').trigger('click');

    await wrapper.find('.dropdown__item').trigger('click');

    expect(wrapper.props('model')).toStrictEqual(['Vue']);
    expect(wrapper.find('.ui-select__value').text()).toBe('Vue');
  });

  it('Check change for several selected value', async () => {
    const wrapper = mount(UiSelectMultiple, {
      props: {
        name: 'dropdownId',
        options: ['Vue', 'React', 'Angular'],
        selectStyles: {
          uiSelectDefault: 'ui-select',
          uiSelectInset: 'ui-select__inset',
          label: 'ui-select__label',
          body: 'ui-select__body',
        },
        dropdownStyles: {
          dropdown: 'dropdown',
          disabled: 'dropdown_disabled',
          list: 'dropdown__list',
          item: 'dropdown__item',
          text: 'dropdown__text',
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    await wrapper.find('.ui-select__value').trigger('click');

    await wrapper.find('.dropdown__item:first-child').trigger('click');

    await wrapper.find('.dropdown__item:last-child').trigger('click');

    expect(wrapper.props('model')).toStrictEqual(['Vue', 'Angular']);
    expect(wrapper.find('.ui-select__value').text()).toBe('Выбрано: 2');
  });

  it('Makes snapshot', async () => {
    const wrapper = mount(UiSelectMultiple, {
      props: {
        options: ['Vue', 'React', 'Angular'],
        name: 'dropdownId',
        selectStyles: {
          uiSelectDefault: 'ui-select',
          uiSelectInset: 'ui-select__inset',
          label: 'ui-select__label',
          body: 'ui-select__body',
        },
        dropdownStyles: {
          dropdown: 'dropdown',
          disabled: 'dropdown_disabled',
          list: 'dropdown__list',
          item: 'dropdown__item',
          text: 'dropdown__text',
        },
      },
    });

    await wrapper.find('.ui-select__value').trigger('click');
    expect(wrapper.html()).toMatchSnapshot();
  });
});
