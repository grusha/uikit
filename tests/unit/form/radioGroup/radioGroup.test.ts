import { expect, describe, it } from 'vitest';
import { mount } from '@vue/test-utils';
import UiRadioGroup from '../../../../src/components/form/radio-group/UiRadioGroup.vue';

describe('UiRadioGroup', () => {
  const styles = {
    radioGroup: 'ui-radio-group',
    radioGroupLabel: 'ui-radio-group__label',
    radioGroupContainer: 'ui-radio-group__container',
    radioGroupContainerError: 'ui-radio-group__container_error',
    radioGroupErrorMessage: 'ui-radio-group__error-message',
    radioButton: 'ui-radio-button',
    radioButtonDisabled: 'ui-radio-button_disabled',
    radioButtonInput: 'ui-radio-button__input',
    radioButtonCheckmark: 'ui-radio-button__checkmark',
    radioButtonCheckmarkChecked: 'ui-radio-button__checkmark_checked',
    radioButtonCheckmarkDisabled: 'ui-radio-button__checkmark_disabled',
    radioButtonLabel: 'ui-radio-button__label',
  };

  it('check rendering', () => {
    const wrapper = mount(UiRadioGroup, {
      props: {
        radioGroupStyles: styles,
        options: ['Vue', 'Angular', 'React'],
        label: 'UiRadioGroup',
        errorMessage: 'Some Message',
      },
    });

    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).length).toBe(3);
    expect(wrapper.findAll(`.${styles.radioGroupLabel}`).length).toBe(1);
    expect(wrapper.findAll(`.${styles.radioGroupErrorMessage}`).length).toBe(1);
  });

  it('check is model updating', async () => {
    const wrapper = mount(UiRadioGroup, {
      attachTo: document.body,
      props: {
        radioGroupStyles: styles,
        options: ['Vue', 'Angular', 'React'],
        model: 'Angular',
        label: 'UiRadioGroup',
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    const radioButtons = wrapper.findAll(`.${styles.radioButton}`);

    await radioButtons.at(0)?.find('input[type=radio]').setValue('Vue');
    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).at(0)?.classes(`${styles.radioButtonCheckmarkChecked}`)).toBe(true);

    await radioButtons.at(2)?.find('input[type=radio]').setValue('React');
    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).at(2)?.classes(`${styles.radioButtonCheckmarkChecked}`)).toBe(true);
    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).at(0)?.classes(`${styles.radioButtonCheckmarkChecked}`)).toBe(false);
  });

  it('make snapshot', async () => {
    const wrapper = mount(UiRadioGroup, {
      attachTo: document.body,
      props: {
        radioGroupStyles: styles,
        options: ['Vue', 'Angular', 'React'],
        model: 'Angular',
        label: 'UiRadioGroup',
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
    const radioButtons = wrapper.findAll(`.${styles.radioButton}`);

    await radioButtons.at(0)?.find('input[type=radio]').setValue('Vue');
    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).at(0)?.classes(`${styles.radioButtonCheckmarkChecked}`)).toBe(true);
    expect(wrapper.html()).toMatchSnapshot();

    await radioButtons.at(2)?.find('input[type=radio]').setValue('React');
    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).at(2)?.classes(`${styles.radioButtonCheckmarkChecked}`)).toBe(true);
    expect(wrapper.findAll(`.${styles.radioButtonCheckmark}`).at(0)?.classes(`${styles.radioButtonCheckmarkChecked}`)).toBe(false);
    expect(wrapper.html()).toMatchSnapshot();
  });
});
