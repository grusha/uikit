import { expect, describe, it } from 'vitest';
import { mount } from '@vue/test-utils';
import UiDatePicker from '../../../../src/components/form/datePicker/UiDatePicker.vue';


describe('DatePicker', () => {
    it('snapshot', async () => {
        const wrapper = mount(UiDatePicker, {
            props: {
                dateRangeStyles: {
                    dateRange: 'ui-daterange',
                    label: 'ui-daterange__label',
                    required: 'ui-date-range__required',
                    error: 'ui-daterange__error',
                    input: 'ui-date-range__input',
                    inputError: 'ui-date-range__input_error',
                },
                'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
            },
        });
       await wrapper.find('.dp__input_wrap').trigger('click')
       expect(wrapper.html()).toMatchSnapshot();
    })
})