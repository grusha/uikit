import { expect, describe, it } from 'vitest';
import { mount } from '@vue/test-utils';
import UiSelect from '../../../../src/components/form/select/UiSelect.vue';

describe('UiSelectExpandable', () => {
  it('Check rendering dropdown items', async () => {
    const wrapper = mount(UiSelect, {
      props: {
        name: 'dropdownId',
        options: [
          { id: '1', collectionName: 'test', title: 'Vue' },
          { id: '2', collectionName: 'test', title: 'React' },
          { id: '3', collectionName: 'test', title: 'Angular' },
        ],
        selectStyles: {
          selectBig: 'ui-select-big',
          label: 'ui-select-big__label',
          input: 'ui-select-big__select__input',
          inputDisabled: 'ui-select-big__select__input_disabled',
          required: 'ui-select-big__required',
          selectDefault: 'ui-select-big__select',
          selectError: 'ui-select-big__select--error',
          selectValue: 'ui-select-big__select__value',
          error: 'ui-select-big__error',
          dropdown: {
            dropdown: 'dropdown',
            disabled: 'dropdown_disabled',
            list: 'dropdown__list',
            item: 'dropdown__item',
            text: 'dropdown__text',
          },
        },

      },
    });

    await wrapper.find('.ui-select-big__select').trigger('click');

    expect(wrapper.findAll('.dropdown__item')).length(3);
  });

  it('Check change for one selectet value', async () => {
    const wrapper = mount(UiSelect, {
      props: {
        name: 'dropdownId',
        options: [
          { id: '1', collectionName: 'test', title: 'Vue' },
          { id: '2', collectionName: 'test', title: 'React' },
          { id: '3', collectionName: 'test', title: 'Angular' },
        ],
        selectStyles: {
          selectBig: 'ui-select-big',
          label: 'ui-select-big__label',
          input: 'ui-select-big__select__input',
          inputDisabled: 'ui-select-big__select__input_disabled',
          required: 'ui-select-big__required',
          selectDefault: 'ui-select-big__select',
          selectError: 'ui-select-big__select--error',
          selectValue: 'ui-select-big__select__value',
          error: 'ui-select-big__error',
          dropdown: {
            dropdown: 'dropdown',
            disabled: 'dropdown_disabled',
            list: 'dropdown__list',
            item: 'dropdown__item',
            text: 'dropdown__text',
          },
        },
        'onUpdate:model': async (model: any) => { await wrapper.setProps({ model }); },
      },
    });

    await wrapper.find('.ui-select-big__select').trigger('click');

    await wrapper.find('.dropdown__item').trigger('click');

    expect(wrapper.props('model')).toStrictEqual({ id: '1', title: 'Vue', collectionName: 'test' });
    expect(wrapper.find('.ui-select-big__select__value').text()).toBe('Vue');
  });

  it('Makes snapshot', async () => {
    const wrapper = mount(UiSelect, {
      props: {
        options: [
          { id: '1', collectionName: 'test', title: 'Vue' },
          { id: '2', collectionName: 'test', title: 'React' },
          { id: '3', collectionName: 'test', title: 'Angular' },
        ],
        name: 'dropdownId',
        selectStyles: {
          selectBig: 'ui-select-big',
          label: 'ui-select-big__label',
          input: 'ui-select-big__select__input',
          inputDisabled: 'ui-select-big__select__input_disabled',
          required: 'ui-select-big__required',
          selectDefault: 'ui-select-big__select',
          selectError: 'ui-select-big__select--error',
          selectValue: 'ui-select-big__select__value',
          error: 'ui-select-big__error',
          dropdown: {
            dropdown: 'dropdown',
            disabled: 'dropdown_disabled',
            list: 'dropdown__list',
            item: 'dropdown__item',
            text: 'dropdown__text',
          },
        },
      },
    });

    await wrapper.find('.ui-select-big__select').trigger('click');
    expect(wrapper.html()).toMatchSnapshot();
  });
});
