import { test, expect } from '@playwright/test';

test.describe('UiInput', () => {
  test('ui-input', async ({ page }) => {
    await test.step('Test input with type password', async () => {
      await page.goto('http://localhost:6006/?path=/story/form-uiinput--password');

      const frame = page.frame('storybook-preview-iframe');

      expect(await frame?.getByTestId('storybook-root')
        .screenshot({ path: './tests/e2e/form/input/snapshots/ui-input-password-hidden.png' }))
        .toMatchSnapshot('ui-input-password-hidden.png');

      await frame?.getByTestId('input__icon').click();
      expect(await frame?.getByTestId('storybook-root')
        .screenshot({ path: './tests/e2e/form/input/snapshots/ui-input-password-open.png' }))
        .toMatchSnapshot('ui-input-password-open.png');
    });
    await test.step('Test input with  mask', async () => {
      await page.goto('http://localhost:6006/?path=/story/form-uiinput--masked');

      const frame = page.frame('storybook-preview-iframe');

      expect(await frame?.getByTestId('storybook-root')
        .screenshot({ path: './tests/e2e/form/input/snapshots/ui-input-masked.png' }))
        .toMatchSnapshot('ui-input-masked.png');
    });
    await test.step('Test input with error', async () => {
      await page.goto('http://localhost:6006/?path=/story/form-uiinput--error');

      const frame = page.frame('storybook-preview-iframe');

      expect(await frame?.getByTestId('storybook-root')
        .screenshot({ path: './tests/e2e/form/input/snapshots/ui-input-error.png' }))
        .toMatchSnapshot('ui-input-error.png');
    });
    await test.step('Test disabled input', async () => {
      await page.goto('http://localhost:6006/?path=/story/form-uiinput--disabled');

      const frame = page.frame('storybook-preview-iframe');

      expect(await frame?.getByTestId('storybook-root')
        .screenshot({ path: './tests/e2e/form/input/snapshots/ui-input-disabled.png' }))
        .toMatchSnapshot('ui-input-disabled.png');
    });
  });
});
